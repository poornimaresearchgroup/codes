# -*- coding: utf-8 -*-
"""
Takes the data from the LAMMPS simulations of the gauss-legendre quadrature and
calculates the free energy profiles.

@author: Grant
"""

#set, nodes

import csv
import pandas as pd
import scipy.stats as stats

def pullLogFiles(setNumber, nodes):
    logFiles = []
    for i in range(nodes):
        point = i+1
        path = "Set" + str(setNumber) + "/epsilon/nodes" + str(nodes) + "/point" + str(point) + "/log.lammps"
        #path = "thermo/epsilon/nodes" + str(nodes) + "/point" + str(point) + "/log.lammps"
        
        logFiles.append(path)
        
    return logFiles

def readInData(trj):
    with open(trj, 'r') as f:
        lines = f.readlines()
        
        #Find Start of thermo data and end of thermo data
        start = 0
        end = 0
        for i in range(len(lines)):
            if lines[i][0:12] == "Per MPI rank":
                start = i+1
                break
        for i in range(start, len(lines)):
            if lines[i][0:9] == "Loop time":
                end = i
                break
        
        #Converts to pandas dataframe
        #First row will the the headers, all else will be data
        frameInfo = []
        for i in range(start, end):
            data = (" ".join(lines[i].split())).split(" ")
            frameInfo.append(data)
            
        df = pd.DataFrame(frameInfo[1:], columns=frameInfo[0]).astype(float)

    return df

def removeBurnIn(data, burn):
    #Number of thermo outputs (removing 0)
    observables = len(data.index)-1
    #Take the last burn% of the simulation
    df = data.iloc[int(-observables*burn):]
    
    return df

def generateBlocks(data, blocksize, datatype):
    df = pd.DataFrame()
    df["Block Averages"] = []
    samples= len(data.index)
    
    for i in range(int(samples/blocksize)):
        tempFrame = data.iloc[int(i*blocksize):int((i+1)*blocksize)]
        blockAverage = tempFrame[datatype].mean()
        df.loc[len(df.index)] = blockAverage
        
    return df

def generateStats(blocks):
    averageProperty = blocks["Block Averages"].mean()
    experimentalStandardDeviation = blocks["Block Averages"].std()/((len(blocks.index))**0.5)
    confidenceInterval = averageProperty - stats.norm.interval(0.95, loc=averageProperty, scale=experimentalStandardDeviation)[0]
    
    return averageProperty, confidenceInterval

#We need to show that the the average value is independent of block size
#A certain block size should show convergence

def tabulateData(trj):
    blockSizes = [1,2,3,4,5,6,7,8,9,10]
    initialLog = readInData(trj)
    production = removeBurnIn(initialLog, 0.4)
    
    dataToPull = ['Volume',
                  'Press',
                  'KinEng',
                  'PotEng',
                  'c_AA',
                  'c_BB',
                  'c_AB',
                  'c_EAAintra',
                  'c_EBBintra',
                  'c_EABintra',
                  'c_EAAinter',
                  'c_EBBinter',
                  'c_EABinter',
                  'c_mean_ang',
                  'c_mean_dihed']
    
    confidenceIntervalData = []
    
    for metric in dataToPull:
        confidenceIntervalData.append(metric + "CI")
    
    energyTerms = []

    for data in dataToPull:
        averageData = 0
        for blockSize in blockSizes:
            blocks = generateBlocks(production, blockSize, data)
            blockStats = generateStats(blocks)
            
            if round(blockStats[0],3) == averageData:
                energyTerms.append(blockStats[0])
                energyTerms.append(blockStats[1])
                break
            else:
                averageData = round(blockStats[0],3)

    return energyTerms

def main(setNumber, nodes):
    with open('Set' + str(setNumber) + 'nodes' + str(nodes) +'.csv', 'w+', newline='') as csvfile:
        #Write headers for file:
        writer = csv.writer(csvfile)
        writer.writerow(['Volume',
                         'Volume CI',
                         'Press',
                         'Press CI',
                         'KinEng',
                         'KinEng CI',
                         'PotEng',
                         'PotEng CI',
                         'AA Total',
                         'AA Total CI',
                         'BB Total',
                         'BB Total CI',
                         'AB Total',
                         'AB Total CI',
                         'AA Intra',
                         'AA Intra CI',
                         'BB Intra',
                         'BB Intra CI',
                         'AB Intra',
                         'AB Intra CI',
                         'AA Inter',
                         'AA inter CI',
                         'BB Inter',
                         'BB Inter CI',
                         'AB Inter',
                         'AB Inter CI',
                         'Mean Angle',
                         'Mean Angle CI',
                         'Mean Dihedral',
                         'Mean Dihedral CI'])
        
        #Iterate through each point in the list
        logs = pullLogFiles(setNumber, nodes)
        for log in logs:
            writer.writerow(tabulateData(log))
    
