#!/bin/bash

#SBATCH -J Kronrod

#SBATCH -o output.o
#SBATCH -e error.e

#SBATCH -t 1:0:0

#SBATCH -A chiral -p tier3 -N 1 -n 4

#job memory requirements in MB
#SBATCH --mem=1000

module load python-3.8.7-gcc-9.3.0-vbbky6n
spack load py-scipy@1.5.4 /lgxcynr
spack load py-pandas

python -c "import gaussLegnedrePostScript; gaussLegnedrePostScript.main(2,7)"
