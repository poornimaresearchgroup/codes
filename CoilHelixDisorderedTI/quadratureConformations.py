# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 13:22:17 2023

@author: Grant
"""

import csv
import numpy as np
import pandas as pd
import scipy.signal as sc
from scipy.signal import argrelextrema
import scipy.stats as stats
import warnings
import random
warnings.simplefilter(action='ignore', category=FutureWarning)

# %%% Radius of gyration
def averageRg(trj, burn):
    print("Calculating Radius of Gyration")
    with open(trj, 'r') as f:
        gyration_list = []
        gyration_list_error = []
        lines = f.readlines()[3:]
        data = (" ".join(lines[0].split())).split(" ")
        row_count = int(data[1])
        count = 1
        start = 1
        while start < len(lines):
            end = start+row_count
            mol_gyrations = []
        
            for line in lines[start:end]:
                #Iterate through timestep, average the Rg of each molecule then take all the averages for Rg
                data = (" ".join(line.split())).split(" ")
                if float(data[1]) != 0:
                    mol_gyrations.append(float(data[1]))

            gyration_list.append(np.average(mol_gyrations))
            gyration_list_error.append(np.std(mol_gyrations))
            start = end + 1
            count += 1
        

        
        #We can remove burn in here (was 40% of data)
        gyration_list = gyration_list[int(len(gyration_list)*burn):]
        gyration_list_error = gyration_list_error[int(len(gyration_list_error)*burn):]
        

    # with open(output_file, 'w') as f:
    #     f.write(str(avg_Rg))

        
    return gyration_list

# %%% Helicity
def helicity(angleFile, dihedralFile, theta, phi, tolerance):
    print("Calculating Helicity")
    SET_PHI = phi - 180

    # Number of angles from first timestep of angle file
    headerinfo = pd.read_csv(angleFile, nrows=9)
    angleNum = int(headerinfo.iloc[2, 0])

    # Number of dihedrals from first timestep of dihedral file
    headerinfo = pd.read_csv(dihedralFile, nrows=9)
    dihedralNum = int(headerinfo.iloc[2, 0])

    angle_chunker = pd.read_csv(angleFile, chunksize=angleNum + 9)
    dihedral_chunker = pd.read_csv(dihedralFile, chunksize=dihedralNum + 9)

    TimeHelicityData = pd.DataFrame(columns=["Time", "Helicity"])
    TIMESTEPS = 0

    # For single timeset of data in angle and dihedral files
    for angleChunk, dihedralChunk in zip(angle_chunker, dihedral_chunker):
        angle_time = int(angleChunk.iloc[0, 0])
        dihedral_time = int(dihedralChunk.iloc[0, 0])
        if angle_time != dihedral_time:  # checks that on same timestep
            print("Time error")

        # Removes header and creates data table sorted by atom ID for angle data
        currentAngle = [item[0].split() for item in
                        angleChunk.iloc[8:angleNum+8].values.tolist()]
        currentAngle = pd.DataFrame(currentAngle,
                                    columns=['id', "type", 'atom1', 'atom2',
                                             'atom3', 'Theta', 'Eng'],
                                    dtype='float')
        # Sorts by atom id
        currentAngle["id"] = currentAngle["id"].astype("int64")
        currentAngle.set_index("atom1", inplace=True)
        currentAngle.index = currentAngle.index.astype("int64")
        currentAngle = currentAngle.sort_values("atom1")

        # Removes header and create data table sorted by atom ID for dihedral data
        currentDihedral = [item[0].split() for item in
                           dihedralChunk.iloc[8: dihedralNum + 8].values.tolist()]
        currentDihedral = pd.DataFrame(currentDihedral,
                                       columns=['id', "type", 'atom1', 'atom2',
                                                'atom3', 'atom4', 'Phi'],
                                       dtype='float')
        # Sorts by atom id
        currentDihedral["id"] = currentDihedral["id"].astype("int64")
        currentDihedral.set_index("atom1", inplace=True)
        currentDihedral = currentDihedral.sort_values("atom1")

        # Creates dataframe for current timestep
        currentHelicity = pd.DataFrame(index=list(currentDihedral.index.values),
                                       columns=["Theta", "Theta Diff",
                                                "Theta Spec", "Phi",
                                                "Phi Diff", "Phi Spec", "Residue"],
                                       dtype="float")
        currentHelicity.index = currentHelicity.index.astype("int64")
        currentHelicity.update(currentAngle)
        currentHelicity.update(currentDihedral)

        # Finds difference between setpoint and theta with wrapping
        currentHelicity["Theta Diff"] = currentHelicity["Theta"] - theta
        currentHelicity.loc[(currentHelicity["Theta Diff"]) >
                            180, "Theta Diff"] -= 360
        currentHelicity.loc[(currentHelicity["Theta Diff"]) < -180,
                            "Theta Diff"] += 360

        # Theta Spec = 1 if theta is within spec
        currentHelicity["Theta Spec"] = np.where(abs(currentHelicity["Theta Diff"])
                                                 <= tolerance, 1, 0)

        # Finds difference between setpoint and phi with wrapping
        currentHelicity["Phi Diff"] = currentHelicity["Phi"] - SET_PHI
        currentHelicity.loc[(currentHelicity["Phi Diff"]) >
                            180, "Phi Diff"] -= 360
        currentHelicity.loc[(currentHelicity["Phi Diff"]) <
                            -180, "Phi Diff"] += 360
        # Phi Spec = 1 if within Tolerance
        currentHelicity["Phi Spec"] = np.where(abs(currentHelicity["Phi Diff"])
                                               <= tolerance, 1, 0)
        currentHelicity["Residue"] = np.where((currentHelicity["Theta Spec"] == 1)
                                              & (currentHelicity["Phi Spec"] == 1),
                                              1, 0)
        helicity = currentHelicity["Residue"].sum()/dihedralNum
        TimeHelicityData = TimeHelicityData.append({"Time": angle_time,
                                                    "Helicity": helicity},
                                                   ignore_index=True)
        TIMESTEPS += 1


    return round(TimeHelicityData["Helicity"].mean(), 3)
        


# %%% End to end Distance
def averageRee(inputFile, burn):
    print("Calculating End to End Distance")
    # Number of atoms from first timestep of trj file
    headerinfo = pd.read_csv(inputFile, nrows=9)
    atoms = int(headerinfo.iloc[2, 0])

    # TimeReeData stores mean-squared end-to-end distance for each time step
    TimeReeData = pd.DataFrame(columns=["Time", "Ree^2"])
    TIMESTEPS = 0

    # Takes each time step of a chunksize
    with pd.read_csv(inputFile, chunksize=(atoms+9)) as reader:
        for chunk in reader:
            time = int(chunk.iloc[0, 0])  # time associated with chunk
            # Get box dimensions at current timestep
            [xlo, xhi, ylo, yhi, zlo, zhi] = [float(chunk.iloc[4, 0].split()[0]),
                                              float(chunk.iloc[4, 0].split()[1]),
                                              float(chunk.iloc[5, 0].split()[0]),
                                              float(chunk.iloc[5, 0].split()[1]),
                                              float(chunk.iloc[6, 0].split()[0]),
                                              float(chunk.iloc[6, 0].split()[1])]
            
            currentstep = [item[0].split() for item in
                           chunk.iloc[8:atoms + 8].values.tolist()]
            
            currentstep = pd.DataFrame(currentstep,
                                       columns=['id', 'mol', 'type', 'x', 'y', 'z',
                                                'ix', 'iy', 'iz'], dtype='float')
            # Sorts by atom id
            currentstep["id"] = currentstep["id"].astype("int64")
            currentstep.set_index("id", inplace=True)
            currentstep = currentstep.sort_values("id")


            bChains = currentstep[currentstep.type == 2].sort_values('id')

    	    # Ree^2 calculation
            # Distance between the chosen atoms of each chain
            N = 0
            
            endToEndList = []
            
            for mol in bChains['mol'].unique():
                # Makes sure atoms are in the box
                current_chain = bChains[bChains['mol'] == mol]
                atom0 = current_chain.iloc[0]
                atomN = current_chain.iloc[-1]

                #if atom0.mol.values == atomN.mol.values:
                atom0["x"]+(atom0["ix"]*(xhi-xlo))
                x0 = atom0["x"]*(xhi-xlo)+(atom0["ix"]*(xhi-xlo))
                y0 = atom0["y"]*(yhi-ylo)+(atom0["iy"]*(yhi-ylo))
                z0 = atom0["z"]*(zhi-zlo)+(atom0["iz"]*(zhi-zlo))
                x1 = atomN["x"]*(xhi-xlo)+(atomN['ix']*(xhi-xlo))
                y1 = atomN["y"]*(yhi-ylo)+(atomN['iy']*(yhi-ylo))
                z1 = atomN["z"]*(zhi-zlo)+(atomN['iz']*(zhi-zlo))
                distance = (((x1-x0)**2)+((y1-y0)**2)+((z1-z0)**2))**0.5
                endToEndList.append(distance)
                
                #else:
                #    print("Error: " + str(molcounter))
    
    return endToEndList[int(len(endToEndList)*burn):]

# %%% Persitance Length and Pitch
def persistanceLengthPitch(trj, mon_type, burn):
    print("Calculating Persistence Length and Pitch")
    with open(trj) as f:
        data=f.readlines()
        d = []
        for a in range(len(data)):
            line = data[a]
            line = line.strip()
            line = line.split()
            d.append(line) #builds a list of all the lines in the target area
            
    atoms = int(d[3][0])
    totalsteps = int(len(d)/(atoms+9))
    firstStep = int(round(totalsteps*0.9, 0))
    lastStep = int(round(totalsteps, 0))

    pitch_list = []
    lp_list = []
    s_lp_list = []
    s_p_list = []
    total_skipped = 0

    for ts in range(firstStep, lastStep):
        currentstep = d[ts*(atoms+9):(ts+1)*(atoms+9)]
        [xlo, xhi, ylo, yhi, zlo, zhi] = [float(currentstep[5][0]),
                                          float(currentstep[5][1]),
                                          float(currentstep[6][0]),
                                          float(currentstep[6][1]),
                                          float(currentstep[7][0]),
                                          float(currentstep[7][1])]
        # currentstep hold atom info for the current step
        currentstep = currentstep[9:]
        currentstep = pd.DataFrame(currentstep, columns=[
                                'id', 'mol', 'type', 'x', 'y', 'z',
                                'ix', 'iy', 'iz'],
                                dtype='float')
        currentstep = currentstep.sort_values("id")

        # %% Unwrap Coordinates
        for i in range(atoms):
            atom0 = currentstep.iloc[i]
            atom0["x"] = atom0["x"]*(xhi-xlo)+(atom0["ix"]*(xhi-xlo))
            atom0["ix"] = 0
            atom0["y"] = atom0["y"]*(yhi-ylo)+(atom0["iy"]*(yhi-ylo))
            atom0["iy"] = 0
            atom0["z"] = atom0["z"]*(zhi-zlo)+(atom0["iz"]*(zhi-zlo))
            atom0["iz"] = 0
            
        bChains = currentstep[currentstep.type == mon_type].sort_values('id')

        # %% Tangent Function
        # %%% Information for first tangent t_0
        for chain in bChains['mol'].unique():
            try:
                currentTanTable = np.zeros((atoms-1, 4))
                currentTanTable = pd.DataFrame(currentTanTable,
                                               columns=["Sum of tan", "Number", "Avg Tan",
                                                        "Avg Distance"])
                
                current_chain = bChains[bChains.mol == float(chain)]
                for i in range(len(current_chain)-2):
    
                    atom0 = current_chain.iloc[i]
                    atom1 = current_chain.iloc[i+1]
    
                    # Unit vector t0
                    vectordistance = np.sqrt(((atom0['x']-atom1['x'])**2) + ((atom0['y']-atom1['y'])**2) + ((atom0['z']-atom1['z'])**2))
                    
                    t0 = [(atom1["x"] - atom0["x"]) / vectordistance,
                          (atom1["y"] - atom0["y"]) / vectordistance,
                          (atom1["z"] - atom0["z"]) / vectordistance]
        
                    # Information for second tangent t_N
                    for j in range(i, len(current_chain)-1):
                        atomN = current_chain.iloc[j]
                        atomN1 = current_chain.iloc[j+1]
                        
                        vectordistance = np.sqrt(((atomN['x']-atomN1['x'])**2) + ((atomN['y']-atomN1['y'])**2) + ((atomN['z']-atomN1['z'])**2))
                        
                        # Unit Vector tn
                        tn = [(atomN1["x"] - atomN["x"]) / vectordistance,
                              (atomN1["y"] - atomN["y"]) / vectordistance,
                              (atomN1["z"] - atomN["z"]) / vectordistance]
        
                        # Distance between monomers
                        distance = np.sqrt(((atom0['x']-atomN1['x'])**2) + ((atom0['y']-atomN1['y'])**2) + ((atom0['z']-atomN1['z'])**2))
        
                        # Dot product, adds data to table
                        dotproduct = np.dot(t0, tn)
                        s = j - i
                        currentTanTable["Sum of tan"][s] = \
                            currentTanTable["Sum of tan"][s] + dotproduct
                        currentTanTable["Number"][s] = currentTanTable["Number"][s]+1
                        currentTanTable["Avg Distance"][s] = \
                            currentTanTable["Avg Distance"][s] + distance
    
                # %%% Finds average tan product and distance. Stores in currenTanTable
                currentTanTable["Avg Tan"] = \
                    currentTanTable["Sum of tan"] / currentTanTable["Number"]
                currentTanTable["Avg Distance"] = \
                    currentTanTable["Avg Distance"] / currentTanTable["Number"]
        
                # %% Tangent Function over n
                # %%% Finds maxima of tangent function
                # Finds peaks on tangent function
                peaks, _ = sc.find_peaks(currentTanTable["Avg Tan"]
                                         [:int(atoms/2)], height=0)
                fullpeaks, _ = sc.find_peaks(currentTanTable["Avg Tan"][:], height=0)
        
                # peakTable holds peak locations, distance, and tan value
                peakTable = {"Location": fullpeaks,
                             "Avg Distance": currentTanTable["Avg Distance"][fullpeaks],
                             "Avg Tan": currentTanTable["Avg Tan"][fullpeaks]}
                peakTable = pd.DataFrame(peakTable)
        
                peakTable.loc[len(peakTable.index)] = [0, 0, 1]   # Adds 0,1 point as max
                peakTable = peakTable.sort_values("Location")
                peakTable.reset_index(drop=True, inplace=True)  # Index now by peak
        
                # %%% ln(tangent) over n for all peaks where n <= 
                CUTOFF = 50   # picked due to sufficient statistics as a starting point
        
                # expoFrame holds peak and ln(tan) info for current time step
                expoFrame = pd.DataFrame(peakTable["Location"].loc[peakTable["Location"]
                                                                   <= CUTOFF])
                expoFrame["Expo Fit"] = peakTable["Avg Tan"].loc[peakTable["Location"]
                                                                 <= CUTOFF]
                expoFrame["Expo Fit"] = np.log(expoFrame["Expo Fit"])
        
                pitch = peakTable["Avg Distance"].diff(1)[1]
                s_pitch = peakTable["Location"].diff(1)[1]
                # Find the first minimum peak which will be used for fit
                
                #Eliminates all the NaN values to prevent ExpoMin from selcing Nan and beind a null frame
                expoFrame.dropna(inplace=True)
                expoMin = expoFrame.iloc[argrelextrema(expoFrame["Expo Fit"].values,
                                                       np.less_equal, order=2)[0]]
                
    
                expoFrame = expoFrame[0:expoMin.index[0] + 1]
        
                # Linear fit of ln(tan) of peak points
                peak_fit = np.polyfit(expoFrame["Location"], expoFrame["Expo Fit"], 1)
                x_peak_fit = range(0, expoMin["Location"].iloc[0], 1)
                y_peak_fit = []
                for i in range(len(x_peak_fit)):
                    y_peak_fit.append(peak_fit[0]*x_peak_fit[i] + peak_fit[1])
        
                # %%% Find l_p, and s_lp
                s_lp = -1/peak_fit[0]
        
                # Extrapolates/interpolates average distance for s_lp found
                if s_lp > 0:
                    currentTanTable.index = currentTanTable.index.map(float)
                    # Extrapolates value for s_lp if value found higher than cutoff
                    if s_lp > currentTanTable.index[-1]:
                        slope = ((currentTanTable["Avg Distance"].iloc[-2] -
                                 currentTanTable["Avg Distance"].iloc[-1]) /
                                 (currentTanTable.index[-2]-currentTanTable.index[-1]))
                        b = currentTanTable["Avg Distance"].iloc[-1] \
                            - (slope * currentTanTable.index[-1])
                        currentTanTable.loc[s_lp] = [np.nan, np.nan, np.nan, np.nan]
                        currentTanTable["Avg Distance"][s_lp] = (slope*s_lp) + b
                    else:  # Interpolates if within cutoff
                        currentTanTable.loc[s_lp] = [np.nan, np.nan, np.nan, np.nan]
                        currentTanTable.sort_index(inplace=True)
                        currentTanTable.interpolate(method="linear", inplace=True)
                    lp = currentTanTable["Avg Distance"][s_lp]
                else:
                    # Print error message stating the s_lp does not exist
                    print("\n" + "s_lp = " + str(round(s_lp, 3)) + ". Cannot find lp.")
                    
                pitch_list.append(pitch)
                lp_list.append(lp)
                s_lp_list.append(s_lp)
                s_p_list.append(s_pitch)
                
            except KeyError:
                total_skipped += 1
    
    return pitch_list[int(len(pitch_list)*burn):], s_p_list[int(len(s_p_list)*burn):], lp_list[int(len(lp_list)*burn):]


# %%% Bootstrapping
def bootstrapData(dataMean, n, repeat):
    #masterList = []
    #for num in range(repeat):
    #    meanList = []
    #    for i in range(n):
    #        meanList.append(random.choice(dataMean))
    #    masterList.append(np.average(meanList))
        
        
    average = np.average(dataMean)
    experimentalError = np.std(dataMean)/(len(dataMean)**0.5)
    confidenceInterval = average - stats.norm.interval(0.95, loc=average, scale=experimentalError)[0]
    
    return average, confidenceInterval

# %%% Main function
def generateOutputTables(filepath, theta, phi, K, outputfile, burn):
    
    RgA = bootstrapData(averageRg(filepath+'/gyrationA.out', burn),40, 40)
    RgB = bootstrapData(averageRg(filepath+'/gyrationB.out', burn),40, 40)
    RgPoly = bootstrapData(averageRg(filepath+'/gyrationPoly.out', burn),40, 40)
    Ree = bootstrapData(averageRee(filepath+'/chiral.lammpstrj', burn),40, 40)
    #plp = persistanceLengthPitch(filepath+'/chiral.lammpstrj', 2, burn)
    #pitch = bootstrapData(plp[0], 40, 40)
    #sp = bootstrapData(plp[1], 40, 40)
    #lp = bootstrapData(plp[2], 40, 40)
    #heli = helicity(filepath+'/angle.dump',  filepath+'/dihedral.dump', theta, phi, 20)
    
    #thermoData = generateThermo(filepath+'/log.lammps')
    
    with open(outputfile, 'a+', newline='') as csvfile:
        writer = csv.writer(csvfile)
        
        writer.writerow([K, 
                         #theta, 
                         #phi, 
                         Ree[0],
                         Ree[1],
                         RgPoly[0],
                         RgPoly[1],
                         RgA[0],
                         RgA[1],
                         RgB[0],
                         RgB[1]
                         #pitch[0],
                         #pitch[1],
                         #sp[0],
                         #sp[1],
                         #lp[0],
                         #lp[1],
                         #heli
                         # thermoData["Volume"][0],
                         # thermoData["Volume"][1],
                         # thermoData["Temp"][0],
                         # thermoData["Temp"][1],
                         # thermoData["U"][0],
                         # thermoData["U"][1],
                         # thermoData["Ebond"][0],
                         # thermoData["Ebond"][1],
                         # thermoData["AA"][0],
                         # thermoData["AA"][1],
                         # thermoData["BB"][0],
                         # thermoData["BB"][1],
                         # thermoData["AB"][0],
                         # thermoData["AB"][1],
                         # thermoData["AA_intra"][0],
                         # thermoData["AA_intra"][1],
                         # thermoData["BB_intra"][0],
                         # thermoData["BB_intra"][1],
                         # thermoData["AB_intra"][0],
                         # thermoData["AB_intra"][1],
                         # thermoData["AA_inter"][0],
                         # thermoData["AA_inter"][1],
                         # thermoData["BB_inter"][0],
                         # thermoData["BB_inter"][1],
                         # thermoData["AB_inter"][0],
                         # thermoData["AB_inter"][1],
                         # thermoData["Mean_Ang"],
                         # thermoData["Mean_Dihed"]
                         ])

    