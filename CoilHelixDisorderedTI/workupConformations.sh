#!/bin/bash

#SBATCH -t 12:0:0

#job memory requirements in MB
#SBATCH --mem=1000

module load python-3.8.7-gcc-9.3.0-vbbky6n
spack load py-scipy@1.5.4 /lgxcynr
spack load py-pandas

python -c "import quadratureConformations; quadratureConformations.generateOutputTables(${folder}, ${bondAngle}, ${dihedralAngle}, ${K}, 'conformations'+str(${setNumber})+'.csv', 0.1)"