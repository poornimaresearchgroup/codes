Contains the python scripts used in analysis of LAMMPS simulation of the coil-helix polymer melts studied in "Coil�Helix Block Copolymers Can Exhibit Divergent Thermodynamics in the Disordered Phase" (https://doi.org/10.1021/acs.jctc.3c00680)

[] equil.lammps
	-> For running Gauss Quadrature for K = 0-5

[] equil_eps.lammps
	-> For running Gauss Quadrature for epsAB = 1-2

[] controller.sh
	-> For automating multiple runs (also passes variables to the slurm file)
	
[] workup.sh
	-> The slurm file working up the thermodynamics - calls gaussLegendrePostScript

[] workupConformations.sh
	-> For automating the postprocessing

[] workupConformations.slurm
	-> Slurm job file called by workupConformations

[] quadratureConfromations.py
	-> For extracting conformational properties post process (includes helicity, pitch, etc - Mostly Natalie's code)

[] gaussLegendrePostScript.py
	-> For extracting log file information post process (polymertools.py is just better than this is now)
	
The restart file we used is not currently in the repo. You can find our initialization procedures in the manuscript.