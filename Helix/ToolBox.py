# -*- coding: utf-8 -*-
import math
import sys

#General Math
def avg(data):
    """
    Takes a list of data
    note that this can handle None type.
    Returns the average value of that data
    """
    t = 0
    count = 0
    for number in data:
        if number:
            t += float(number)
            count += 1
    if count != 0:
        return t / count
        
def distance(point1,point2):
    [x1,y1,z1]=point1; [x2,y2,z2]=point2
    d=math.sqrt(((x2-x1)**2)+((y2-y1)**2)+((z2-z1)**2))
    return d

def float_round(num, places = 4, direction = round):
    """
    Takes a number, number of decimal places to round to
    Returns rounded number
    """
    return direction(num * (10**places)) / float(10**places)
        
def percent_error(expected, actual):
    """
    Takes theoritical and actual values
    Returns percent error
    """
    if expected ==0:
        return 0
    else:
        v1 = ((actual-expected)/expected)*100
        return float_round(v1,3)

#File and List Management
def is_number(s):
    """If input can be made into a float, returns true. Else returns false"""
    try:
        float(s)
        return True
    except ValueError:
        return False

def filter_number(s):
    import re
    s=re.sub('[^0-9]','',s)
    return s
        
def FilteredTextToList(files):
    """ Takes open file 
    Returns a list containing info from a text document """
    QList = listmaker(files)
    QMaster=[]
    for k in QList:
        List=[]
        for j in k:
            j=str(j)
            numbers=filter_number(j)  
            List.append(numbers)
        QMaster.append(List)
    return QMaster

def listmaker(file):
    """
    takes a open file
    returns all data between the start and end as a list of lists,
    note: this splits by whitespace
    """
    data = (file).readlines()
    ans = []
    for a in range(len(data)):
        line = data[a]
        list = line.strip()
        list = list.split()
        ans.append(list) #builds a list of all the lines in the target area
    file.close()
    return ans
    
def convert(list):
    """Takes list and return strings as floats"""
    for c in range(len(list)): #Makes every part of temp str (needed)
        list[c]=float(list[c])
    return list
    
def convertNew(List):
    for l in range(len(List)):
        for m in range (len(List[l])):
            List[l][m] = float(List[l][m])
    return List
            

def TextToList(files):
    """ Takes open file 
    Returns a list containing info from a text document """
    QList = listmaker(files)
    QMaster=[]
    for k in QList:
        if is_number(k[0]):
            k = [float(x) for x in k]
            QMaster.append(k)
    return QMaster
    
def getKey(item):
    """Used to sort Lists"""
    return item[0]

#Structure Factor        
def q(n, Lbox):
    """Returns coordinate for wave vector
    Inputs: number of wavelengths inside box, edge length of cubic periodic box"""
    return 2*n*math.pi/Lbox 
    
def structure_for_atom(q_info, atom, trig_totals, count):
    [qx, qy, qz, ql] = q_info
    [x, y, z] = atom
    [cossum, sinsum] = trig_totals
    dotProduct = (qx * x) + (qy * y) + (qz * z)
    cossum += math.cos(dotProduct)
    sinsum += math.sin(dotProduct)
    count += 1
    return [cossum, sinsum, count]
    
def structure_values(type_info, atoms, k):
    [cossum, sinsum, no_] = type_info
    struct=((cossum**2)+(sinsum**2))/float(atoms)
    beta=0#(2/no_)*((struct*atoms)**.5)
    theta=math.atan2(cossum,sinsum)
    
    k[5]+= struct#Structure
    k[6]+= beta#Beta
    k[7]+= theta #Theta
    #file.write("{} {} {} {} {} {}\n".format(qx,qy,qz,ql,struct,theta))
    return k

    
def rhofind(q_info, atom_info, sum, lbox):
    """Returns updated sum term used in concentration profile equation
    Inputs: [qx,qy,qz,ql,structurefactor, betam theta], [atomx, atomy,atomz], current sum term, Lbox"""
    q_info = [float(x) for x in q_info]
    [qx, qy, qz, ql, struct, beta, theta] = q_info
    [x, y, z] = atom_info
    if ql <= lbox:
        dot = (qx * x) + (qy * y) + (qz * z)
        term = beta * math.sin(dot + theta)
        sum += term
    return sum
    

#Voxel Code    
def red(v1,v2,Lbox):
    """Returns image flag by comparing 2 coordinates
    Inputs: coordinate 1, coordinate 2, Length of box"""
    delta=abs(v1-v2)
    if delta<(Lbox/2.0):
        return 0
    else:
        if v2>v1:
            return -1
        elif v2<v1:
            return 1
        else:
            return False
            
def adjust(v1,v2,Lbox):
    """Returns new coordinate 2 based on flag from red function
    Inputs: coordinate 1, coordinate 2, Lbox"""
    if red(v1,v2,Lbox)==0:
        v2=v2
    elif red(v1,v2,Lbox)==1:
        v2=v2+Lbox
    elif red(v1,v2,Lbox)==-1:
        v2=v2-Lbox
    else:
        print("ERROR")
    return v2
            
def wrap(v,Lbox):
    """Returns updated coordinate that lies within periodic box
    Inputs: coordinate, Lbox"""
    return v-Lbox*math.floor(v/Lbox)

def unwrap(v, flag, Lbox):
    "Unwraps coordinates based on flags from LAMMPS"
    flag = int(float(flag))
    if flag == 1:
        v = v + Lbox
    elif flag == -1:
        v = v - Lbox
    else:
        v = v
    return v
