function F =roots3d(r)
global b theta dihedral r1 r2 r3
r4=r;
b21=r2-r1;
b12=r1-r2;
b32=r3-r2;
b23=r2-r2;
b43=r4-r3;
b34=r3-r4;
n1=cross(b21,b32);
n2=cross(b32,b43);
F(1)=dot(n1,n2)/(norm(n1)*norm(n2))-cosd(dihedral);
F(2)=dot(b32,b34)/(norm(b32)*norm(b34))-cosd(theta);
F(3)=norm(r4-r3)-b;
end
