%% Set up the Import Options and import the data
opts = spreadsheetImportOptions("NumVariables", 34);
opts.Sheet = "FinalData";
opts.DataRange = "A3:AH124";
opts.VariableNames = ["Name", "BondType", "DataSet", "KangleKphi",...
    "Kbond", "phi_0", "theta_0", "Status", "TimeRun", "Timestep",...
    "l_p0", "s_p0", "Pitch_0", "Helicity", "HelicityStd", "Ree",...
    "ReeStd", "Rog", "RogStd","l_p", "l_pStd", "s_lp", "s_lpStd",...
    "Pitch","PitchStd","s_pitch", "s_pitchStd"];
opts.VariableTypes = ["string", "categorical", "categorical", "double", ...
    "double", "double", "double", "categorical", "double", "double", ...
    "double", "double", "double", "double", "double", "double", ...
    "double", "double", "double", "double", "double", "double", "double",...
    "double", "double", "double", "double"];
opts = setvaropts(opts, "Name", "WhitespaceRule", "preserve");
opts = setvaropts(opts, ["Name", "BondType", "DataSet", "Status"],...
    "EmptyFieldRule", "auto");
CompleteDataSet = readtable("G:\Shared drives\Poornima's Research Group\" + ...
    "MANUSCRIPTS\DONE Paper 5 Single Chiral Chain\Data\" + ...
    "CompleteDataSet.xlsx", opts, "UseExcel", false);
clear opts
%% Code Simple
selectRows= CompleteDataSet.BondType == "Harmonic" & ...
    CompleteDataSet.KangleKphi == 10 & ...
    (CompleteDataSet.DataSet == "Multiple" | ... 
    CompleteDataSet.DataSet=="Phi0" | ...
    CompleteDataSet.DataSet == "Theta0" | ...
    CompleteDataSet.DataSet == "BothAngles");
selectData = CompleteDataSet(selectRows,:);

selectData = sortrows(selectData,["theta_0", "phi_0"]);
selectData.phi_0 = selectData.phi_0 - 180;

% s=scatter3(selectData,"theta_0", "phi_0","Helicity", "filled", ...
%     "ColorVariable", "Helicity");

%% Let's Try Contour0;
xi = unique(selectData.theta_0);
yi = unique(selectData.phi_0);
Z = NaN(length(yi),length(xi));

for i=1:height(selectData)
    current_theta = selectData{i, "theta_0"};
    current_phi = selectData{i, "phi_0"};
    current_Z = selectData{i,"Pitch"};
    xn = find(xi == current_theta);
    yn = find(yi == current_phi);
    Z(yn,xn) = current_Z;
end
minZ = min(min(Z)); maxZ = max(max(Z));
minZ = floor(minZ/10)*10; maxZ = ceil(maxZ/10)*10;
levels = [minZ:10:maxZ];

f1 = figure();
[C, z] = contour(xi,yi,Z, "ShowText", "off");
hold on;
scatter(selectData.theta_0, selectData.phi_0, 25,...
    selectData.Pitch, "filled")
xlim([80 180]); ylim([0 70]);
ylabel("\phi_0 - 180^\circ");
xlabel("\theta_0");
title("Pitch")
colorbar();
formatGraph(figure(1));
legend("off")

%% Rg;
xi = unique(selectData.theta_0);
yi = unique(selectData.phi_0);
Z = NaN(length(yi),length(xi));

for i=1:height(selectData)
    current_theta = selectData{i, "theta_0"};
    current_phi = selectData{i, "phi_0"};
    current_Z = selectData{i,"Helicity"};
    xn = find(xi == current_theta);
    yn = find(yi == current_phi);
    Z(yn,xn) = current_Z;
end
minZ = min(min(Z)); maxZ = max(max(Z));
minZ = floor(minZ/10)*10; maxZ = ceil(maxZ/10)*10;
levels = [0:0.1:1];

f2 = figure();
[C, z] = contour(xi,yi,Z, "ShowText", "off");
hold on;
scatter(selectData.theta_0, selectData.phi_0, 25,...
    selectData.Helicity, "filled")
xlim([80 180]); ylim([0 70]);
ylabel("\phi_0 - 180^\circ");
xlabel("\theta_0");
title("Helicity")
colorbar();
formatGraph(f2);

legend("off")

%% sl;
xi = unique(selectData.theta_0);
yi = unique(selectData.phi_0);
Z = NaN(length(yi),length(xi));

for i=1:height(selectData)
    current_theta = selectData{i, "theta_0"};
    current_phi = selectData{i, "phi_0"};
    current_Z = selectData{i,"s_lp"};
    xn = find(xi == current_theta);
    yn = find(yi == current_phi);
    if current_Z < 100
        Z(yn,xn) = current_Z;
    else
        Z(yn,xn) = 100;
    end
end
minZ = min(min(Z)); maxZ = max(max(Z));
minZ = floor(minZ/10)*10; maxZ = ceil(maxZ/10)*10;
levels = [0:0.1:1];

f2 = figure();
[C, z] = contour(xi,yi,Z, "ShowText", "off");
hold on;
plotData= selectData.s_lp;
scatter(selectData.theta_0, selectData.phi_0, 25,...
    plotData, "filled")
xlim([80 180]); ylim([0 70]);
ylabel("\phi_0 - 180^\circ");
xlabel("\theta_0");
title("s_{l_{p}}")
colorbar();
formatGraph(f2);

legend("off")