
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 13:29:54 2022
Finds helicity at every time step using inputed set points and tolerance,
following Boehm equation for helicity. Find average helicity of last 10% of
timesteps captured in angle/dihedral dump files

Inputs:
    SIMULATION = name of sim for name of folder and text inputs and outputs
    FILEPATH = directory where data is saved and outputs will be saved
    ROGFILE = name of file with radius of gyration infor for whole chain
    AROGFILE = name of file with radius of gyration info for A block
    BROGFILE = name of file with radius of gyration info for B block
    OUTPUTFILE = name of csv file containing chain rog, A block rog, and B
        block rog for each time step

Outputs:
    csv file containing chain rog, A block rog, and B block rog 
        for each time step
    Plot 1: chain rog, A block rog, and B block rog over each time
    Prints: span, time passed during simulation, mean and std deviations 
        for the chain, A block, and B block

Hard-Coded:
    * Columns in rog files
    
Error Messages:
    * "Chunk is Not Chunking Correct": Indexing is wrong. mol index included
        in chunk does not go from 1 to max number of molecules

@author: Natalie Buchanan
@python: 3.8.13
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Input FILEPATH and trj file to be examined
SIMULATION = "K5"
FILEPATH = "C:/Users/nxb1090/Desktop/GradResearch/Helix/Data/Single_Chain/" + \
    "ParameterVaryHarmonic/BothAngles/Phi215/P215T95/" + SIMULATION
ROGFILE = FILEPATH + "/LAMMPS/" + "rog.txt"
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "RogData.txt"

headerinfo = pd.read_csv(ROGFILE, nrows=0, comment="#", sep=" ")
NUMCHUNKS = int(headerinfo.columns[1])

# %% Code
# Get header info for each time step
num_lines = sum(1 for line in open(ROGFILE))
steplist = [*range(3, num_lines, NUMCHUNKS + 1)]
to_exclude = [i for i in range(num_lines) if i not in steplist]
stepdata = pd.read_csv(ROGFILE, skiprows = to_exclude,
                        names=["Timestep", "Number-of-Chunks"],
                        sep=" ")

TimeRogData = pd.DataFrame(columns=["Time", "Rog"])
timecounter=0
# Saves mean ROG of molecule for each timestep
with pd.read_csv(ROGFILE, chunksize=NUMCHUNKS+1, header=0, sep=" ",
                 comment="#", names=["Mol", "Rog"]) as reader:
        for chunk in reader:
            chunk.where(chunk.Mol <= NUMCHUNKS, float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            #Raises error if first and last chunks in range are not the IDs expected
            if chunk.Mol.iloc[0] != 1 or chunk.Mol.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" + 
                                 str(chunk.Mol.iloc[0]) +"    " + 
                                 str(chunk.Mol.iloc[-1]))
            #Finds data from header at same time
            time = stepdata.iloc[timecounter]["Timestep"]
            TimeRogData.loc[timecounter] = [time, np.mean(chunk.Rog)]
            
            timecounter += 1

# %% Outputs
TimeRogData.to_csv(OUTPUTFILE)
SPAN = int(timecounter)
print(SPAN)
print("Time: " + str(TimeRogData.Time.max() - TimeRogData.Time.min()))

print("Mean Rog = " + str(TimeRogData["Rog"]
                                     [-SPAN:].mean()))
print("Std Rog = " + str(TimeRogData["Rog"]
                                     [-SPAN:].std()))

plt.figure()
plt.plot(TimeRogData.Time, TimeRogData.Rog, label = "Molecule")
plt.legend()
plt.xlabel("Time"); plt.ylabel("Radius of Gyration")

