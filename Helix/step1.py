# code to create a genetic algorithm
import os, shutil, math, random#, numpy as np
import subprocess, time

# assumptions. single-type monomer in molecule. Full Lennard-Jones potential
# parameter space
angle = 95
dihedral = 145 - 180

T = [1.0]
eps = [0.1]
kbond = [400]
bond_R0 = [1.0]
kangle = [0]
kdihedral = kangle

# constants for genetic algorithm
pop = 1 # population in each generation
n_gen = 1 # number of generations
# analysis of each simulation
num_lines = []
success = []
helicity = []

# create initial files
for i in range(pop):
  # folder naming scheme: gx-y. x refers to generation number and y refers to specific simulation within population
  os.mkdir("g0-"+str(i+1))
  os.chdir("g0-"+str(i+1))
  # select parameter values
  valT = T[int(math.floor(random.random()*len(T)))] # randomly select a temperature
  valeps = eps[int(math.floor(random.random()*len(eps)))] # randomly select a temperature
  valkbond = kbond[int(math.floor(random.random()*len(kbond)))] # randomly select a temperature
  valbond_R0 = bond_R0[int(math.floor(random.random()*len(bond_R0)))] # randomly select a temperature
  valkangle = kangle[int(math.floor(random.random()*len(kangle)))] # randomly select a temperature
  valkdihedral = kdihedral[int(math.floor(random.random()*len(kdihedral)))] # randomly select a temperature
  lmp = open('in.lammps','w')
  lmp.write("""units lj
atom_style molecular
pair_style lj/cut 1.1224
bond_style harmonic
angle_style quartic
dihedral_style fourier

variable T equal {}
variable eps equal {}
variable K equal {}
variable R0 equal {}
variable theta0 equal {}
variable Ktheta equal {}
variable dihedral0 equal {}
variable Kphi equal {}
\n""".format(valT, valeps,valkbond,valbond_R0,angle,valkangle,dihedral+180,valkdihedral))
  lmp.write("""
read_data in.data

velocity all create ${T} 123456789 rot yes dist gaussian

pair_coeff * * ${eps} 1
bond_coeff 1 ${K} ${R0}
angle_coeff 1 ${theta0} ${Ktheta} 0 0
dihedral_coeff 1 1 ${Kphi} 1 ${dihedral0}

special_bonds lj 1 1 1 

thermo 1000
timestep 0.0001

fix 1 all nvt temp ${T} ${T} 0.1

## Bond Info
compute bondLabel all property/local batom1 batm2
compute bondLength all bond/lcal dist engpot force
dump bondData all local 100000 ./bond.dump c_bondLabel[*] c_bondLength[*]

## Angle and Dihedral Info
compute 1 all property/local atype aatom1 aatom2 aatom3
compute 2 all angle/local theta eng
dump 1 all local 100000 ./angle.dump index c_1[1] c_1[2] c_1[3] c_1[4] c_2[1] c_2[2]

compute 3 all property/local dtype datom1 datom2 datom3 datom4
compute 4 all dihedral/local phi
dump 2 all local 100000 ./dihedral.dump index c_3[1] c_3[2] c_3[3] c_3[4] c_3[5] c_4[*]

## Radius of Gyration
compute cc1 all chunk/atom molecule
compute myChunk all gyration/chunk cc1
fix 2 all ave/time 100 1 100000 c_myChunk file rog.txt mode vector

dump trj all custom 100000 helix.lammpstrj id mol type x y z ix iy iz

run 30000000""")
  lmp.close()
  # generate in.data
  lmp = open('in.data','w')
  lmp.write('''helix N=100

100 atoms
99 bonds
98 angles
97 dihedrals

1 atom types
1 bond types
1 angle types 
1 dihedral types

0 120 xlo xhi
0 120 ylo yhi
0 120 zlo zhi

Masses

1 1.0

Atoms

''')
  gene = open('genes.txt','w');
  gene.write("{}\n{}\n{}\n{}\n{}\n{}".format(valT,valeps,valkbond,valbond_R0,valkangle,valkdihedral))
  gene.close()
  shutil.copy("../EquilibriumPositions3.m","EquilibriumPositions.m")
  shutil.copy("../roots3d.m","roots3d.m")
  os.system('module load matlab/2020a')
  mat = open('setpoints.txt','w');
  mat.write("{}\n{}\n{}".format(0.7*valbond_R0,angle,dihedral))
  mat.close()
  os.system('matlab -nosplash -nodisplay < EquilibriumPositions.m')
  # generates position.txt
  mat = open('positions.txt','r');
  data = mat.readlines()
  for n in range(len(data)):
      out = data[n].split()
      lmp.write('{} {} {} {} {} {}\n'.format(int(float(out[0])), int(float(out[1])), int(float(out[2])), float(out[3]), float(out[4]), float(out[5]))) # float converts scientific notation to a number first. Then you can re-type cast to what LAMMPS requires
  mat.close()
  lmp.write('''
Bonds

''')
  for n in range(99):
      lmp.write('{} {} {} {}\n'.format(int(n+1), 1, int(n+1), int(n+2)))
  lmp.write('''
Angles

''')
  for n in range(98):
      lmp.write('{} {} {} {} {}\n'.format(int(n+1), 1, int(n+1), int(n+2), int(n+3)))
  lmp.write('''
Dihedrals

''')
  for n in range(97):
      lmp.write('{} {} {} {} {} {}\n'.format(int(n+1), 1, int(n+1), int(n+2), int(n+3), int(n+4)))
  lmp.close()
  slurm = open('slurm-mpi.sh','w')
  slurm.write('''#!/bin/bash -l
#SBATCH --job-name g0-{}

#SBATCH --output mpi_test.o
#SBATCH --error mpi_test.e

#SBATCH --mail-type=ALL

#SBATCH --time 0-1:0:0
#SBATCH --account chirality --partition tier3 -n 4 --gres=gpu:2
#SBATCH --mem=3g
module load lammps-20181212-gcc-8.2.0-d7ufswbn
srun lmp -in in.lammps'''.format(int(i+1)))
  slurm.close()
  os.system('sbatch slurm-mpi.sh')
  os.chdir("../")
