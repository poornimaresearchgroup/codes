function formatGraph(figureID)
    for j = 1: length(figureID)
        if length(figureID) == 1
            ax = figureID(j).CurrentAxes;
        else
            ax = figureID(1,j).CurrentAxes;
        end
        box(ax, "on")
        legend(ax, "FontSize", 12, 'Location','northwest')
        ax.FontSize = 24;
        lines = findobj(figureID, "Type", "line", '-or',...
            "Type", "FunctionLine");
        for i = 1: length(lines)
            data = lines(i);
            data.LineWidth = 2;
        end
    
        scatters = findobj(figureID, "Type", "scatter");
        for i = 1:length(scatters)
            data = scatters(i);
            data.SizeData = 200;
            data.LineWidth = 2;
        end
    
        contours = findobj(figureID, "Type", "Contour");
        for i = 1: length(contours)
            data = contours(i);
            data.LineWidth = 2;
           % clabel([],data, "FontSize", 12)
        end
    end
end