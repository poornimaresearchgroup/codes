clc, clear
global b theta dihedral r1 r2 r3

vals=load('setpoints.txt');
b = vals(1);
theta = vals(2);
dihedral = vals(3);
%b = 1: %
%theta= 90; % 160
%dihedral= 20; % 20
r1=[0,0,0];
r2=[b,0,0];
r3=[b-b*cosd(theta),b*sind(theta),0];

len=100; %number of molecules

position=[r1;r2;r3];

for i=1:len-3
b21=r2-r1;
b32=r3-r2;
n=cross(b21,b32)/norm(cross(b21,b32));
r4i=r3-b*n;
func = @roots3d;
newr_plus=fsolve(func, r4i);
position(i+3,:)= newr_plus;
r4p = newr_plus;
r1=r2; % need to uncomment
r2=r3;
r3=newr_plus;

end

md=transpose([1:len]);
vmdmatrix=[md,ones(len,2),position,zeros(len,3)];
indatamatrix=[md,ones(len,2),position];
save('positions.txt','indatamatrix','-ascii');

