FILEPATH = "G:\Other computers\My Laptop\GradResearch\HelixMelt\Data\K0SmallBox\Loops\LAMMPS\";

%% Import dihedral data
for z=1
opts = delimitedTextImportOptions("NumVariables", 9);
opts.DataLines = [318010, Inf];
opts.Delimiter = " ";
opts.VariableNames = ["ITEM", "TIMESTEP", "VarName3", "VarName4", "VarName5", "VarName6", "VarName7", "Var8", "Var9"];
opts.SelectedVariableNames = ["ITEM", "TIMESTEP", "VarName3", "VarName4", "VarName5", "VarName6", "VarName7"];
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "double", "string", "string"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
opts.ConsecutiveDelimitersRule = "join";
opts.LeadingDelimitersRule = "ignore";
opts = setvaropts(opts, ["Var8", "Var9"], "WhitespaceRule", "preserve");
opts = setvaropts(opts, ["Var8", "Var9"], "EmptyFieldRule", "auto");
dihedral = readtable(FILEPATH + 'dihedral.dump', opts);
clear opts
end

%% Import data last time step
for z=1
opts = delimitedTextImportOptions("NumVariables", 18);
opts.DataLines = [1, Inf];
opts.Delimiter = [" ", "#"];
opts.VariableNames = ["VarName1", "VarName2", "Pair", "Coeffs", "VarName5", "VarName6", "VarName7", "Var8", "Var9", "Var10", "Var11", "Var12", "Var13", "Var14", "Var15", "Var16", "Var17", "Var18"];
opts.SelectedVariableNames = ["VarName1", "VarName2", "Pair", "Coeffs", "VarName5", "VarName6", "VarName7"];
opts.VariableTypes = ["string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string", "string"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
opts = setvaropts(opts, ["VarName1", "VarName2", "Pair", "Coeffs", "VarName5", "VarName6", "VarName7", "Var8", "Var9", "Var10", "Var11", "Var12", "Var13", "Var14", "Var15", "Var16", "Var17", "Var18"], "WhitespaceRule", "preserve");
opts = setvaropts(opts, ["VarName1", "VarName2", "Pair", "Coeffs", "VarName5", "VarName6", "VarName7", "Var8", "Var9", "Var10", "Var11", "Var12", "Var13", "Var14", "Var15", "Var16", "Var17", "Var18"], "EmptyFieldRule", "auto");
test = readmatrix(FILEPATH + 'Run3.data', opts);
clear opts
end
%% Code
dihedral = sortrows(dihedral, "VarName3");
newarray = zeros(height(dihedral), 2);

for i=1:97
 phi = dihedral{i,7};
 a1 = dihedral{i,3};
 
 if test(37+i,1) == num2str(a1)
     test(37+i, 4) = num2str(phi);
     newarray(i,1) = a1;
     if phi > 5
         newarray(i,2) = 1;
     elseif phi < -5
         newarray(i,2) = -1;
     else
         newarray(i,2) = 0;
     end     
     %newarray(i,2) = phi;
 else
     disp(i + "     test: " + test{37+i,1} + ...
        "     A1: " + num2str(a1))
 end
end

disp("DONE")

fileName =FILEPATH + "matlab.txt";
fileID = fopen(fileName, "w");
fprintf(fileID, "%i", (height(dihedral)));
writematrix(newarray,fileName,...
   "FileType","text", "Delimiter", ' ', "WriteMode","append")
fclose("all");
disp("Print DONE")

phiMin = min(dihedral.VarName7);
phiMax = max(dihedral.VarName7);

offset = (phiMin)/(phiMax - phiMin);
disp("Offset = " + offset)