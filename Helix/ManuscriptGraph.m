%% Import data from FullData
% opts = spreadsheetImportOptions("NumVariables", 18);
% opts.Sheet = "CombinedData";
% opts.DataRange = "A2:R38";
% opts.VariableNames = ["Name", "BondType", "DataSet", "KangleKphi", "Kbond", "phi_0", "theta_0", "EndtoEnd", "LpS", "NpS", "PitchS", "Helicity", "Lp", "LpStd", "Np", "NpStd", "Pitch", "PitchStd"];
% opts.VariableTypes = ["categorical", "categorical", "categorical", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];
% opts = setvaropts(opts, ["Name", "BondType", "DataSet"], "EmptyFieldRule", "auto");
% FullData = readtable("U:\GradResearch\Helix\Data\Single_Chain\FullData.xlsx", opts, "UseExcel", false);
% clear opts

opts = spreadsheetImportOptions("NumVariables", 25);
opts.Sheet = "FinalData";
opts.DataRange = "A3:Y49";
opts.VariableNames = ["Name", "BondType", "DataSet", "KangleKphi", "Kbond", "phi_0", "theta_0", "Status", "TimeRun", "Timstep", "l_p0", "s_p0", "Pitch_0", "Helicity", "HelicityStd", "Ree", "ReeStd", "l_p", "l_pStd", "s_p", "s_pStd", "Pitch", "PitchStd", "s_pitch", "s_pitchStd"];
opts.VariableTypes = ["categorical", "categorical", "categorical", "double", "double", "double", "double", "categorical", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];
opts = setvaropts(opts, ["Name", "BondType", "DataSet", "Status"], "EmptyFieldRule", "auto");
FullData = readtable("U:\GradResearch\Helix\Data\Single_Chain\FullData.xlsx", opts, "UseExcel", false);
clear opts

%% Graph Colors
%Colors
PitchColor = [0 0 1];
LpColor = [0 0.6 0];
E2EColor = [0 0 0];
HelColor = [0.8 0 0];

%Shapes
HarShape = "s";
FENEShape = "o";

%% K Graph
for j = 1
KVary = FullData(ismember(FullData.DataSet, {'Kvary', 'Multiple'}),:);
KVaryHarmonic = KVary(ismember(KVary.BondType, {'Harmonic'}),:);
KVaryFENE = KVary(ismember(KVary.BondType, {'FENE'}),:);

syms fit(K)
fit(K) = 1 - exp(-0.1*K);

K_Helicity = figure();
scatter(KVaryFENE.KangleKphi, KVaryFENE.Helicity,"Marker",...
    FENEShape, "MarkerEdgeColor", HelColor, "DisplayName", "FENE"); 
hold on
e = errorbar(KVaryFENE.KangleKphi, KVaryFENE.Helicity, KVaryFENE.HelicityStd,...
    "LineStyle","none");
e.Annotation.LegendInformation.IconDisplayStyle="off";
scatter(KVaryHarmonic.KangleKphi, KVaryHarmonic.Helicity,"Marker",...
    HarShape, "MarkerFaceColor", HelColor,...
    "MarkerEdgeColor", "k", "DisplayName", "Harmonic");
e2 = errorbar(KVaryHarmonic.KangleKphi, KVaryHarmonic.Helicity,...
    KVaryHarmonic.HelicityStd, "LineStyle","none");
e2.Annotation.LegendInformation.IconDisplayStyle="off";
fplot(fit, "Color", HelColor, "LineStyle", "--", "DisplayName","Eq. 8")
xlim([0 11]); ylim([0 1]);
xlabel("K^*"); ylabel("Helicity")
formatGraph(figure(K_Helicity))
legend("Location", "northwest")
end
%% Theta Graphs
for j=1
ThetaVary = FullData(ismember(FullData.DataSet, {'Theta0', 'Multiple'}),:);
ThetaVaryHarmonic = ThetaVary(ismember(ThetaVary.BondType, {'Harmonic'}),:);
ThetaVaryFENE = ThetaVary(ismember(ThetaVary.BondType, {'FENE'}),:);

% % Monomers per turn
ThetaMPT = figure();
scatter(ThetaVaryFENE.theta_0, ThetaVaryFENE.s_p,...
    "Marker", FENEShape, "MarkerEdgeColor", PitchColor, "DisplayName", "FENE");
hold on
scatter(ThetaVaryHarmonic.theta_0, ThetaVaryHarmonic.s_p,...
    "Marker", HarShape, "MarkerEdgeColor", PitchColor, "DisplayName",...
    "Harmonic", "MarkerFaceColor", [0.5 0.5 0.5]);
xlim([75 190]);
xlabel("\theta_0"); ylabel("s_p")
formatGraph(figure(ThetaMPT))

% % %End-to-End
% ThetaEnd = figure();
% scatter(ThetaVaryFENE.theta_0, ThetaVaryFENE.Ree,...
%     "Marker", FENEShape, "MarkerEdgeColor", E2EColor, "DisplayName", "FENE");
% hold on
% scatter(ThetaVaryHarmonic.theta_0, ThetaVaryHarmonic.Ree,...
%     "Marker", HarShape, "MarkerEdgeColor", E2EColor, "DisplayName",...
%     "Harmonic", "MarkerFaceColor", [0.5 0.5 0.5]);
% xlim([75 190]); ylim([0 100])
% xlabel("\theta_0"); ylabel("<R_{ee}^2 >")
% formatGraph(figure(ThetaEnd))
% 
% %Pitch
% ThetaPitch = figure();
% scatter(ThetaVaryFENE.theta_0, ThetaVaryFENE.Pitch,...
%     "Marker", FENEShape, "MarkerEdgeColor", PitchColor, "DisplayName", "FENE");
% hold on
% scatter(ThetaVaryHarmonic.theta_0, ThetaVaryHarmonic.Pitch,...
%     "Marker", HarShape, "MarkerEdgeColor", "k", "DisplayName",...
%     "Harmonic", "MarkerFaceColor", PitchColor);
% xlim([75 190]); ylim([0 20]);
% xlabel("\theta_0"); ylabel("Pitch")
% formatGraph(figure(ThetaPitch))
% 
% %Lp
% ThetaLp = figure();
% scatter(ThetaVaryFENE.theta_0, ThetaVaryFENE.Lp,...
%     "Marker", FENEShape, "MarkerEdgeColor", LpColor, "DisplayName", "FENE");
% hold on
% scatter(ThetaVaryHarmonic.theta_0, ThetaVaryHarmonic.Lp,...
%     "Marker", HarShape, "MarkerEdgeColor", "k", "DisplayName",...
%     "Harmonic", "MarkerFaceColor", LpColor);
% xlim([75 190]);
% xlabel("\theta_0"); ylabel("l_p")
% formatGraph(figure(ThetaLp))
% 
% 
% %Helicity
% ThetaHelicity = figure();
% scatter(ThetaVaryFENE.theta_0, ThetaVaryFENE.Helicity,...
%     "Marker", FENEShape, "MarkerEdgeColor", HelColor, "DisplayName", "FENE");
% hold on
% scatter(ThetaVaryHarmonic.theta_0, ThetaVaryHarmonic.Helicity,...
%     "Marker", HarShape, "MarkerEdgeColor", "k", "DisplayName",...
%     "Harmonic", "MarkerFaceColor", HelColor);
% xlim([75 190]); ylim([0 1])
% xlabel("\theta_0"); ylabel("Helicity")
% formatGraph(figure(ThetaHelicity))
end
%% Phi Graphs
for j=1
PhiVary = FullData(ismember(FullData.DataSet, {'Phi0', 'Multiple'}),:);
PhiVaryHarmonic = PhiVary(ismember(PhiVary.BondType, {'Harmonic'}),:);
PhiVaryFENE = PhiVary(ismember(PhiVary.BondType, {'FENE'}),:);

% %End-to-End
% PhiEnd = figure();
% scatter(PhiVaryFENE.phi_0-180, PhiVaryFENE.Ree,...
%     "Marker", FENEShape, "MarkerEdgeColor", E2EColor, "DisplayName", "FENE");
% hold on
% scatter(PhiVaryHarmonic.phi_0-180, PhiVaryHarmonic.Ree,...
%     "Marker", HarShape, "MarkerEdgeColor", E2EColor, "DisplayName",...
%     "Harmonic", "MarkerFaceColor", [0.5 0.5 0.5]);
% xlim([120-180 260-180]); ylim([0 100])
% xlabel("\phi_0 - 180^\circ"); ylabel("<R_{ee}^2 >")
% formatGraph(figure(PhiEnd))
% 
% %Pitch
% PhiPitch = figure();
% scatter(PhiVaryFENE.phi_0-180, PhiVaryFENE.Pitch,...
%     "Marker", FENEShape, "MarkerEdgeColor", PitchColor, "DisplayName", "FENE");
% hold on
% scatter(PhiVaryHarmonic.phi_0-180, PhiVaryHarmonic.Pitch,...
%     "Marker", HarShape, "MarkerEdgeColor", "k", "DisplayName",...
%     "Harmonic", "MarkerFaceColor", PitchColor);
% xlim([120-180 260-180]); ylim([0 20]);
% xlabel("\phi_0 - 180^\circ"); ylabel("Pitch")
% formatGraph(figure(PhiPitch))
% 
% %Lp
% PhiLp = figure();
% scatter(PhiVaryFENE.phi_0-180, PhiVaryFENE.Lp,...
%     "Marker", FENEShape, "MarkerEdgeColor", LpColor, "DisplayName", "FENE");
% hold on
% scatter(PhiVaryHarmonic.phi_0-180, PhiVaryHarmonic.Lp,...
%     "Marker", HarShape, "MarkerEdgeColor", "k", "DisplayName",...
%     "Harmonic", "MarkerFaceColor", LpColor);
% xlim([120-180 260-180]);
% xlabel("\phi_0 - 180^\circ"); ylabel("l_p")
% formatGraph(figure(PhiLp))
% 
% 
% %Helicity
% PhiHelicity = figure();
% scatter(PhiVaryFENE.phi_0-180, PhiVaryFENE.Helicity,...
%     "Marker", FENEShape, "MarkerEdgeColor", HelColor, "DisplayName", "FENE");
% hold on
% scatter(PhiVaryHarmonic.phi_0-180, PhiVaryHarmonic.Helicity,...
%     "Marker", HarShape, "MarkerEdgeColor", "k", "DisplayName",...
%     "Harmonic", "MarkerFaceColor", HelColor);
% xlim([120-180 260-180]); ylim([0 1])
% xlabel("\phi_0 - 180^\circ"); ylabel("Helicity")
% formatGraph(figure(PhiHelicity))
end
%% Ree v Helicity
Ree_v_Helicity = figure();
scatter(KVaryFENE.Helicity, KVaryFENE.Ree, "Marker",...
    FENEShape, "MarkerEdgeColor", HelColor, "DisplayName",...
    "K^* FENE");
hold on
scatter(KVaryHarmonic.Helicity, KVaryHarmonic.Ree, "Marker",...
    HarShape, "MarkerEdgeColor", "k", "DisplayName",...
    "K^* Harmonic", "MarkerFaceColor", HelColor);

% scatter(ThetaVaryFENE.Helicity, ThetaVaryFENE.Ree, "Marker",...
%     FENEShape, "MarkerFaceColor",LpColor, "MarkerEdgeColor", "k", ...
%     "DisplayName", "Theta FENE")
% scatter(ThetaVaryHarmonic.Helicity, ThetaVaryHarmonic.Ree, "Marker",...
%     HarShape, "MarkerFaceColor", LpColor,"MarkerEdgeColor", "k", ...
%     "DisplayName","Theta Harmonic")
% 
% scatter(PhiVaryFENE.Helicity, PhiVaryFENE.Ree, "Marker",...
%     FENEShape, "MarkerFaceColor",PitchColor, "MarkerEdgeColor", "k", ...
%     "DisplayName", "Phi FENE")
% scatter(PhiVaryHarmonic.Helicity, PhiVaryHarmonic.Ree, "Marker",...
%     HarShape, "MarkerFaceColor", PitchColor, "MarkerEdgeColor", "k", ...
%     "DisplayName","Phi Harmonic")

xlabel("Helicity"); ylabel("<R_{ee}^2>");
xlim([0 1]); ylim([0 25])
formatGraph(figure(Ree_v_Helicity))
%% Tangent Graphs
for j=1
    for simple = 1 % Import data from K10_TanFunc
    %    filename: U:\GradResearch\Helix\Data\Single_Chain\Repulsion_Only\KphiKtheta\K10\PostProcessing\K10_TanFunc.txt
    opts = delimitedTextImportOptions("NumVariables", 5);
    opts.DataLines = [2, Inf];
    opts.Delimiter = ",";
    opts.VariableNames = ["VarName1", "n", "TanFun", "ExponentialFit", "AvgDistance"];
    opts.VariableTypes = ["double", "double", "double", "double", "double"];
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    K10TanFunc = readtable("U:\GradResearch\Helix\Data\Single_Chain\Repulsion_Only\KphiKtheta\K10\PostProcessing\K10_TanFunc.txt", opts);
    clear opts
    end
    
    for simple = 1 % Import data from K10_peakdata
    %    filename: U:\GradResearch\Helix\Data\Single_Chain\Repulsion_Only\KphiKtheta\K10\PostProcessing\K10_peakdata.txt
    opts = delimitedTextImportOptions("NumVariables", 5);
    opts.DataLines = [2, Inf];
    opts.Delimiter = ",";
    opts.VariableNames = ["VarName1", "Location", "AvgDistance", "AvgTan", "ExponentialFit"];
    opts.VariableTypes = ["double", "double", "double", "double", "double"];
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    K10peakdata = readtable("U:\GradResearch\Helix\Data\Single_Chain\Repulsion_Only\KphiKtheta\K10\PostProcessing\K10_peakdata.txt", opts);
    clear opts
    end
    
    TanGraph = figure();
    plot(K10TanFunc.n, K10TanFunc.TanFun, "LineWidth",2, "Color",PitchColor); hold on
    scatter(K10peakdata.Location, K10peakdata.AvgTan, "rd", "filled")
    TanGraph.CurrentAxes.XTick = sort([xticks...
        K10peakdata.Location(2)]);
    TanGraph.CurrentAxes.XTickLabel{find(TanGraph.CurrentAxes.XTick ==...
        K10peakdata.Location(2))} = "s_p";
    xlim([0 50]);
    xlabel("s"); ylabel("$\left< \vec{b}_i \cdot \vec{b}_{i+s}\right>$", "interpreter", "latex");
    formatGraph(figure(TanGraph));
    legend("off");
    
    ExpoGraph = figure();
    plot(K10TanFunc.n, K10TanFunc.ExponentialFit, "Color", LpColor); hold on
    scatter(K10peakdata.Location, K10peakdata.ExponentialFit, "rd", "filled")
    xlim([0 50]);
    xlabel("s"); 
    ylabel("$\ln \left( peak \left< \vec{b}_i \cdot \vec{b}_{i+s}\right> \right)$", "interpreter", "latex")
    formatGraph(figure(ExpoGraph))
    legend("off")
    
    DistGraph = figure();
    plot(K10TanFunc.n, K10TanFunc.AvgDistance,...
        "DisplayName", "Average Distance", "LineWidth",2, "Color", "k"); hold on
    scatter(K10peakdata.Location, K10peakdata.AvgDistance, "rd","filled", ...
        "DisplayName", "Tangent Function Peaks")
    scatter(K10TanFunc.n(mod(K10TanFunc.n,1) > 0), ...
        K10TanFunc.AvgDistance(mod(K10TanFunc.n,1) > 0), "o", ...
        "MarkerFaceColor", LpColor, "MarkerEdgeColor"', LpColor,...
        "DisplayName", "Persistence Length")
    xlim([0 50]);
    xlabel("s"); ylabel("Average Distance"); legend("off")
    formatGraph(figure(DistGraph))
    DistGraph.CurrentAxes.XTick = sort([xticks...
        K10TanFunc.n(mod(K10TanFunc.n,1) > 0) K10peakdata.Location(2)]);
    DistGraph.CurrentAxes.XTickLabel{find(DistGraph.CurrentAxes.XTick ==...
        K10TanFunc.n(mod(K10TanFunc.n,1) > 0)),1} = "s_{lp}";
    DistGraph.CurrentAxes.XTickLabel{find(DistGraph.CurrentAxes.XTick ==...
        K10peakdata.Location(2))} = "s_{p}";
    DistGraph.CurrentAxes.YTick = sort([yticks...
        K10TanFunc.AvgDistance(mod(K10TanFunc.n,1) > 0) K10peakdata.AvgDistance(2)]);
    DistGraph.CurrentAxes.YTickLabel{find(DistGraph.CurrentAxes.YTick ==...
        K10TanFunc.AvgDistance(mod(K10TanFunc.n,1) > 0)),1} = "l_p";
    DistGraph.CurrentAxes.YTickLabel{find(DistGraph.CurrentAxes.YTick ==...
        K10peakdata.AvgDistance(2))} = "p";
    
    
%     DistGraph.CurrentAxes.XTick = sort([xticks...
% %         K10peakdata.Location(2)]);
%     DistGraph.CurrentAxes.XTickLabel{find(DistGraph.CurrentAxes.XTick ==...
%         K10peakdata.Location(2))} = "s_{p}";
%     DistGraph.CurrentAxes.YTick = sort([yticks...
%         K10peakdata.AvgDistance(2)]);
%     DistGraph.CurrentAxes.YTickLabel{find(DistGraph.CurrentAxes.YTick ==...
%         K10peakdata.AvgDistance(2))} = "p";
    legend("off")
end
