# -*- coding: utf-8 -*-
"""
Finds average squared end-to-end distance between two monomers of the users
choice for the last 10% of timesteps in the selected trajectory file. Also
plots average squared end-to-end distance for each timestep.

Inputs:
    SIMULATION = name of sim for name of folder and text inputs and outputs
    FILEPATH = directory where data is saved and outputs will be saved
    FILENAME = name of trajectory file
    OUTPUTFILE = name of csv file that data is written to
    INDEX = index of first and last atom for distance calculations

Outputs:
    1. csv file containing time and average squared end-to-end distance
        A text verson of TimeReeData
    2. Plot 1: Average squared end-to-end distance over time
    3. Prints: Average squared end-to-end distance over the last 10% of
        timesteps from the trajectory file

Hard-Coded:
    *Header length of trajectory file is 9 (v_headerinfo)
    *Averages over last 10% of time steps (v_SPAN)
    *Finds distance between first and last atom in chain (v_INDEX)

@author: Natalie Buchanan
@python: 3.8.10
"""

import matplotlib.pyplot as plt
import pandas as pd
import ToolBox as tb

# Input FILEPATH and trj file to be examined, span for Ree
SIMULATION = "K5"
FILEPATH = "C:/Users/nxb1090/Desktop/GradResearch/Helix/Data/Single_Chain/" + \
    "ParameterVaryHarmonic/BothAngles/Phi215/P215T95/" + SIMULATION
FILENAME = FILEPATH + "/LAMMPS/" +  "helix.lammpstrj"
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "Ree.txt"
print(FILENAME + "\n")

# %% Atom Information

# Number of atoms from first timestep of trj file
headerinfo = pd.read_csv(FILENAME, nrows=9)
atoms = int(headerinfo.iloc[2, 0])
INDEX = [1, atoms]  # first, last atom to measure Ree

# TimeReeData stores mean-squared end-to-end distance for each time step
TimeReeData = pd.DataFrame(columns=["Time", "Ree^2"])
TIMESTEPS = 0

# Takes each time step of a chunksize
with pd.read_csv(FILENAME, chunksize=(atoms+9)) as reader:
    for chunk in reader:
        time = int(chunk.iloc[0, 0])  # time associated with chunk
        # Get box dimensions at current timestep
        [xlo, xhi, ylo, yhi, zlo, zhi] = [float(chunk.iloc[4, 0].split()[0]),
                                          float(chunk.iloc[4, 0].split()[1]),
                                          float(chunk.iloc[5, 0].split()[0]),
                                          float(chunk.iloc[5, 0].split()[1]),
                                          float(chunk.iloc[6, 0].split()[0]),
                                          float(chunk.iloc[6, 0].split()[1])]
        currentstep = [item[0].split() for item in
                       chunk.iloc[8:atoms + 8].values.tolist()]
        currentstep = pd.DataFrame(currentstep,
                                   columns=['id', 'mol', 'type', 'x', 'y', 'z',
                                            'ix', 'iy', 'iz'], dtype='float')
        # Sorts by atom id
        currentstep["id"] = currentstep["id"].astype("int64")
        currentstep.set_index("id", inplace=True)
        currentstep = currentstep.sort_values("id")

        numChains = int(currentstep["mol"].max())
        chainLen = int(currentstep["mol"].value_counts().unique()[0])
        len_mol_divide = INDEX[1] - INDEX[0]

        # Unwraps atom coordinates
        for i in range(atoms):
            atom0 = currentstep.iloc[i]
            atom0["x"] = tb.unwrap(atom0["x"], atom0["ix"], xhi-xlo)
            atom0["ix"] = 0
            atom0["y"] = tb.unwrap(atom0["y"], atom0["iy"], yhi-ylo)
            atom0["iy"] = 0
            atom0["z"] = tb.unwrap(atom0["z"], atom0["iz"], zhi-zlo)
            atom0["iz"] = 0
# %%% Ree^2 calculation
        # Distance between the chosen atoms of each chain
        RSQ = 0
        N = 0
        for molcounter in range(numChains):
            # Makes sure atoms are in the box
            indexstart = (molcounter * chainLen) + INDEX[0]
            indexend = indexstart + INDEX[1] - 1

            atom0 = currentstep.iloc[[indexstart-1]]
            atomN = currentstep.iloc[[indexend-1]]

            if atom0.mol.values == atomN.mol.values:
                RSQ += (tb.distance([float(atom0["x"]), float(atom0["y"]),
                                    float(atom0["z"])],
                                    [float(atomN["x"]), float(atomN["y"]),
                                     float(atomN["z"])]))**2
            else:
                print("Error: " + str(molcounter))
            N += 1
        # Adds average Ree^2 value and the associated timestep to the table
        temp = pd.DataFrame([[time,RSQ/(N*len_mol_divide)]],
                                        columns=["Time", "Ree^2"])
        TimeReeData = pd.concat([TimeReeData,temp], ignore_index=True)
        TIMESTEPS += 1

# %% Outputs
# Saves Ree^2 per timestep table to text file
TimeReeData.to_csv(OUTPUTFILE)

# Average Ree^2 from last 10% of timesteps
SPAN = int(TIMESTEPS * 0.1)
print("Mean Ree^2 = " + str(round(TimeReeData["Ree^2"][-SPAN:].mean(), 10)))
print("Std Ree^2 = " + str(round(TimeReeData["Ree^2"][-SPAN:].std(), 10)))

# Figure 1: Average Ree^2 over timesteps
plt.figure()
plt.plot(TimeReeData["Time"], TimeReeData["Ree^2"], lw=0.5)
plt.xlabel("Time")
plt.ylabel("$R_{ee}^2$")
plt.annotate((r"Mean $R_{ee}^2$ = " +
              str(round(TimeReeData["Ree^2"][-SPAN:].mean(), 3))),
             (TimeReeData["Time"].iloc[-1],
              TimeReeData["Ree^2"].iloc[-1]),
             (.7, 0.9), "axes fraction")
plt.show()
