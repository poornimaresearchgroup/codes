import ToolBox as tb
import PostProcessingCode as code
from config import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import TestCode as test
import winsound

plt.rcParams["font.family"] = "Arial"
plt.rcParams.update({'font.size': 16})    
# 
fracForName = int((round(no_ho_mol/float(no_mol+no_ho_mol), 2))*100)
trjFile = 'L'+str(VBox[1])+'-x'+str(fracForName)
trjFile = 'Ree.txt'

# Index = [0, 60-1]; text_info = ["", ""]
# code.BondLength(filepath + 'bonds.5.lammpstrj', '15-30-15', 9, 1, Index, 'red', text_info, configInfo)

savetype=1 #1=A, 2=B, 3=C,
TypedVoxel=code.NewVoxel(configInfo, ho_configInfo, savetype ,str(trjFile), 'FromPosition.txt') #config, type(1=A, 2=B, 3=C), infile, outfile
[InterfaceA, DistanceToInterface, mindistance, maxdistance] = test.TestGetDI(configInfo, ho_configInfo, savetype, 'FromPosition.txt', 'interfaceA.txt','Distance.txt')
print('Max Distance: ' + str(maxdistance))
print('Min Distance: ' + str(mindistance))

#test.StructureFactorUpdate(configInfo, ho_configInfo, str(trjFile)+'.lammpstrj')
#test.StructureFactorGraph(configInfo, ho_configInfo, 5)

mol_type='block' #'block' or 'hom'
if mol_type == 'block':
    Index = [0, no_A-1]
else:
    Index = [0, len_ho_mol-1]
ReeArray = test.NewGetRee_Voxel(configInfo, ho_configInfo, mol_type, Index, str(trjFile), 'com_Ree_'+str(mol_type)+'.txt') #config, infile, outfile
ReeMaster = test.TestReePlots(configInfo, ho_configInfo, mol_type, 'com_Ree_'+str(mol_type)+'.txt', 'Distance.txt', 'interfaceA.txt')

winsound.MessageBeep()
