#Box Information
Box=[0, 28, 0, 28, 0, 28] #xlo xhi ylo yhi zlo zhi
VBox=[28, 28, 28] #Number of voxels in the x, y, and z directions

# Copolymer Information
no_mol = 732
fA = .25; fB=.5
len_mol = 60
atoms = no_mol*len_mol
no_A = int(fA*len_mol); no_B=int(fB*len_mol); no_C=len_mol - no_A - no_B
bonds = (len_mol-1)*no_mol

#Homopolymer Information
no_ho_mol = 1098
len_ho_mol = 20
ho_atoms = no_ho_mol*len_ho_mol
no_ho_A = len_ho_mol; no_ho_B=0; no_ho_C=0
ho_bonds = (len_ho_mol-1)*no_ho_mol

#File Information
name = str(no_A)+"-"+str(no_B)+"-"+str(no_C)
filepath = "./"  #PureABC/fA"+str(int(fA*100))+"-fB"+str(int(fB*100))+"/"
configInfo = [atoms, len_mol, Box, VBox, no_mol, no_A, no_B, no_C, bonds, name, filepath]
ho_configInfo = [ho_atoms, len_ho_mol, no_ho_mol, no_ho_A, no_ho_B, no_ho_C, ho_bonds]
