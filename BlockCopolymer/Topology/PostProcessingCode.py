# import matplotlib
# matplotlib.use("Agg") #allows figures to be saved when using ion
# DANGER: PUT BACK MATPLOTLIB COMMANDS LATER.
# DANGER: Fix com_Ree.txt to reflect Voxel
import matplotlib.pyplot as plt
import numpy as np 
import ToolBox as tb
import math
from operator import itemgetter
import scipy.io
import random 

def BondLength(filename, name, head, span, Index, color, text_info, configInfo):
    """ Returns value of x for mode bond length.
    Save a graph of the distribution of bond length
    Inputs: header length of shorttmp.txt, number of timesteps"""

    """ SECTION 2: Variables and Unpacking"""
    Output = []; Mean=0; Counter=0
    [StartIndex, EndIndex] = Index
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, tempfilepath] = configInfo
    
    """ SECTION 3: Input File """
    BDistfile = str(filename)#input(" Bond Distance File -->")
    BDistData = open(BDistfile,'r')
    BondList = tb.listmaker(BDistData)
    print(name)
    
    """ SECTION 4: Code """
    # grabs only the bonds and appends them to a new list for as many timesteps as you wanted
    for i in range(span):
        start = head + ((bonds + head) * i)
        for k in range (bonds):
            bond_number = ((k + 1) % (len_mol))
            if bond_number >= StartIndex and bond_number <= EndIndex:
                target = start + k
                temp = BondList[(target)]
                if tb.is_number(temp[0]):
                    Output.append(float(temp[-1]))
                    Mean += float(temp[-1])
                    Counter += 1
                    
    """ SECTION 5: Graph """ 
    #Creates bins for histogram that are defined by width   
    bin_size = 0.05 #bin width
    currentbin = 0
    binlist = []
    while currentbin < 4: #4 is max on x axis
        binlist.append(currentbin)
        currentbin += bin_size
    # num_bins = 100
    
    #Creates a normalized histogram
    plt.figure(1)
    y,x,_ = plt.hist(Output, binlist, label = name, alpha = 0.5, color = color, normed = True)
    max_y = (max(y)); 
    yindex = (np.where(y == max_y))
    
    mode = float(x[yindex]);
    print("Mode: "+str(mode))
    max_x = Mean/Counter
    print("Numpy Mean: "+str(np.mean(Output)))
    plt.axis([0, 2, 0.0, 3])
    #text = plt.text(text_info[0], text_info[1], 'Mean: ' + str(tb.float_round(max_x, 4)), wrap = True, backgroundcolor = color) #Text Box with Mean in it
   # text.set_bbox(dict(facecolor = color, alpha = 0.5)) #Color for text box
    plt.legend(prop = {'size': 12}) #font size for legend
    plt.xlabel("Bond Length")
    plt.ylabel("Frequency (normalized)")
    plt.title("Bond Length Distribution")
    plt.show()

    print(('Standard Deviation: '+str(np.std(x))))
    print('Max Bond Distance: '+str(max(x))+'\n')
    return max_x
    
def Ree(head, Index, filename, graphData, len_mol_div, configInfo):
    """Calculates the average end to end distance and a histogram of the distribution
        Input: head(header in Ree.txt), Index(start,end), graph(text to print)
        Hardcoded: Histogram bin size, axis ranges, number of timsteps used
        Output: prints histogram, has no return
        Assumptions: linear polymer
        """
    
    Output=[]
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    [StartIndex, EndIndex] = Index
    [graph, text_space, color] = graphData
    span = 1 # How many timesteps you want to report
    # end settings

    """ SECTION 3: Input File """
    Reefile = (filename)
    Reefile = open(str(Reefile),'r')
    AtomList = tb.listmaker(Reefile)
    
    """ SECTION 4: Code """
    # grabs only the atom info and appends them to a new list for as many timesteps as you wanted
    Sum = 0; X = [];
    for i in range(span):
        Output = []
        if i == 0:
            start = head + 1
        else:
            start = head + (atoms + head) * i
        for k in range (atoms):
            target = start + k
            temp = AtomList[target]
            temp = [float(x) for x in temp]
            if tb.is_number(temp[0]):
                Output.append(temp)
        Output.sort(key=lambda x: x[0]) #sorts by atom number

        for molcounter in range(no_mol):
            #Makes sure atoms are in the box
            indexstart = (molcounter * len_mol) + StartIndex
            [x1, y1, z1] = [tb.wrap(Output[indexstart][3], Lbox), tb.wrap(Output[indexstart][4], Lbox), tb.wrap(Output[indexstart][5], Lbox)]

            indexend = indexstart + EndIndex
            [x2, y2, z2] = [tb.wrap(Output[indexend][3], Lbox), tb.wrap(Output[indexend][4], Lbox), tb.wrap(Output[indexend][5], Lbox)]
            
            #wraps atoms if necessary
            x2 = tb.adjust(x1, x2, Lbox)
            y2 = tb.adjust(y1, y2, Lbox)
            z2 = tb.adjust(z1, z2, Lbox)
            
            #Finds distance between points
            deltax = (x2 - x1) * (x2 - x1)
            deltay = (y2 - y1) * (y2 - y1)
            deltaz = (z2 - z1) * (z2 - z1)

            Rsq = deltax + deltay + deltaz
            X.append(Rsq / (len_mol_div))
            Sum += Rsq / len_mol


    #Creates bins for histogram of specified width
    bin_size = .1 #width of bins
    currentbin = 0
    binlist = []
    while currentbin < 4: #4 is the max value of x axis
        binlist.append(currentbin)
        currentbin += bin_size

    plt.axis([0, 4, 0.0, 1.5]) #(minX, maxX, minY, maxY)
    y,x,_ = plt.hist(X, binlist, alpha=0.5, label=graph, normed=True, color=color)
    
    mean = np.mean(X)
    max_y = (max(y));
    yindex = (np.where(y == max_y))
    yindex = yindex[0][-1]
    mode = float(x[yindex]); 
    
    plt.xlabel('Ree/N (lj)'); plt.ylabel('Frequency (normalized)')
    text = plt.text(text_space[0], text_space[1], 'Mean: ' + str(round(mean,4)))
    text.set_bbox(dict(facecolor = color, alpha = 0.5))
    plt.legend()
    plt.show()
    print(str(graph) + ': ' + str(tb.avg(X)))
    print("Mode: " + str(mode))
    print(('Standard Deviation: ' + str(np.std(X))))
    print("Max Distance: " + str(max(X)) + '\n')
    
def EachRee_Voxel(head, Index, filename, len_mol_div, configInfo):
    """Calculates the Ree for each molecule or block and its center of mass
        Input: head(header in Ree.txt), Index(start,end), graph(text to print)
        Hardcoded: Histogram bin size, axis ranges, number of timsteps used
        Output: prints histogram, has no return
        Assumptions: linear polymer
        """
    Output=[]
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    [StartIndex, EndIndex] = Index
    len_mol_div = no_A
    span = 1 # How many timesteps you want to report
    # end settings

    # write to outputfile
    f = open(filepath + 'com_Ree.txt','w')

    """ SECTION 3: Input File """
    Reefile = (filename)
    Reefile = open(str(Reefile),'r')
    AtomList = tb.listmaker(Reefile)
    
    """ SECTION 4: Code """
    # grabs only the atom info and appends them to a new list for as many timesteps as you wanted
    Sum = 0; X = [];
    for i in range(span):
        Output = []
        if i == 0:
            start = head #+ 1
        else:
            start = head + (atoms + head) * i
        for k in range (atoms):
            target = start + k
            temp = AtomList[target]
            temp = [float(x) for x in temp]
            if tb.is_number(temp[0]):
                Output.append(temp)
        Output.sort(key=lambda x: x[0]) #sorts by atom number

        for molcounter in range(no_mol):
            #Makes sure atoms are in the box
            indexstart = (molcounter * len_mol) + StartIndex
            [x1, y1, z1] = [tb.wrap(Output[indexstart][3], Lbox), tb.wrap(Output[indexstart][4], Lbox), tb.wrap(Output[indexstart][5], Lbox)]

            indexend = indexstart + EndIndex
            [x2, y2, z2] = [tb.wrap(Output[indexend][3], Lbox), tb.wrap(Output[indexend][4], Lbox), tb.wrap(Output[indexend][5], Lbox)]

            #wraps atoms if necessary
            x2 = tb.adjust(x1, x2, Lbox)
            y2 = tb.adjust(y1, y2, Lbox)
            z2 = tb.adjust(z1, z2, Lbox)
            
            #Finds distance between points
            deltax = (x2 - x1) * (x2 - x1)
            deltay = (y2 - y1) * (y2 - y1)
            deltaz = (z2 - z1) * (z2 - z1)

            Rsq = deltax + deltay + deltaz
            Rsq = Rsq # does not account for number of timesteps

            # calculate R_com
            # Makes sure first atom is in the box
            indexstart = (molcounter * len_mol) + StartIndex
            [x1, y1, z1] = [tb.wrap(Output[indexstart][3], Lbox), tb.wrap(Output[indexstart][4], Lbox), tb.wrap(Output[indexstart][5], Lbox)]
            Rcom = [x1, y1, z1]
            for index_mol in range(1,len_mol_div):
                indexcurrent = indexstart + index_mol
                [x2, y2, z2] = [tb.wrap(Output[indexcurrent][3], Lbox), tb.wrap(Output[indexcurrent][4], Lbox), tb.wrap(Output[indexcurrent][5], Lbox)]
                #wraps atoms if necessary
                x2 = tb.adjust(x1, x2, Lbox)
                y2 = tb.adjust(y1, y2, Lbox)
                z2 = tb.adjust(z1, z2, Lbox)
                Rcom[0] = Rcom[0]+x2
                Rcom[1] = Rcom[1]+y2
                Rcom[2] = Rcom[2]+z2
            Rcom[0] = Rcom[0]/(len_mol_div)
            Rcom[1] = Rcom[1]/(len_mol_div)
            Rcom[2] = Rcom[2]/(len_mol_div)
            # Wrap Rcom back
            Rcom[0] = math.floor(tb.wrap(Rcom[0],Lbox)*25/Lbox)
            Rcom[1] = math.floor(tb.wrap(Rcom[1],Lbox)*25/Lbox)
            Rcom[2] = math.floor(tb.wrap(Rcom[2],Lbox)*25/Lbox)
            # write out Ree and Rcom
            f.write("{0} {1} {2} {3}\n".format(Rsq,int(Rcom[0]),int(Rcom[1]),int(Rcom[2])))
    f.close()

def VoxelStructurewithVolFrac(Vox, start, configInfo, filename):
    """Creates Document of # of atoms in Voxel.
    Using cutoff densities, each voxel can only be 1 type of atom
    Assumptions: Cubic Voxel
    Inputs: edge length of voxel, start line of Ree file
    Outputs: prints volume fraction information, creates files for use in MatLab
    """
    
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    
    mult = float(1 / Vox)
    limit = int(Lbox * mult)
    #inputfilename = 'Ree.txt' #Ree.txt'
    inputfile = open(filename, 'r')
    outputfile = open(filepath+'FromPosition.txt', 'w+')
    outputfile2 = open(filepath+'FromPositionWithVol.txt', 'w+')
    
    """SECTION 3: Text to List"""
    AtomList = [];
    AtomList = tb.listmaker(inputfile)
    
    """SECTION 4: Removes lines starting with text"""
    Output = [];
    for k in range (atoms-1):
        target = start + k
        temp = [float(x) for x in AtomList[target]]
        if tb.is_number(temp[0]): #if first character of temp a number added to List
            Output.append(temp)
    MasterList = [];
    for x in range(limit):
        for y in range(limit):
            for z in range(limit):
                List=[x, y, z, 0, 0, 0]
                MasterList.append(List)
    
    """SECTION 5: Atom -> Box"""
    counter=0
    for line in Output:
        #Scale atom coordinates to match Voxel coordinates
        AtomX, AtomY, AtomZ, Type = [int(math.floor(line[3]) * mult), int(math.floor(line[4]) * mult),int(math.floor(line[5]) * mult), line[2]]
        counter += 1
        for box in range(len(MasterList)): #Runs through list of Voxel,supplying coords
            CurrentVoxel = MasterList[box]
            CurrentX = CurrentVoxel[0]; CurrentY = CurrentVoxel[1]; CurrentZ = CurrentVoxel[2]
            ACount = CurrentVoxel[3]; BCount = CurrentVoxel[4]; CCount = CurrentVoxel[5]
            if CurrentX == AtomX and CurrentY == AtomY and CurrentZ == AtomZ: #If atom in Current Voxel
                if counter % 10000 == 0: #Prints atom counter for progress monitoring
                    print(counter)
                del(MasterList[box]) #Deletes current Voxel info
                if Type == 1:
                    ACount += 1
                elif Type == 4:
                    ACount += 1
                elif Type == 2:
                    BCount += 1
                else:
                    CCount += 1
                List=[CurrentX, CurrentY, CurrentZ, ACount, BCount, CCount]
                MasterList.append(List) #Adds updated Voxel info
    
    """SECTION 6: Volume Fractions"""
    MasterVolList = [];
    VolA = 0; VolB = 0; VolC = 0; VolAB = 0; VolBC = 0; VolAC = 0; VolE = 0; V = Vox ** 3
    for entry in MasterList: 
        #Info from current Voxel
        CurrentX = entry[0]; CurrentY = entry[1]; CurrentZ = entry[2]
        ACount = entry[3]; BCount = entry[4]; CCount = entry[5]
        fA = ACount / 3; fB = BCount / 3; fC = CCount / 3
        if fA > fB and fA > fC and fA > (3 * no_A / len_mol) * V: #if fA most prominant and higher than expected
            List = [CurrentX, CurrentY, CurrentZ, fA, 0, 0]
            VolA += 1
        elif fB > fA and fB > fC and fB > (3 * no_B / len_mol) * V:
            List = [CurrentX, CurrentY, CurrentZ, 0, fB, 0]
            VolB += 1
        elif fC > fA and fC > fB and fC > (3 * no_C / len_mol) * V:
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolC += 1
        elif fA == fB: #if volume fractions of multiple types is equal
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolAB += 1
        elif fB == fC:
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolBC += 1
        elif fA == fC:
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolAC += 1
        else:
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, 0]
            VolE += 1
        MasterVolList.append(List)
    
    MasterList = sorted(MasterList, key = itemgetter(0))
    MasterVolList = sorted(MasterVolList, key = itemgetter(0))
    
    Vbox = len(MasterVolList)
    Expected = [no_A / len_mol, no_B / len_mol, no_C / len_mol]
    Actual = [float(VolA) / Vbox, float(VolB) / Vbox, float(VolC) / Vbox]
    Error = [tb.percent_error(Expected[0], Actual[0]), tb.percent_error(Expected[1], Actual[1]), tb.percent_error(Expected[2], Actual[2])]
    
    #prints Volume Fraction Information
    print("""Expected A: {}; Frac={}; Error={}%
Expected B: {}; Frac: {}; Error: {}%
Expected C: {}; Frac: {}; Error: {}%
VolAB: {}; VolBC: {}; VolAC: {}
Expected Empty Volume: {}; Frac: {}""".format(Expected[0], Actual[0], Error[0], Expected[1], Actual[1], Error[1], Expected[2], Actual[2], Error[2], VolAB / Vbox, VolBC / Vbox, VolAC / Vbox,0,VolE / Vbox))
    
    """SECTION 7: Write Results to OutoutFile for MatLab Analysis"""
    for data in MasterList:
        outputfile.write(str(data)+'\n')
    for Voldata in MasterVolList:
        outputfile2.write(str(Voldata) + '\n')
    
    """SECTION 8: Close Files"""
    outputfile.close()
    outputfile2.close()
    inputfile.close()
    
    """SECTION 9: Return Written Data as Lists"""
    return MasterList

def Part2StructureFactor(start, stepsize, inputfiles, configInfo):
    """
    WARNING: Only works for single Sq data. No averaging can be done on the Sq for this to work
    Calculates the concentration profile using structure factor info 1
    Inputs: start (header of Ree file) and stepsize (should match size of voxel)
    Outputs 2 Documents that can be used for MatLab Visualization
    """
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    [file1, file2] = inputfiles       
                            
    """SECTION 2: File Paths"""
    inputfile1 = open(filepath + file1, 'r')
    inputfile2 = open(filepath + file2, 'r')
    
    outputfile2 = open(filepath+'DensityA.txt', 'w+')
    outputfile2C = open(filepath+'DensityC.txt', 'w+')
    
    """SECTION 3: Text to List"""
    QMaster = tb.TextToList(inputfile1)
    QMasterC = tb.TextToList(inputfile2)
    
    """SECTION 4: Concentration Profile"""
    rhoStarA = (no_A / len_mol) #expected density
    rhoStarC = (no_A / len_mol) * 3 
    counter = 0
    for x in range(int(Lbox / stepsize)):
        AtomX = x * stepsize
        print(AtomX) #for progress tracking
        for y in range(int(Lbox / stepsize)):
            AtomY = y * stepsize
            for z in range(int(Lbox / stepsize)): #Generates position in box
                AtomZ = z * stepsize
                counter += 1/rhoStarA
                SumA = 0; SumC = 0
                for qValue in QMaster:
                    SumA = tb.rhofind(qValue, [AtomX, AtomY, AtomZ], SumA, Lbox)
                rhoR =  (1 + SumA)
                outputfile2.write("{} {} {} {} {}\n".format(counter, AtomX, AtomY, AtomZ, rhoR))
    
                for qValueC in QMasterC:
                    SumC = tb.rhofind(qValueC, [AtomX, AtomY, AtomZ], SumC, Lbox)
                rhoRC = rhoStarC * (1 + SumC)
                outputfile2C.write("{} {} {} {} {}\n".format(counter, AtomX, AtomY, AtomZ, rhoRC))
    print("DONE")
    
    """SECTION 5: Files Close"""
    inputfile1.close()
    inputfile2.close()
    outputfile2.close()
    outputfile2C.close()
    

def NewGetRee_Voxel(configInfo, ho_configInfo, mol_type, Index, filein, fileout, DistanceToInterface):
    """Calculates the Ree for each molecule or block and its center of mass
        Input: head(header in Ree.txt), Index(start,end), graph(text to print)
        Hardcoded: Histogram bin size, axis ranges, number of timsteps used
        Output: prints histogram, has no return
        Assumptions: linear polymer
        """
    
    [atoms, len_mol, Box, VBox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    [ho_atoms, len_ho_mol, no_ho_mol, no_ho_A, no_ho_B, no_ho_C, ho_bonds] = ho_configInfo
    
    total_atoms = atoms + ho_atoms; total_mol = no_ho_mol + no_mol
    
    
    filename = filepath + str(filein)
    head=9
    [StartIndex, EndIndex] = Index;
    [NVBoxX, NVBoxY, NVBoxZ] = VBox
    LBoxX=Box[1]-Box[0]; LBoxY=Box[3]-Box[2]; LBoxZ=Box[5]-Box[4]
    
    Output=[]
    span = 1 # How many timesteps you want to report
    len_mol_div = Index[1]-Index[0] + 1
    # end settings
    
    # write to outputfile
    f = open(filepath + str(fileout),'w+')
    
    """ SECTION 3: Input File """
    Reefile = (filename)
    Reefile = open(str(Reefile),'r')
    AtomList = tb.listmaker(Reefile)

    ReeArray=np.zeros((VBox[0], VBox[1], VBox[2]))
    
    """ SECTION 4: Code """
    # grabs only the atom info and appends them to a new list for as many timesteps as you wanted
    for i in range(span):
        Output = []
        
        if i == 0:
            start = head #+ 1
        else:
            start = head + (total_atoms + head) * i
        for k in range (total_atoms):
            target = start + k
            temp = AtomList[target]
            temp = [float(x) for x in temp]
            if tb.is_number(temp[0]):
                Output.append(temp)
        Output.sort(key=lambda x: x[0]) #sorts by atom number
        
        if mol_type == 'block':
            for molcounter in range(no_mol):
                #Makes sure atoms are in the box
                indexstart = (molcounter * len_mol) + StartIndex
                [x1, y1, z1] = [tb.wrap(Output[indexstart][3], LBoxX), tb.wrap(Output[indexstart][4], LBoxY), tb.wrap(Output[indexstart][5], LBoxZ)]
        
                indexend = indexstart + EndIndex
                [x2, y2, z2] = [tb.wrap(Output[indexend][3], LBoxX), tb.wrap(Output[indexend][4], LBoxY), tb.wrap(Output[indexend][5], LBoxZ)]
        
                #wraps atoms if necessary
                x2 = tb.adjust(x1, x2, LBoxX)
                y2 = tb.adjust(y1, y2, LBoxY)
                z2 = tb.adjust(z1, z2, LBoxZ)
                
                #Finds distance between points
                deltax = (x2 - x1) * (x2 - x1)
                deltay = (y2 - y1) * (y2 - y1)
                deltaz = (z2 - z1) * (z2 - z1)
        
                Rsq = deltax + deltay + deltaz
                Rsq = Rsq # does not account for number of timesteps
        
                # calculate R_com
                # Makes sure first atom is in the box
                indexstart = (molcounter * len_mol) + StartIndex
                [x1, y1, z1] = [tb.wrap(Output[indexstart][3], LBoxX), tb.wrap(Output[indexstart][4], LBoxY), tb.wrap(Output[indexstart][5], LBoxZ)]
                Rcom = [x1, y1, z1]
                for index_mol in range(1,len_mol_div):
                    indexcurrent = indexstart + index_mol
                    [x2, y2, z2] = [tb.wrap(Output[indexcurrent][3], LBoxX), tb.wrap(Output[indexcurrent][4], LBoxY), tb.wrap(Output[indexcurrent][5], LBoxZ)]
                    #wraps atoms if necessary
                    x2 = tb.adjust(x1, x2, LBoxX)
                    y2 = tb.adjust(y1, y2, LBoxY)
                    z2 = tb.adjust(z1, z2, LBoxZ)
                    Rcom[0] = Rcom[0]+x2
                    Rcom[1] = Rcom[1]+y2
                    Rcom[2] = Rcom[2]+z2
                Rcom[0] = Rcom[0]/(len_mol_div)
                Rcom[1] = Rcom[1]/(len_mol_div)
                Rcom[2] = Rcom[2]/(len_mol_div)   
                # Wrap Rcom back
                Rcom[0] = int(math.floor(tb.wrap(Rcom[0],LBoxX)*NVBoxX/LBoxX))
                Rcom[1] = int(math.floor(tb.wrap(Rcom[1],LBoxY)*NVBoxY/LBoxY))
                Rcom[2] = int(math.floor(tb.wrap(Rcom[2],LBoxZ)*NVBoxZ/LBoxZ))
                
                DI=DistanceToInterface[Rcom[0], Rcom[1], Rcom[2]]
                
                ReeArray[int(Rcom[0]),int(Rcom[1]),int(Rcom[2])]=Rsq
                # write out Ree and Rcom
                f.write("{0} {1} {2} {3} {4} {5}\n".format(molcounter, Rsq, Rcom[0], Rcom[1], Rcom[2], DI))
            f.close()
            
        if mol_type == 'hom':    
                
            for molcounter in range(no_ho_mol-1):
                #Makes sure atoms are in the box
                indexstart = (molcounter * len_ho_mol) + StartIndex + atoms
                [x1, y1, z1] = [tb.wrap(Output[indexstart][3], LBoxX), tb.wrap(Output[indexstart][4], LBoxY), tb.wrap(Output[indexstart][5], LBoxZ)]
        
                indexend = indexstart + EndIndex
                [x2, y2, z2] = [tb.wrap(Output[indexend][3], LBoxX), tb.wrap(Output[indexend][4], LBoxY), tb.wrap(Output[indexend][5], LBoxZ)]
        
                #wraps atoms if necessary
                x2 = tb.adjust(x1, x2, LBoxX)
                y2 = tb.adjust(y1, y2, LBoxY)
                z2 = tb.adjust(z1, z2, LBoxZ)
                
                #Finds distance between points
                deltax = (x2 - x1) * (x2 - x1)
                deltay = (y2 - y1) * (y2 - y1)
                deltaz = (z2 - z1) * (z2 - z1)
        
                Rsq = deltax + deltay + deltaz
                Rsq = Rsq # does not account for number of timesteps
        
                # calculate R_com
                # Makes sure first atom is in the box
                indexstart = (molcounter * len_ho_mol) + StartIndex
                [x1, y1, z1] = [tb.wrap(Output[indexstart][3], LBoxX), tb.wrap(Output[indexstart][4], LBoxY), tb.wrap(Output[indexstart][5], LBoxZ)]
                Rcom = [x1, y1, z1]
                for index_mol in range(1,len_mol_div):
                    indexcurrent = indexstart + index_mol
                    [x2, y2, z2] = [tb.wrap(Output[indexcurrent][3], LBoxX), tb.wrap(Output[indexcurrent][4], LBoxY), tb.wrap(Output[indexcurrent][5], LBoxZ)]
                    #wraps atoms if necessary
                    x2 = tb.adjust(x1, x2, LBoxX)
                    y2 = tb.adjust(y1, y2, LBoxY)
                    z2 = tb.adjust(z1, z2, LBoxZ)
                    Rcom[0] = Rcom[0]+x2
                    Rcom[1] = Rcom[1]+y2
                    Rcom[2] = Rcom[2]+z2
                Rcom[0] = Rcom[0]/(len_mol_div)
                Rcom[1] = Rcom[1]/(len_mol_div)
                Rcom[2] = Rcom[2]/(len_mol_div)   
                # Wrap Rcom back
                Rcom[0] = int(math.floor(tb.wrap(Rcom[0],LBoxX)*NVBoxX/LBoxX))
                Rcom[1] = int(math.floor(tb.wrap(Rcom[1],LBoxY)*NVBoxY/LBoxY))
                Rcom[2] = int(math.floor(tb.wrap(Rcom[2],LBoxZ)*NVBoxZ/LBoxZ))
                
                DI=DistanceToInterface[Rcom[0], Rcom[1], Rcom[2]]
                
                ReeArray[int(Rcom[0]),int(Rcom[1]),int(Rcom[2])]=Rsq
                # write out Ree and Rcom
                f.write("{0} {1} {2} {3} {4} {5}\n".format(molcounter, Rsq, Rcom[0], Rcom[1], Rcom[2], DI))
            f.close()
            
    return ReeArray

def ReePlots(configInfo, maxdistance, Reeinfile, DIfile):
    [atoms, len_mol, Box, VBox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    DI=[]; ReeMaster=[]; Ncom=[]; Avg=[]; Nvox=[]
    for counter in range(int(round(maxdistance))+1):
        DI.append(counter)
        ReeMaster.append([])
        Ncom.append(0); Avg.append(0); Nvox.append(0)
        
    Reefile = (filepath+Reeinfile)
    Reefile = open(str(Reefile),'r')
    ReeList = tb.listmaker(Reefile)
    
    DIfile = (filepath+DIfile)
    DIfile = open(str(DIfile),'r')
    DIList = tb.listmaker(DIfile)
    
    maxRee=0; line=0
    for mol in ReeList:
        mol_distance=int(round(float(mol[5])))
        ReeMaster[mol_distance].append(float(mol[1]))
        if float(mol[1])>maxRee:
            maxRee = float(mol[1])
            line = mol
    print(line)
    
    for i in range(len(ReeMaster)):
        Ncom[i] = len(ReeMaster[i])/float(no_mol)
        if len(ReeMaster[i]) > 0:
            Avg[i] = tb.avg(ReeMaster[i])
        else:
            Avg[i] = 0
            
    for vox in DIList:
        VoxDistance=int(round(float(vox[3])))
        Nvox[VoxDistance]+=1
    for i in range(len(Nvox)):
        Nvox[i] = Nvox[i]/float((VBox[0] * VBox[1] * VBox[2]))
    
    bin_size = 5 #bin width
    currentbin = 0
    binlist = []
    while currentbin < maxRee: #4 is max on x axis
        binlist.append(currentbin)
        currentbin += bin_size
    # num_bins = 100
    
            
    fig = plt.figure()
    plt.bar(DI, Nvox)
    plt.xlabel('Distance To Interface'); plt.ylabel('Number of Voxels')
    plt.title('DI of Voxels')
    plt.show()    
 
    print(DI, Avg)
    fig = plt.figure()
    plt.bar(DI, Avg)
    plt.xlabel('Distance To Interface'); plt.ylabel('Average Ree')
    plt.title('Average Ree by DI')
    plt.show()  
    
    fig = plt.figure()
    plt.bar(DI, Ncom)
    plt.xlabel('Distance To Interface'); plt.ylabel('Number of Polymers')
    plt.title('Number of Polymers by DI')
    plt.show()  
    
    fig = plt.figure()
    plt.hist(ReeMaster, binlist)
    plt.xlabel('Ree'); plt.ylabel('Number of Polymers')
    plt.title('Ree Frequency for each DI')
    plt.legend(DI)
    plt.show()  
    
    return ReeMaster
    
def NewVoxel(configInfo, ho_configInfo, Type, filein, fileout):
    """NewVoxelCode"""
    #Extract atom positions from last step of trj file
    #Matrix that represents every voxel - must work if voxels not size 1
    #For each atom: Find which voxel it is located in
        #Each voxel store: x, y, z, NoA, NoB, NoC
        #Output this
        
    #For each voxel - assign type, whatever is most [do not consider density]
        #If equal - assign randomly ( rand number generator)
    
    #Input: configInfo, type to counter (1=A, 2=B, 3=C), infile, outfile    
    #Output: List of each voxel: x, y, z, typeA, typeB, typeC (type are 0 or 1)
    
    #Checks:
        #First output - if add all NoA, NoB, NoC - should get total number of atoms
            #If add all NoA should get number of A etc.
        #Second Output - if add all types, should get number of voxels
        #Plot in MatLab
        #Check if sorted input effects code
        
    [atoms, len_mol, Box, VBox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    [ho_atoms, len_ho_mol, no_ho_mol, no_ho_A, no_ho_B, no_ho_C, ho_bonds] = ho_configInfo
    
    total_atoms = atoms + ho_atoms; total_mol = no_ho_mol + no_mol

    header=9
    [NVBoxX, NVBoxY, NVBoxZ] = VBox
    
    inputfile = open(filepath + filein, 'r')
    outputfile = open(filepath + fileout, 'w+')
    
    """Extrat Atom Positions from lastsnap"""
    InputList = [];
    InputList = tb.listmaker(inputfile)
     
    AtomList = [];
    for k in range (total_atoms):
        target = header + k
        temp = [float(x) for x in InputList[target]]
        if tb.is_number(temp[0]): #if first character of temp a number added to List
            AtomList.append(temp)
    
    """ Create empty Voxel - {xlo, ylo, zlo, 0, 0, 0}"""
    LBoxX=Box[1]-Box[0]; LBoxY=Box[3]-Box[2]; LBoxZ=Box[5]-Box[4]
    StepX=LBoxX/NVBoxX; StepY=LBoxY/NVBoxY; StepZ=LBoxZ/NVBoxZ
    Voxels=[]; TypedVoxels=[]
    for i in range (NVBoxX):
        for j in range (NVBoxY):
            for k in range (NVBoxZ):
                Voxels.append([StepX * i, StepY* j, StepZ * k, 0, 0, 0])
                
    """Assign Each Atom to a Voxel"""
    InVoxel=False
    for atom in AtomList:
        AtomID=atom[0]; MolType=atom[1]; AtomType=int(atom[2]); AtomX=atom[3]; AtomY=atom[4]; AtomZ=atom[5]
        AtomX=tb.wrap(AtomX, LBoxX); AtomY=tb.wrap(AtomY,LBoxY); AtomZ=tb.wrap(AtomZ,LBoxZ)
        InVoxel=False
        for Voxel in Voxels:
            [xlo, ylo,zlo, NoA, NoB, NoC] = Voxel
            xhi=xlo + StepX; yhi=ylo+StepY; zhi=zlo+StepZ
            if AtomX >=xlo and AtomX <xhi and AtomY >=ylo and AtomY < yhi and AtomZ >= zlo and AtomZ < zhi:
                InVoxel=True
                if AtomType == 1 or AtomType == 4:
                    Voxel[3]+=1
                elif AtomType == 2:
                    Voxel[4]+=1
                elif AtomType == 3:
                    Voxel[5]+=1
                else:
                    print('No Type:' + str(AtomID))
        if InVoxel==False:
            print("Not In Voxel: " + str(atom))
        
    """Check 1"""
    totalcounter=0; Acount=0; Bcount=0; Ccount=0
    for Voxel in Voxels:
        [xlo, ylo,zlo, NoA, NoB, NoC] = Voxel
        totalcounter=totalcounter+NoA+NoB+NoC
        Acount+=NoA; Bcount+=NoB; Ccount+=NoC
    print("Check 1: total atoms, A atoms, B atoms, C atoms" + '\n'+str([totalcounter, Acount, Bcount, Ccount]))
    
    """Typing Voxels"""
    TypedVoxels=[]; EmptyCounter=0;
    for Voxel in Voxels:
        [xlo, ylo,zlo, NoA, NoB, NoC] = Voxel
        Counts=[NoA, NoB, NoC]
        maxCount = max(Counts)
        TempTypedVoxel=[int(xlo/StepX), int(ylo/StepY), int(zlo/StepZ), 0, 0, 0]
        maxIndex = [i for i,j in enumerate(Counts) if j==maxCount]
        ran=random.random()
        if maxCount > 0:
            for i in range(len(maxIndex)):
                if ran >= i/len(maxIndex) and ran < (i+1)/len(maxIndex):
                    TempTypedVoxel[maxIndex[i]+3]=1
        else: EmptyCounter+=1
        TypedVoxels.append(TempTypedVoxel)
    
    """Check 2""" 
    print("Empty Counter:" + str(EmptyCounter))
    
    """Output Document"""
    for line in TypedVoxels:
        outputfile.write(str(line) + '\n')
    outputfile.close()
    
    X=[]; Y=[]; Z=[]; Output=[]
    for Voxel in TypedVoxels:
        if Voxel[Type+2]==1:
           X.append(Voxel[0]); Y.append(Voxel[1]); Z.append(Voxel[2])
    Output=[X,Y,Z]
    
    return Output
        
def GetDI(configInfo, ho_configInfo, Type, filein, intfile,distfile):
    [atoms, len_mol, Box, VBox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo    
    
    [ho_atoms, len_ho_mol, no_ho_mol, no_ho_A, no_ho_B, no_ho_C, ho_bonds] = ho_configInfo
    total_atoms = atoms + ho_atoms; total_mol = no_ho_mol + no_mol
    
    LBoxX=Box[1]-Box[0]; LBoxY=Box[3]-Box[2]; LBoxZ=Box[5]-Box[4]
    xdata = []; ydata = []; zdata = []; MasterList = []; counterA=0
    
    ToDelete=[0, 1, 2, 3, 4, 5]
    del ToDelete[Type+2]
    
    PositionFile = open(filepath + filein,'r')
    List = tb.FilteredTextToList(PositionFile)
    ConvertedList=np.asarray(List, dtype=int)
    Typed=np.delete(ConvertedList, ToDelete,1)
    Typed=Typed.reshape((VBox[0], VBox[1], VBox[2]))

    """INTERFACE USING GRAPH DATA"""
    # WHAT WORKS WELL: TypeFace = 1, threshold = 0.4 is better
    #Output: np array where 1=is interface and 0=not interface
    xdataI=[]; ydataI=[]; zdataI=[]
    InterfaceA = np.zeros((VBox[0], VBox[1], VBox[2]))
    for i in range(VBox[0]):
        for j in range(VBox[1]):
            for k in range(VBox[2]):
                isInterfaceCounter=0; counter=0
                if Typed[i,j,k] == 1:
                    for iadd in range(i-1, i+2):
                        for jadd in range (j-1, j+2):
                            for kadd in range (k-1, k+2):
                                counter+=1
                                if iadd<0: iadd+=VBox[0]
                                if jadd<0: jadd+=VBox[1]
                                if kadd<0: kadd+=VBox[2]
                                
                                if iadd>(VBox[0]-1): iadd-=VBox[0]
                                if jadd>(VBox[1]-1): jadd-=VBox[1]
                                if kadd>(VBox[2]-1): kadd-=VBox[2]
                                
                                iadd=int(iadd); jadd=int(jadd); kadd=int(kadd)
                                
                                if (Typed[iadd,jadd,kadd] != Typed[i,j,k]):
                                    isInterfaceCounter+=1
                if counter!=0 and isInterfaceCounter/float(counter) >= 0.45:
                    InterfaceA[i,j,k]=1
                    xdataI.append(i)
                    ydataI.append(j)
                    zdataI.append(k)
                else:
                    InterfaceA[i,j,k]=0
    intfc = open(filepath+intfile,'w')
    print (len(xdataI))
    for i in range(len(xdataI)):
        intfc.write("{} {} {}\n".format(xdataI[i],ydataI[i],zdataI[i]))
    intfc.close()
    print ('InterfaceA done', len(xdataI))
    # we like the settings >= 0.2 if comparing null voxels
    
    fig = plt.figure()
    ax = plt.axes(projection = '3d')
    #ax.scatter3D(xdata, ydata, zdata, c = "pink")
    ax.scatter3D(xdataI, ydataI, zdataI, c = "blue")
    ax.set_xlim(0,VBox[0]); ax.set_ylim(0, VBox[1]); ax.set_zlim(0,VBox[2])
    plt.xlabel('X'); plt.ylabel("Y"); plt.title('Interface')
    plt.show() 
    
    """Distance from Interface"""
    #Output: np array where value=distance of voxel from the nearest interface; saves matrix to file DtoI for use in matlab
    xdataD=[]; ydataD=[]; zdataD=[]; disdata=[];
    Distance = np.zeros((VBox[0], VBox[1], VBox[2])); maxdistance=0
    for i in range(VBox[0]):
        for j in range(VBox[1]):
            for k in range(VBox[2]):
                if Typed[i,j,k] == 1:
                    mindistance=max(LBoxX, LBoxY, LBoxZ); counter=0
                    for iadd in range(i-(VBox[0]/2), i+(VBox[0]/2)):
                        for jadd in range (j-(VBox[1]/2),j+(VBox[1]/2)):
                            for kadd in range (k-(VBox[2]/2), k+(VBox[2]/2)):
                                if iadd<0: iadd+=VBox[0]
                                if jadd<0: jadd+=VBox[1]
                                if kadd<0: kadd+=VBox[2]
                                if iadd>(VBox[0]-1): iadd-=VBox[0]
                                if jadd>(VBox[1]-1): jadd-=VBox[1]
                                if kadd>(VBox[2]-1): kadd-=VBox[2]
                                
                                iadd=int(iadd); jadd=int(jadd); kadd=int(kadd)
                                
                                if InterfaceA[iadd,jadd,kadd]==1:
                                    point1=[i,j,k]
                                    point2=[iadd,jadd,kadd]
                                    distance=(tb.distance(point1,point2)) #Rounds to nearest whole integer
                                    if distance<mindistance:
                                        mindistance=distance
                    Distance[i,j,k]=mindistance
                    xdataD.append(i)
                    ydataD.append(j)
                    zdataD.append(k)
                    disdata.append(mindistance)
                    if mindistance>maxdistance:
                        maxdistance=mindistance
                else:
                    Distance[i,j,k]=0 #Change to separate out voxels in from out
    dtoi = open(filepath+distfile,'w')
    for i in range(VBox[0]):
        for j in range(VBox[1]):
            for k in range(VBox[2]):
                dtoi.write("{0} {1} {2} {3}\n".format(i, j, k, Distance[i,j,k]))
    dtoi.close()
    scipy.io.savemat(filepath+'DtoI.mat', mdict={'arr': Distance})
    
    #Plot 3
    fig = plt.figure()
    ax = plt.axes(projection = '3d')
    #ax.scatter3D(xdata, ydata, zdata, c = "pink")
    ax.scatter3D(xdataD, ydataD, zdataD, c=disdata, cmap='gist_rainbow')
    ax.set_xlim(0,VBox[0]); ax.set_ylim(0, VBox[1]); ax.set_zlim(0,VBox[2])
    plt.xlabel('X'); plt.ylabel("Y"); plt.title('Distance from Interface')
    plt.show()
    print ('Distance done' )
    print('------------------'+'\n')
    
    return [InterfaceA,Distance, maxdistance]
    
def Ree_v_DI(config, ree_com, distance, maxdistance):
    [atoms, len_mol, Box, VBox, no_mol, no_A, no_B, no_C, bonds, name, filepath]=config
    Ree=[]; DI=[]
    for counter in range(int(math.floor(maxdistance))+1):
        DI.append(counter)
        Ree.append(0)
    
    for i in range(VBox[0]):
        for j in range(VBox[1]):
            for k in range(VBox[2]):
                Rcom=ree_com[i,j,k]
                D=int(round(distance[i,j,k]))
                Ree[D]+=1

        
    fig = plt.figure()
    plt.scatter(DI,Ree)
    plt.show()
                    