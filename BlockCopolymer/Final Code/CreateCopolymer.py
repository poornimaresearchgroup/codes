import random
import math

"""
INPUTS:
    Prompted: Number A Beads, Number B Beads, chiAB*N, chiAC*N, chiBC*N 
    Hard Coded: file path, file names, Lbox, aa, in.lammps file, Molecule Length
OUTPUTS:
    in.data, in.lammps, config.py
ASSUMPTIONS:
    DPD Density=3
    Linear Triblock Chains
    aa=25, bb=25, cc=25
"""

#Settings
filepath = 'C:/Users/nxb1090/Documents/Research/Final Code/Test/'
Lbox = 25
mol_len = 60
no_atoms = (Lbox * Lbox * Lbox * 3)
chain = math.ceil(no_atoms / mol_len)
bonds = chain * (mol_len - 1)
no_atoms = chain * mol_len

no_a = int(input('Number of A: '))
no_b = int(input('Number of B: '))
no_c = mol_len - no_a - no_b
name = str(no_a) + '-' + str(no_b) + '-' + str(no_c)

NchiAB = float(input('Value for chiAB*N: '));
NchiAC = float(input('Value for chiAC*N: '));
NchiBC = float(input('Value for chiBC*N: '));
chiAB = NchiAB / mol_len; chiAC = NchiAC / mol_len; chiBC = NchiBC / mol_len
titleline = (str(no_a) + '-' + str(no_b) + '-' + str(no_c) + '	' + 'ChiABN: ' + str(NchiAB) + '	' + 'ChiACN: ' + str(NchiAC) + '	' + 'ChiBCN: ' + str(NchiBC))

aa = 25; bb = aa; cc = aa; #Default pair coefficient
ab = aa + (3.27 * chiAB) #Flory-Huggins Equation
ac = aa + (3.27 * chiAC)
bc = bb + (3.27 * chiBC)


#In.data Atom Info
data = open(filepath+"in.data", "w+")
data.write("""{0}

{1} atoms
{2} bonds

3 atom types
1 bond types

0 {3} xlo xhi
0 {3} ylo yhi
0 {3} zlo zhi

Masses

1 1.0
2 1.0
3 1.0

Atoms

""".format(titleline,no_atoms, bonds,Lbox))
atom_count = 1
for i in range(int(chain)): #Randomly places first atom (Type 1) in chain
    x = random.random() * Lbox
    y = random.random() * Lbox
    z = random.random() * Lbox
    data.write("{} {} {} {} {} {}\n".format(atom_count, i + 1, 1, x, y, z))
    atom_count += 1
    for j in range(no_a - 1): #Creates number of Type A atoms specified
        thet = random.random() * 2 * math.pi
        phi = random.random() * 2 * math.pi #Creates random angle, steps that far, creates next atom
        x_ = math.sin(thet) * math.cos(phi)
        y_ = math.sin(thet) * math.sin(phi)
        z_ = math.cos(thet)
        x += x_
        y += y_
        z += z_
        data.write("{} {} {} {} {} {}\n".format(atom_count, i + 1, 1, x, y, z))
        atom_count += 1
    for f in range(no_b): #Creates number of Type B atoms specified
        thet = random.random() * 2 * math.pi
        phi = random.random() * 2 * math.pi
        x_ = math.sin(thet) * math.cos(phi)
        y_ = math.sin(thet) * math.sin(phi)
        z_ = math.cos(thet)
        x += x_
        y += y_
        z += z_
        data.write("{} {} {} {} {} {}\n".format(atom_count, i + 1, 2, x, y, z))
        atom_count += 1
    for g in range(no_c): #Creates number of Type C atoms specified
        thet = random.random() * 2 * math.pi
        phi = random.random() * 2 * math.pi
        x_ = math.sin(thet) * math.cos(phi)
        y_ = math.sin(thet) * math.sin(phi)
        z_ = math.cos(thet)
        x += x_
        y += y_
        z += z_
        data.write("{} {} {} {} {} {}\n".format(atom_count, i + 1, 3, x, y, z))
        atom_count += 1
        
#In.data Bond Info
data.write("""
Bonds

""")
bond_count = 1
bond_type = 1
for a in range(int(chain)):
    for g in range(mol_len): #While in chain, creates chain between (atom n) and (atom n+1)
        plus = mol_len * a
        if g != (mol_len - 1):
            data.write("{} {} {} {}\n".format(bond_count, bond_type, g + 1 + plus, g + 2 + plus))
            bond_count += 1
data.close()

#In.lammps writes
lammps = open(filepath+"in.lammps", "w+")
lammps.write("""units lj
atom_style angle
bond_style harmonic
pair_style dpd 1 1 285886

read_data in.data

comm_modify vel yes

velocity all create 1.0 123456789 rot no dist gaussian
pair_coeff 1 1 {} 4.5
pair_coeff 1 2 {} 4.5
pair_coeff 1 3 {} 4.5
pair_coeff 2 2 {} 4.5
pair_coeff 2 3 {} 4.5
pair_coeff 3 3 {} 4.5
bond_coeff 1 4 0

special_bonds lj 1.0 1.0 1.0

neighbor 1.0 bin
neigh_modify every 1 delay 0 check yes

thermo 1000
timestep 0.04

minimize 1.0e-4 1.0e-6 1000 10000
fix 1 all nve
fix 2 all recenter INIT INIT INIT shift all

compute 1 all rdf 50
fix myrdf all ave/time 1000 100 500000 c_1 file gr.txt mode vector

compute 2 all bond/local dist
dump 2 all local 1000 tmp.dump index c_2[*]

restart 100000 ReStart/polymer.*.restart
variable a loop 10
label fun
dump 3 all custom 1000 Trj/bonds.${}.lammpstrj id mol type x y z ix iy iz
run 100000
undump 3
next a
jump in.lammps fun
""".format(aa, ab, ac, bb, bc, cc, "{a}"))
lammps.close()

#Config.py Writing
config = open(filepath+"config.py", "w+")
config.write("""atoms = {}
len_mol = {}
Lbox = {}; Vbox = Lbox**3
no_mol = {}
no_A = {}; no_B = {}; no_C = {}
bonds = {}
name = '{}'
filepath = '{}' """.format(no_atoms, mol_len, Lbox, chain, no_a, no_b, no_c, bonds, name, filepath+name))
config.close()