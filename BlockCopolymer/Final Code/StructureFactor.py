import ToolBox as tb
from config import *
import math
from operator import itemgetter


""" SECTION 1: Settings"""
start = 1 # Line to start on
stepsize = 1 #stepsize for q coordinate generation
Timesteps = 10 #number of timesteps from trj file to be averaged
Output = []
header = 9 #header length of file

"""SECTION 2: File Paths"""
inputfilename = filepath + 'Doc.txt'
inputfile = open(inputfilename, 'r')
outputfile1 = open(filepath + 'SqEntireA.txt', 'w+')
outputfile1C = open(filepath + 'SqEntireC.txt','w+')
QListfile = open(filepath + 'QList.txt', 'w+')
SqAfile = open(filepath + 'SqA.txt','w+')
SqCfile = open(filepath + 'SqC.txt','w+')
SqTfile = open(filepath + 'SqT.txt','w+')

"""SECTION 3: Text to List"""
Atomlist = tb.listmaker(inputfile)

"""SECTION 4: Removes lines starting with text"""
for k in range (atoms):
    if tb.is_number(Atomlist[header + k][0]) != False: #if first character of temp a number added to List
        float_line = tb.convert(Atomlist[header + k])
        outputline = ((float_line))
        Output.append(outputline)
        
"""SECTION 5: Finds Structure Factor for Valid Wave Vectors"""

## Make set of viable wave vectors ##
qmin=2 * math.pi / Lbox; qmax = 2 * math.pi
MasterQ = []; TotalXA = []; TotalYA = []; TotalXC = []; TotalYC = []
TotalBetaA = []; TotalThetaA = []; TotalBetaC = []; TotalThetaC = []
for nx in range(int(Lbox / stepsize)): #Generates valid wave vectors
    qx= tb.q(nx, Lbox) * stepsize
    print('qx: ' + str(qx)) #prints for progress tracking
    for ny in range(int(Lbox / stepsize)):
        qy = tb.q(ny, Lbox) * stepsize
        for nz in range(int(1), int(Lbox / stepsize)):
            qz = tb.q(nz, Lbox) * stepsize
            ql = ((qx ** 2) + (qy ** 2) + (qz ** 2)) ** 0.5
            if ql >= qmin and ql <= qmax:
                q_info = [qx, qy, qz, ql]
                QListfile.write(str(q_info) + '\n')
                MasterQ.append(q_info)
                TotalXA.append(0.0); TotalXC.append(0.0)
                TotalYA.append(0.0); TotalYC.append(0.0)
                TotalBetaA.append(0.0); TotalBetaC.append(0.0)
                TotalThetaA.append(0.0); TotalThetaC.append(0.0)
QListfile.close()
##
MasterA = []; YA = []; XA = []
ValuesA = [XA, YA, MasterA]
MasterC = []; YC = []; XC = []
ValuesC = [XC, YC, MasterC]


start=0; end=atoms-1
#For current timestep
for step in range(Timesteps):
    print(step); #prints for progress tracking
    #Makes a set of atoms only from the timestep, and adjust counters for next timestep
    CurrentOutput=Output[start:end]
    start=end; end=end+(atoms)
    counter=0
    #For value of q
    for k in MasterQ:
        q_info = k
        CosSumA = 0; SinSumA = 0; current_A = 0;
        CosSumC = 0;  SinSumC = 0; current_C = 0;
        #Progress report
        if counter % 1000 == 0:
            print(counter) #prints for progress tracking
        counter += 1
        #For each atom
        for line in range(len(CurrentOutput)):
            Test = CurrentOutput[line]
            [AtomX, AtomY, AtomZ, Type] = [Test[3], Test[4], Test[5], Test[2]] #Atom info
            Atom_info = [AtomX, AtomY, AtomZ]
            #Input = q_info, atom, trig_totals, count; return [cossum, sinsum, count]
            if Type == 1.0: #Type A Atoms
                [CosSumA, SinSumA, current_A] = tb.structure_for_atom(q_info, Atom_info, [CosSumA, SinSumA], current_A)
            if Type == 3.0: #Type C Atoms
                [CosSumC, SinSumC, current_C] = tb.structure_for_atom(q_info, Atom_info, [CosSumC, SinSumC], current_C)
        #Input: type_info, file, atoms, q_info, lists
        [XA,YA,MasterA] = tb.structure_values([CosSumA, SinSumA, current_A], outputfile1, atoms, q_info, [XA,YA,MasterA])
        [XC,YC,MasterC] = tb.structure_values([CosSumC, SinSumC, current_C], outputfile1C, atoms, q_info, [XC,YC,MasterC])
        
        #[X, Y, master] = [ql, structure, [qx,qy,qz,ql,struct,beta,theta]
        
        #Add current structure to current total of structure; keep X the same
        TotalYA[counter - 1] += YA[counter - 1]
        TotalXA[counter - 1] = XA[counter - 1]
        TotalBetaA[counter - 1] += MasterA[counter - 1][4]
        TotalThetaA[counter - 1] += MasterA[counter - 1][5]

        TotalYC[counter - 1] += YC[counter - 1]
        TotalXC[counter - 1] = XC[counter - 1]
        TotalBetaC[counter - 1] += MasterC[counter - 1][4]
        TotalThetaC[counter - 1] += MasterC[counter - 1][5]


BetaA = []; BetaC = []; ThetaA = []; ThetaC = []; TuplesA = []; TuplesC = []
#Divide structure by total number of atoms,and replace value in Y list
for j in range(len(TotalYA)):
    value = TotalYA[j]; xvalue = TotalXA[j]
    TotalYA[j] = value / (Timesteps * atoms)
    Coord = (xvalue, value / (Timesteps * atoms))
    
    TotalBetaA[j] = (TotalBetaA[j] / (Timesteps * atoms))
    TotalThetaA[j] = (TotalThetaA[j] / (Timesteps * atoms))
    
    TuplesA.append([Coord, TotalBetaA[j], TotalThetaA[j]])

for j in range(len(TotalYC)):
    value = TotalYC[j]; xvalue = TotalXC[j]
    TotalYC[j] = value / (Timesteps * atoms)
    Coord = (xvalue, value / (Timesteps * atoms))
    
    TotalBetaC[j] = (TotalBetaC[j] / (Timesteps * atoms))
    TotalThetaC[j] = (TotalBetaC[j] / (Timesteps * atoms))
    
    TuplesC.append([Coord, TotalBetaC[j], TotalThetaC[j]])
    
TA = sorted(TuplesA, key=lambda x:float(x[0][0])) #sorts by ql value
TC = sorted(TuplesC, key=lambda x:float(x[0][0]))   
    
XAvgSqA = []; YAvgSqA = []; XAvgSqAT = []; YAvgSqAT = [];
XAvgSqC = []; YAvgSqC = []; XAvgSqCT = []; YAvgSqCT = [];
AvgBetaA = []; AvgThetaA = []; AvgBetaC = []; AvgThetaC = [];

c = 0 #counter
max_y = 0
for x in range(len(MasterQ)): #For each wave vector
    flag = True
    if c >= len(MasterQ): #breaks if beyond list
                flag = False
                break
    check = TA[c][0][0] #Takes value of ql
    tempList = []
    currentY = 0;
    while flag: 
        if c >= len(MasterQ): #breaks if beyond list
            flag = False   
        if TA[c][0][0] == check: #If has the same value of ql
            currentY += TA[c][0][1]
            tempList.append(TA[c][0]) #Appends tuple to a List
            c += 1
            if c >= len(MasterQ): #breaks if beyond list
                flag = False
        else:
            flag = False #breaks if error
            break
    if len(tempList) == 0: #if no values at ql, sets it to 0
        currentY = 0
    else:
        currentY = currentY / (len(tempList)) #averages
    XAvgSqA.append(check)
    YAvgSqA.append(currentY)
    XAvgSqAT.append(check)
    YAvgSqAT.append(currentY)
    
    AvgBetaA.append(TA[c-1][1])
    AvgThetaA.append(TA[c-1][2])
    
    if currentY > max_y: #updates max y value found
        max_y = currentY

for value in range(len(YAvgSqA)): #divides by largest y value to normalize
    YAvgSqA[value] /= max_y
    SqAfile.write(str(XAvgSqA[value]) + '    ' + str(YAvgSqA[value]) + '    ' + str(AvgBetaA[value]) + '    ' + str(AvgThetaA[value]) + '\n')
    
max_y=0; c=0 #repeats above process for Type C atoms 
for x in range(len(MasterQ)):
    flag = True
    if c >= len(MasterQ):
        flag = False
        break
    check = TC[c][0][0]
    tempList = []
    currentY = 0
    while flag:
        if c >= len(MasterQ):
                break
        if TC[c][0][0] == check:
            currentY += TC[c][0][1]
            tempList.append(TC[c])
            c += 1
            if c >= len(MasterQ):
                flag = False   
        else:
            flag = False
    if len(tempList) == 0: #if no values at ql, sets it to 0
        currentY = 0
    else:
        currentY = currentY / len(tempList)
    XAvgSqC.append(check)
    YAvgSqC.append(currentY)
    XAvgSqCT.append(check)
    YAvgSqCT.append(currentY)
    
    if currentY > max_y:
        max_y = currentY 
    
    AvgBetaC.append(TC[c - 1][1])
    AvgThetaC.append(TC[c - 1][2])
    
for entry in range (len(YAvgSqC)):
    YAvgSqC[entry] /= max_y     
    SqCfile.write(str(XAvgSqC[entry]) + '    ' + str(YAvgSqC[entry]) + '    ' + str(AvgBetaC[entry]) + '    ' + str(AvgThetaC[entry]) + '\n')
    
XAvgSqT = []; YAvgSqT = [];
c = 0
max_y = 0
#Averages SqA and SqC values before they were normalized
for x in range(len(YAvgSqC)): #For each wave vector
    check = (XAvgSqAT[x] == XAvgSqCT[x])
    if check == False: #breaks if not on same ql value
        print ("ERROR")
        break
    else:
        YAvgSqT.append((float(YAvgSqAT[x]) + float(YAvgSqCT[x])))
        XAvgSqT.append(XAvgSqAT[x])
        if (float(YAvgSqAT[x]) + float(YAvgSqCT[x])) > max_y: #updates max y value
            max_y = (float(YAvgSqAT[x]) + float(YAvgSqCT[x]))
            
for entry in range (len(YAvgSqT)): #normalizes values by max y
    YAvgSqT[entry] /= max_y     
    SqTfile.write(str(XAvgSqT[entry]) + '    ' + str(YAvgSqT[entry]) + '\n')

"""SECTION 6: Close Files"""
outputfile1.close()
outputfile1C.close()
inputfile.close()
SqAfile.close()
SqCfile.close()
SqTfile.close()