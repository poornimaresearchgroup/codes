import random
import math

"""
INPUTS:
    Prompted: Molecule Length
    Hard Coded: file path, file names, Lbox, aa, in.lammps file
OUTPUTS:
    in.data file, in.lammps file, config file
ASSUMPTIONS:
    DPD Density=3
    Linear Polymer Chains
    aa=25
"""

#Settings
filepath = 'C:/Users/nxb1090/Documents/Research/Final Code/Test/'
Lbox = 25
mol_len = int(input('Molecule Length: '))
name = str(mol_len)
no_atoms = (Lbox * Lbox * Lbox * 3) #Assumes DPD density of 3
chain = math.ceil(no_atoms / mol_len) #Number of chains needed to meet, but not exceed DPD density
bonds = chain * (mol_len-1) #Assumes linear polymer
no_atoms = chain * mol_len

aa = 25 #default pair coefficient

#In.data Writes
data = open(filepath+"in.data", "w+")
data.write("""

{0} atoms
{1} bonds

1 atom types
1 bond types

0 {2} xlo xhi
0 {2} ylo yhi
0 {2} zlo zhi

Masses

1 1.0

Atoms

""".format(no_atoms, bonds, Lbox))
#In.data Atom Initial Position
atom_count = 1
for i in range(int(chain)): #Random position for first atom in chain
    x = random.random() * Lbox
    y = random.random() * Lbox
    z = random.random() * Lbox
    data.write("{} {} {} {} {} {}\n".format(atom_count, i + 1, 1, x, y, z))
    atom_count += 1
    for j in range(mol_len-1): #For rest of chain, picks a random angle, steps out that far, and creates next atom
        thet = random.random() * 2 * math.pi
        phi = random.random() * 2 * math.pi
        x_ = math.sin(thet) * math.cos(phi)
        y_ = math.sin(thet) * math.sin(phi)
        z_ = math.cos(thet)
        x += x_
        y += y_
        z += z_
        data.write("{} {} {} {} {} {}\n".format(atom_count, i + 1, 1, x, y, z))
        atom_count += 1
#In.data Bond Info
data.write("""
Bonds

""")
bond_count = 1
bond_type = 1
for a in range(int(chain)):
    for g in range(mol_len): #While inside a chain, write bonds between (atom n) and (atom n+1)
        plus = mol_len * a
        if g != (mol_len - 1):
            data.write("{} {} {} {}\n".format(bond_count, bond_type, g + 1 + plus, g + 2 + plus))
            bond_count += 1
data.close()

#In.lammps Writing
lammps = open(filepath+"in.lammps", "w+")
lammps.write("""units lj
atom_style angle
bond_style harmonic
pair_style dpd 1 1 285886

read_data in.data

comm_modify vel yes

velocity all create 1.0 123456789 rot no dist gaussian
pair_coeff 1 1 {} 4.5
bond_coeff 1 4 0

special_bonds lj 1.0 1.0 1.0

neigh_modify every 1 delay 0 check yes

thermo 100
timestep 0.04

minimize 1.0e-4 1.0e-6 1000 10000
fix 1 all nve
fix 2 all recenter INIT INIT INIT shift all 

compute 1 all rdf 50
fix myrdf all ave/time 1000 100 1000000 c_1 file gr.txt mode vector

compute 2 all bond/local dist
dump 2 all local 1000 tmp.dump index c_2[*]

restart 100000 ReStart/polymer.*.restart
variable a loop 10
label fun
dump 3 all custom 1000 Trj/bonds.${}.lammpstrj id mol type x y z ix iy iz
run 100000
undump 3
next a 
jump in.lammps fun
""".format(aa, "{a}"))
lammps.close()

#Config.py Writing
config = open(filepath+"config.py", "w+")
config.write("""atoms = {}
len_mol = {}
Lbox = {}; Vbox = Lbox**3
no_mol = {}
bonds = {}
name = '{}'
filepath = '{}' """.format(no_atoms, mol_len, Lbox, chain, bonds, name, filepath+name))
config.close()