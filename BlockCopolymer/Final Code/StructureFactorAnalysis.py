import matplotlib.pyplot as plt
import ToolBox as tb
from config import *
import math
from operator import itemgetter

""" SECTION 1: Settings"""
start = 1 # Line to start on
stepsize = 1 #stepsize for q coordinate generation
AListFloat = []; CListFloat = []; TListFloat = []
XAvgSqA = []; YAvgSqA = []
XAvgSqC = []; YAvgSqC = []
XAvgSqT = []; YAvgSqT = []


"""SECTION 2: File Paths"""
inputfilename = filepath + 'SqA.txt'
inputfilenameC = filepath + 'SqC.txt'
inputfilenameT = filepath + 'SqT.txt'
inputfile = open(inputfilename, 'r')
inputfileC = open(inputfilenameC, 'r')
inputfileT = open(inputfilenameT, 'r')

#File to Lists
Structurelist = tb.listmaker(inputfile)
StructurelistC = tb.listmaker(inputfileC)
StructurelistT = tb.listmaker(inputfileT)


#Seperate qls and structure values
for k in range (len(Structurelist)):
    float_line = tb.convert(Structurelist[k])
    AListFloat.append([float_line[0], float_line[1]]); 
    XAvgSqA.append(float_line[0])
    YAvgSqA.append(float_line[1])

for j in range (len(StructurelistC)):
    float_lineC = tb.convert(StructurelistC[j])    
    CListFloat.append([float_lineC[0], float_lineC[1]])
    XAvgSqC.append(float_lineC[0])
    YAvgSqC.append(float_lineC[1])
    
for h in range (len(StructurelistT)):
    float_lineT = tb.convert(StructurelistT[h])    
    TListFloat.append([float_lineT[0], float_lineT[1]])
    XAvgSqT.append(float_lineT[0])
    YAvgSqT.append(float_lineT[1])

#Sort by Sq Value from highest to lowest
AList = sorted(AListFloat, key = itemgetter(1), reverse = True)
CList = sorted(CListFloat, key = itemgetter(1), reverse = True)
TList = sorted(TListFloat, key = itemgetter(1), reverse = True)


"""SECTION 7: Graphs"""
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams.update({'font.size': 16})   
fig = plt.figure(1)
plt.title('Structure Factor Type A')
plt.xlabel('Wave  number (q)')
plt.ylabel('Structure Factor (Scaled)')
ax = fig.add_subplot(111)
plt.scatter(XAvgSqA, YAvgSqA, c = 'Red', alpha = 0.5)
plt.yscale('log')
plt.ylim(10E-6, 1.5)
plt.xlim(0, 2 * math.pi)
# Draw arrows on selected points
# For xytext, x=x of point, y=y of point plus 3 to second sig fig
# ax.annotate('', xy=(0.3554, 1), xytext=(0.3554, 1.3),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(0.6156, 0.2093), xytext=(0.6156, 0.2393),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(0.7109, 0.2155), xytext=(0.7109, 0.2455),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#               )
# ax.annotate('', xy=(1.0663, 0.02637), xytext=(1.0663, 0.02937),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(1.3059, 0.002910), xytext=(1.3059, 0.003210),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
#plt.savefig(filepath+"AvgSqA.png")
plt.show()

fig = plt.figure(2)
plt.title('Structure Factor Type C')
plt.xlabel('Wave  number (q)')
plt.ylabel('Structure Factor (Scaled)')
ax = fig.add_subplot(111)
plt.scatter(XAvgSqC, YAvgSqC, c = 'Cyan', alpha = 0.5)
plt.yscale('log')
plt.ylim(10E-6, 1.5)
plt.xlim(0, 2 * math.pi)
# ax.annotate('', xy=(0.3554, 1), xytext=(0.3554, 1.3),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(0.4353, 0.1771), xytext=(0.4353, 0.2071),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(0.6156, 0.1027), xytext=(0.6156, 0.1327),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#               )
# ax.annotate('', xy=(0.7109, 0.1391), xytext=(0.7109, 0.1691),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(0 .7540, 0.03190), xytext=(0.7540, 0.03490),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
#plt.savefig(filepath+"AvgSqC.png")
plt.show()

fig = plt.figure(3)
plt.title('Structure Factor Type Total')
plt.xlabel('Wave  number (q)')
plt.ylabel('Structure Factor (Scaled)')
ax = fig.add_subplot(111)
plt.scatter(XAvgSqT, YAvgSqT, c = 'Gray', alpha = 0.5)
plt.yscale('log')
plt.ylim(10E-6, 1.5)
plt.xlim(0, 2 * math.pi)
# ax.annotate('', xy=(0.3554, 1), xytext=(0.3554, 1.3),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(0.4353, 0.1909), xytext=(0.4353, 0.2209),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(0.6156, 0.1420), xytext=(0.6156, 0.1720),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#               )
# ax.annotate('', xy=(0.7109, 0.1673), xytext=(0.7109, 0.1973),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
# ax.annotate('', xy=(1.0663, 0.01230), xytext=(1.0663, 0.01530),
#             arrowprops=dict(facecolor='black', arrowstyle="-|>"),
#             )
#plt.savefig(filepath+"AvgSqT.png")
plt.show()

#Print local minimums for bin size selected
step = math.pi / 100 #size of area to look for local min
numsteps = int(2 * math.pi / step)
lower = 0
for value in range(numsteps):
    upper = (value + 1) * step
    TempList = []
    for coord in AList: #Which graph you are adding points to
        if coord[0] > lower and coord[0] <= upper:
            TempList.append(coord)
    TempList = sorted(TempList, key = itemgetter(1), reverse = True)
    lower = upper
    if len(TempList) > 0:
        print(TempList[0])
    
    