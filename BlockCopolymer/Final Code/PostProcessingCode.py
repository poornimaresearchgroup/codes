# import matplotlib
# matplotlib.use("Agg") #allows figures to be saved when using ion
import matplotlib.pyplot as plt
import numpy as np 
import ToolBox as tb
import math
from operator import itemgetter


def BondLength(filename, name, head, span, Index, color, text_info, configInfo):
    """ Returns value of x for mode bond length.
    Save a graph of the distribution of bond length
    Inputs: header length of shorttmp.txt, number of timesteps"""

    """ SECTION 2: Variables and Unpacking"""
    Output = []; Mean=0; Counter=0
    [StartIndex, EndIndex] = Index
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, tempfilepath] = configInfo
    
    """ SECTION 3: Input File """
    BDistfile = str(filename)#input(" Bond Distance File -->")
    BDistData = open(BDistfile,'r')
    BondList = tb.listmaker(BDistData)
    print(name)
    
    """ SECTION 4: Code """
    # grabs only the bonds and appends them to a new list for as many timesteps as you wanted
    for i in range(span):
        start = head + ((bonds + head) * i)
        for k in range (bonds):
            bond_number = ((k + 1) % (len_mol))
            if bond_number >= StartIndex and bond_number <= EndIndex:
                target = start + k
                temp = BondList[(target)]
                if tb.is_number(temp[0]):
                    Output.append(float(temp[-1]))
                    Mean += float(temp[-1])
                    Counter += 1
                    
    """ SECTION 5: Graph """ 
    #Creates bins for histogram that are defined by width   
    bin_size = 0.05 #bin width
    currentbin = 0
    binlist = []
    while currentbin < 4: #4 is max on x axis
        binlist.append(currentbin)
        currentbin += bin_size
    # num_bins = 100
    
    #Creates a normalized histogram
    plt.figure(1)
    y,x,_ = plt.hist(Output, binlist, label = name, alpha = 0.5, color = color, normed = True)
    max_y = (max(y)); 
    yindex = (np.where(y == max_y))
    
    mode = float(x[yindex]);
    print("Mode: "+str(mode))
    max_x = Mean/Counter
    print("Numpy Mean: "+str(np.mean(Output)))
    plt.axis([0, 2, 0.0, 3])
    text = plt.text(text_info[0], text_info[1], 'Mean: ' + str(tb.float_round(max_x, 4)), wrap = True, backgroundcolor = color) #Text Box with Mean in it
    text.set_bbox(dict(facecolor = color, alpha = 0.5)) #Color for text box
    plt.legend(prop = {'size': 12}) #font size for legend
    plt.xlabel("Bond Length")
    plt.ylabel("Frequency (normalized)")
    plt.title("Bond Length Distribution")
    plt.show()

    print(('Standard Deviation: '+str(np.std(x))))
    print('Max Bond Distance: '+str(max(x))+'\n')
    return max_x
    
    
def Ree(head, Index, filename, graphData, len_mol_div, configInfo):
    """Calculates the average end to end distance and a histogram of the distribution
        Input: head(header in Ree.txt), Index(start,end), graph(text to print)
        Hardcoded: Histogram bin size, axis ranges, number of timsteps used
        Output: prints histogram, has no return
        Assumptions: linear polymer
        """
    
    Output=[]
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    [StartIndex, EndIndex] = Index
    [graph, text_space, color] = graphData
    span = 1 # How many timesteps you want to report
    # end settings

    """ SECTION 3: Input File """
    Reefile = (filename)
    Reefile = open(str(Reefile),'r')
    AtomList = tb.listmaker(Reefile)
    
    """ SECTION 4: Code """
    # grabs only the atom info and appends them to a new list for as many timesteps as you wanted
    Sum = 0; X = [];
    for i in range(span):
        Output = []
        if i == 0:
            start = head + 1
        else:
            start = head + (atoms + head) * i
        for k in range (atoms):
            target = start + k - 1
            temp = AtomList[target]
            temp = [float(x) for x in temp]
            if tb.is_number(temp[0]):
                Output.append(temp)
        Output.sort(key=lambda x: x[0]) #sorts by atom number

        for molcounter in range(no_mol):
            #Makes sure atoms are in the box
            indexstart = (molcounter * len_mol) + StartIndex
            [x1, y1, z1] = [tb.wrap(Output[indexstart][3], Lbox), tb.wrap(Output[indexstart][4], Lbox), tb.wrap(Output[indexstart][5], Lbox)]

            indexend = indexstart + EndIndex
            [x2, y2, z2] = [tb.wrap(Output[indexend][3], Lbox), tb.wrap(Output[indexend][4], Lbox), tb.wrap(Output[indexend][5], Lbox)]
            
            #wraps atoms if necessary
            x2 = tb.adjust(x1, x2, Lbox)
            y2 = tb.adjust(y1, y2, Lbox)
            z2 = tb.adjust(z1, z2, Lbox)
            
            #Finds distance between points
            deltax = (x2 - x1) * (x2 - x1)
            deltay = (y2 - y1) * (y2 - y1)
            deltaz = (z2 - z1) * (z2 - z1)

            Rsq = deltax + deltay + deltaz
            X.append(Rsq / (len_mol_div))
            Sum += Rsq / len_mol


    #Creates bins for histogram of specified width
    bin_size = .1 #width of bins
    currentbin = 0
    binlist = []
    while currentbin < 4: #4 is the max value of x axis
        binlist.append(currentbin)
        currentbin += bin_size

    plt.axis([0, 4, 0.0, 1.5]) #(minX, maxX, minY, maxY)
    y,x,_ = plt.hist(X, binlist, alpha=0.5, label=graph, normed=True, color=color)
    
    mean = np.mean(X)
    max_y = (max(y));
    yindex = (np.where(y == max_y))
    yindex = yindex[0][-1]
    mode = float(x[yindex]); 
    
    plt.xlabel('Ree/N (lj)'); plt.ylabel('Frequency (normalized)')
    text = plt.text(text_space[0], text_space[1], 'Mean: ' + str(round(mean,4)))
    text.set_bbox(dict(facecolor = color, alpha = 0.5))
    plt.legend()
    plt.show()
    print(str(graph) + ': ' + str(tb.avg(X)))
    print("Mode: " + str(mode))
    print(('Standard Deviation: ' + str(np.std(X))))
    print("Max Distance: " + str(max(X)) + '\n')
    
def VoxelStructurewithVolFrac(Vox, start, configInfo):
    """Creates Document of # of atoms in Voxel.
    Using cutoff densities, each voxel can only be 1 type of atom
    Assumptions: Cubic Voxel
    Inputs: edge length of voxel, start line of Ree file
    Outputs: prints volume fraction information, creates files for use in MatLab
    """
    
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    filepath = 'C:/Users/nxb1090/Documents/Research/4Round/Full Chi/15-30-15/'
    
    mult = float(1 / Vox)
    limit = int(Lbox * mult)
    inputfilename = 'Ree.txt' #Ree.txt'
    inputfile = open(filepath+inputfilename, 'r')
    outputfile = open(filepath+'FromPosition.txt', 'w+')
    outputfile2 = open(filepath+'FromPositionWithVol.txt', 'w+')
    
    """SECTION 3: Text to List"""
    AtomList = [];
    AtomList = tb.listmaker(inputfile)
    
    """SECTION 4: Removes lines starting with text"""
    Output = [];
    for k in range (atoms):
        target = start + k
        temp = [float(x) for x in AtomList[target]]
        if tb.is_number(temp[0]): #if first character of temp a number added to List
            Output.append(temp)
    MasterList = [];
    for x in range(limit):
        for y in range(limit):
            for z in range(limit):
                List=[x, y, z, 0, 0, 0]
                MasterList.append(List)
    
    """SECTION 5: Atom -> Box"""
    counter=0
    for line in Output:
        #Scale atom coordinates to match Voxel coordinates
        AtomX, AtomY, AtomZ, Type = [int(math.floor(line[3]) * mult), int(math.floor(line[4]) * mult),int(math.floor(line[5]) * mult), line[2]]
        counter += 1
        for box in range(len(MasterList)): #Runs through list of Voxel,supplying coords
            CurrentVoxel = MasterList[box]
            CurrentX = CurrentVoxel[0]; CurrentY = CurrentVoxel[1]; CurrentZ = CurrentVoxel[2]
            ACount = CurrentVoxel[3]; BCount = CurrentVoxel[4]; CCount = CurrentVoxel[5]
            if CurrentX == AtomX and CurrentY == AtomY and CurrentZ == AtomZ: #If atom in Current Voxel
                if counter % 1000 == 0: #Prints atom counter for progress monitoring
                    print(counter)
                del(MasterList[box]) #Deletes current Voxel info
                if Type == 1:
                    ACount += 1
                elif Type == 2:
                    BCount += 1
                else:
                    CCount += 1
                List=[CurrentX, CurrentY, CurrentZ, ACount, BCount, CCount]
                MasterList.append(List) #Adds updated Voxel info
    
    """SECTION 6: Volume Fractions"""
    MasterVolList = [];
    VolA = 0; VolB = 0; VolC = 0; VolAB = 0; VolBC = 0; VolAC = 0; VolE = 0; V = Vox ** 3
    for entry in MasterList: 
        #Info from current Voxel
        CurrentX = entry[0]; CurrentY = entry[1]; CurrentZ = entry[2]
        ACount = entry[3]; BCount = entry[4]; CCount = entry[5]
        fA = ACount / V; fB = BCount / V; fC = CCount / V
        if fA > fB and fA > fC and fA > (3 * no_A / len_mol) * V: #if fA most prominant and higher than expected
            List = [CurrentX, CurrentY, CurrentZ, fA, 0, 0]
            VolA += 1
        elif fB > fA and fB > fC and fB > (3 * no_B / len_mol) * V:
            List = [CurrentX, CurrentY, CurrentZ, 0, fB, 0]
            VolB += 1
        elif fC > fA and fC > fB and fC > (3 * no_C / len_mol) * V:
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolC += 1
        elif fA == fB: #if volume fractions of multiple types is equal
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolAB += 1
        elif fB == fC:
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolBC += 1
        elif fA == fC:
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolAC += 1
        else:
            List = [CurrentX, CurrentY, CurrentZ, 0, 0, 0]
            VolE += 1
        MasterVolList.append(List)
    
    MasterList = sorted(MasterList, key = itemgetter(0))
    MasterVolList = sorted(MasterVolList, key = itemgetter(0))
    
    Vbox = len(MasterVolList)
    Expected = [no_A / len_mol, no_B / len_mol, no_C / len_mol]
    Actual = [VolA / Vbox, VolB / Vbox, VolC / Vbox]
    Error = [tb.percent_error(Expected[0], Actual[0]), tb.percent_error(Expected[1], Actual[1]), tb.percent_error(Expected[2], Actual[2])]
    
    #prints Volume Fraction Information
    print("""Expected A: {}; Frac={}; Error={}%
Expected B: {}; Frac: {}; Error: {}%
Expected C: {}; Frac: {}; Error: {}%
VolAB: {}; VolBC: {}; VolAC: {}
Expected Empty Volume: {}; Frac: {}""".format(Expected[0], Actual[0], Error[0], Expected[1], Actual[1], Error[1], Expected[2], Actual[2], Error[2], VolAB / Vbox, VolBC / Vbox, VolAC / Vbox,0,VolE / Vbox))
    
    """SECTION 7: Write Results to OutoutFile for MatLab Analysis"""
    for data in MasterList:
        outputfile.write(str(data) + '\n')
    for Voldata in MasterVolList:
        outputfile2.write(str(Voldata) + '\n')
    
    """SECTION 8: Close Files"""
    outputfile.close()
    outputfile2.close()
    inputfile.close()

def Part2StructureFactor(start, stepsize, inputfiles, configInfo):
    """
    WARNING: Only works for single Sq data. No averaging can be done on the Sq for this to work
    Calculates the concentration profile using structure factor info 1
    Inputs: start (header of Ree file) and stepsize (should match size of voxel)
    Outputs 2 Documents that can be used for MatLab Visualization
    """
    [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, filepath] = configInfo
    [file1, file2] = inputfiles       
                            
    """SECTION 2: File Paths"""
    inputfile1 = open(filepath + file1, 'r')
    inputfile2 = open(filepath + file2, 'r')
    
    outputfile2 = open(filepath+'DensityA.txt', 'w+')
    outputfile2C = open(filepath+'DensityC.txt', 'w+')
    
    """SECTION 3: Text to List"""
    QMaster = tb.TextToList(inputfile1)
    QMasterC = tb.TextToList(inputfile2)
    
    """SECTION 4: Concentration Profile"""
    rhoStarA = (no_A / len_mol) * 3 #expected density
    rhoStarC = (no_A / len_mol) * 3 
    counter = 0
    for x in range(int(Lbox / stepsize)):
        AtomX = x * stepsize
        print(AtomX) #for progress tracking
        for y in range(int(Lbox / stepsize)):
            AtomY = y * stepsize
            for z in range(int(Lbox / stepsize)): #Generates position in box
                AtomZ = z * stepsize
                counter += 1
                SumA = 0; SumC = 0
                for qValue in QMaster:
                    SumA = tb.rhofind(qValue, [AtomX, AtomY, AtomZ], SumA, Lbox)
                rhoR = rhoStarA * (1 + SumA)
                outputfile2.write("{} {} {} {} {}\n".format(counter, AtomX, AtomY, AtomZ, rhoR))
    
                for qValueC in QMasterC:
                    SumC = tb.rhofind(qValueC, [AtomX, AtomY, AtomZ], SumC, Lbox)
                rhoRC = rhoStarC * (1 + SumC)
                outputfile2C.write("{} {} {} {} {}\n".format(counter, AtomX, AtomY, AtomZ, rhoRC))
    print("DONE")
    
    """SECTION 5: Files Close"""
    inputfile1.close()
    inputfile2.close()
    outputfile2.close()
    outputfile2C.close()