import ToolBox as tb
import PostProcessingCode as code
from config import *
import matplotlib.pyplot as plt

plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams.update({'font.size': 16})    

"""BOND LENGTH"""
filepath = 'C:/Users/nxb1090/Documents/Research/4Round/Full Chi/15-30-15/'
filename = filepath + 'shorttmp.txt'

plt.figure(1)
head = 1 # Length of the header section
span = 1  # How many timesteps you want to report

Index = [0, len_mol - 1] #StartIndex, EndIndex
name = 'Entire Copolymer'
color = "Gray"
textlocation = [1.3, 1.6]
code.BondLength(filename, name, head, span, Index, color, textlocation, configInfo)

Index = [0, no_A - 1]
name = 'Type A'
color = "Cyan"
textlocation = [1.25, 1.1]
code.BondLength(filename, name, head, span, Index, color, textlocation, configInfo)

Index = [no_A + 1, no_A + no_B - 1]
name = 'Type B'
color = "Blue"
textlocation = [1.25, 1.7]
code.BondLength(filename, name, head, span, Index, color, textlocation, configInfo)

Index = [no_A + no_B + 1, no_A + no_B + no_C - 1]
name='Type C'
color="Purple"
textlocation=[1.25,1.4]
code.BondLength(filename, name, head, span, Index, color, textlocation, configInfo)

"""Homopolymers"""
atoms = 46890
len_mol = 60
Lbox = 25; Vbox = Lbox ** 3
no_A = 60; no_B = 0;
no_C = len_mol - no_A - no_B
no_mol = int(atoms / len_mol)
bonds = int(no_mol * (len_mol - 1))
filepath = 'C:/Users/nxb1090/Documents/Research/Chi100/Homo/60/'
filename = filepath + 'shorttmp.txt'             
head = 1 # Length of the header section
span = 1  # How many timesteps you want to report
Index = [0, len_mol - 1]
name = 'Homopolymer N=60'
color = "Goldenrod"
textlocation = [1.25, .8]
Info = [atoms, len_mol, Lbox, Vbox, no_mol, no_A, no_B, no_C, bonds, name, filepath]
code.BondLength(filename, name, head, span, Index, color, textlocation, Info)


from config import *

"""END TO END DISTANCE"""
plt.figure(2)
head = 1 # Length of the header section
filepath = 'C:/Users/nxb1090/Documents/Research/4Round/Full Chi/15-30-15/'

plt.title('N=60')
# Index = [0, len_mol - 1] #[start,end]
# graphData=['Entire Molecule', [2.5, 0.9], 'C0'] (data name, text location, color)
# name='Ree.txt'    
# Ree(head, Index, filename, graphData, len_mol, configInfo)

plt.title('Copolymer Blocks')
Index = [0, no_A - 1]
graphData = ['Type A', [2, 0.9], "Cyan"]
filename = filepath + 'Ree.txt'    
code.Ree(head, Index, filename, graphData, no_A, configInfo)

Index = [no_A, no_B - 1]
graphData = ['Type B', [2, 0.7], "Blue"]
filename = filepath + 'Ree.txt'    
code.Ree(head, Index, filename, graphData, no_B, configInfo)
# 
Index = [no_B, no_C - 1]
graphData = ["Type C", [2, 0.5], "Purple"]
filename = filepath + 'Ree.txt'    
code.Ree(head, Index, filename, graphData, no_C, configInfo)

"""Homopolymer Analysis"""
# plt.title('Homopolymers')
# filepath = 'C:/users/nxb1090/Documents/Research/Ree/15-30-15/Ree2/'
# len_mol = 60
# atoms = 46920
# no_mol = int(atoms / len_mol)
# bonds = int(no_mol * (len_mol - 1))
# # #Length of the header section
# head = 1  
# Index = [0, len_mol - 1]
# graphData = ["Homopolymer N=60", [2.5, 0.7], "C1"]
# name = filepath + 'ReeN.txt'    
# code.Ree(head, Index, filename, graphData, len_mol, configInfo)
# 

"""VOXEL AND VOLUME FRACTION"""
code.VoxelStructurewithVolFrac(1, 1, configInfo)

"""DENSITY FROM STRUCTURE FACTOR"""
"""NOTE: ONLY WORKS WITH NON-AVERAGED SQ DATA"""
stepsize = 1; start = 0
inputfiles = ['Eq1.txt', 'Eq1C.txt']
code.Part2StructureFactor(start, stepsize, inputfiles, configInfo)