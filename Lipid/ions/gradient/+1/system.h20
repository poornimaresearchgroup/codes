

# ----------------- Init Section -----------------

include "system.in.init"


# ----------------- Atom Definition Section -----------------

read_data "system.data"

# ----------------- Settings Section -----------------

include "system.in.settings"


# ----------------- Run Section -----------------

# The lines above define the system you want to simulate.
# What you do next is up to you.
# Typically a user would minimize and equilibrate
# the system using commands similar to the following:
#  ----   examples   ----
#
#  -- minimize --

thermo 100
timestep 1
fix   1 all nvt temp 300.0 300.0 100.0
run 0

variable target equal 0.05
variable ratio equal 0.33333

group water type 1
group lipids type 2 3 4 5
group tails type 5
group heads type 2
region inside block 0.0 150.0 0.0 150.0 -2.0 60.0
region outside block 0.0 150.0 0.0 150.0 60.0 122.0
group in region inside
group out region outside
group insidewater intersect water in
group outsidewater intersect water out
group outheads intersect heads out
group inheads intersect heads in

variable infrac equal "v_target*(1-v_ratio)"
variable outfrac equal "v_target*v_ratio"

set group insidewater type/fraction 6 ${infrac} 12345
set group outsidewater type/fraction 6 ${outfrac} 42
group in region inside
group out region outside
group na type 6
group inna subtract na out
group outna subtract na in
variable xin equal count(inna)
variable xout equal count(outna)
group insidewater subtract insidewater na
group outsidewater subtract outsidewater na
variable yin equal count(insidewater)
variable yout equal count(outsidewater)
variable infraction equal v_xin/v_yin
variable outfraction equal v_xout/v_yout
set group insidewater type/fraction 7 ${infraction} 234
set group outsidewater type/fraction 7 ${outfraction} 345
group cl type 7
set group na charge 1.0
set group cl charge -1.0
group water3 type 1

change_box all z final -2 122 boundary p p f 
region zhi plane 0 0 122 0 0 -1
region zlo plane 0 0 -2 0 0 1
fix wallhi all wall/region zhi lj126 1 2.3 2.58
fix walllo all wall/region zlo lj126 1 2.3 2.58
compute iwater insidewater stress/atom NULL
compute p1 all reduce sum c_iwater[1] c_iwater[2] c_iwater[3]
variable inpress equal -(c_p1[1]+c_p1[2]+c_p1[3])/3

compute owater outsidewater stress/atom NULL
compute p2 all reduce sum c_owater[1] c_owater[2] c_owater[3]
variable outpress equal -(c_p2[1]+c_p2[2]+c_p2[3])/3
thermo_style custom step temp epair emol etotal press pe v_inpress v_outpress
unfix 1

compute namsd na msd
compute clmsd cl msd
compute iheadmsd inheads msd
compute oheadmsd outheads msd

fix namsd na ave/time 1000000 1 1000000 c_namsd file namsd.txt mode vector
fix clmsd cl ave/time 1000000 1 1000000 c_clmsd file clmsd.txt mode vector
fix inheadsmsd inheads ave/time 1000000 1 1000000 c_iheadmsd file inheadsmsd.txt mode vector
fix oheadsmsd outheads ave/time 1000000 1 1000000 c_oheadmsd file outheadsmsd.txt mode vector

compute 4 na chunk/atom bin/1d z lower 2.0
compute 5 cl chunk/atom bin/1d z lower 2.0
compute 3 tails chunk/atom bin/1d z lower 2.0
compute 2 water3 chunk/atom bin/1d z lower 2.0
compute 1 heads chunk/atom bin/1d z lower 2.0

fix cl cl ave/chunk 1 10 1000000 5 density/number file chlorine.txt
fix na na ave/chunk 1 10 1000000 4 density/number file sodium.txt
fix 4 tails ave/chunk 1 10 1000000 3 density/number file tails.txt
fix 5 water3 ave/chunk 1 10 1000000 2 density/number file water.txt
fix 6 heads ave/chunk 1 10 1000000 1 density/number file heads.txt

fix   1 all npt temp 300.0 300.0 4000.0 x 1.0 1.0 40000 y 1.0 1.0 40000 couple xy
timestep 5
run 10000
timestep 40
restart 100000 bilayer.*.restart
dump 3 all custom 1000 salt.lammpstrj id mol type xu yu zu
run 1000000
