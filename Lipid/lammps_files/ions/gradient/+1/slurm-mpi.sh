#!/bin/bash -l
# Name of the job - You'll probably want to customize this.
#SBATCH -J 2-1

# Standard out and Standard Error output files
#SBATCH -o log.txt
#SBATCH -e errors.txt

#To send emails, set the adcdress below and remove one of the "#" signs.
##SBATCH --mail-user das9574@rit.edu

# notify on state change: BEGIN, END, FAIL or ALL
#SBATCH --mail-type=END

# Request 5 hours run time MAX, anything over will be KILLED
#SBATCH -t 20:00:00

# Put the job in the "debug" partition and request FOUR cores
# "work" is the default partition so it can be omitted without issue.
#SBATCH -p work -n 2

# Job memory requirements in MB
##SBATCH --mem=2048

# Explicitly state you are a free user
#SBATCH --qos=free

#
# Your job script goes below this line.  
module load openmpi-1.10-x86_64
module load lammps/20160928 
mpirun lmp_g++ < system.h20
