from toolbox import *
import matplotlib.pyplot as plt
import numpy as np
#file = "water.txt" # you will need to put the whole filepath if your file isn't in the same directory you run this
start_line = 5 #allows you to grab only data
end_line = 148
x_axis_index = 1 # when split up into a list, which index is your data at?
y_axis_index = 3
timesteps = 5

files = ["water.txt","heads.txt","tails.txt","nanoparticle.txt"]
for i in range(timesteps): #this whole assembly will change depending on what you want to plot, and how your data is divided. make sure you understand how pyplot works
    print(i)
    start_line = 5 + (144 * i)
    end_line = 147 + (144 * i)
    lists = [listmaker(start_line, files[0], end_line),listmaker(start_line, files[1], end_line),listmaker(start_line, files[2], end_line)]
    [[x1,y1],[x2,y2],[x3,y3]] = [xymake(lists[0], x_axis_index, y_axis_index), xymake(lists[1], x_axis_index, y_axis_index), xymake(lists[2], x_axis_index, y_axis_index)]

    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams.update({'font.size': 16})
    plt.figure(i + 1)
    plt.plot(x1, y1, linewidth=3.0, color="blue", label='water')
    plt.plot(x2, y2, linewidth=3.0, color="red", label='heads')
    plt.plot(x3, y3, linewidth=3.0, color="cyan", label='tails')
    #plt.plot(x4, y4, linewidth=3.0, color="green", label='nanoparticle')
#plt.plot(x, tails, linewidth=4.0, color="black", label='tails')
    plt.xlabel("position in box"); plt.ylabel("density");
    plt.title("density vs z")
    plt.legend(bbox_to_anchor=(1, .8), loc=1, borderaxespad=0., prop={'size': 16})
#plt.axis([175, 330, -100.0, 10.00])
plt.show()
