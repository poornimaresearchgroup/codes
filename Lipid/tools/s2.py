# This requires that the data file be ordered with all the lipid molecules written first
# the positions are which atom# in the file corresponds to the desired group
from toolbox import *
file_path = "10x10.data"
start_line = "34"
end_line = "10033"
num_lipids = 308
head_pos = 1
[tail_pos_1, tail_pos_2] = [8, 12]
[lx,ly,lz] = [100, 100, 120]
lipids_length = 12
data = listmaker(int(start_line), str(file_path), int(end_line))
data = vectorize(data, head_pos, tail_pos_1, tail_pos_2, num_lipids, lipids_length, x = 2, y = 3, z = 4, lx, ly, lz)
list = []
for el in data:
    list.append(s2(el, [0,0,1]))
print(avg(list))
