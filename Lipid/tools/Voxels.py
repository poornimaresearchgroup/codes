
import toolbox as tb
import math
from operator import itemgetter
def VoxelStructurewithVolFrac(Vox,start, infile):
    """Creates Document of # of atoms in Voxel.
    Using cutoff densities, each voxel can only be 1 type of atom
    Assumptions: Cubic Voxel
    Inputs: edge length of voxel, start line of Ree file"""

    mult=float(1/Vox)
    xlim = int(lx*mult)
    ylim = int(ly*mult)
    zlim = int(lz*mult)
    inputfilename='Ree.txt' #Ree.txt'
    inputfile=open(infile,'r')
    outputfile=open('voxels.txt','w+')
    outputfile2=open('volume.txt','w+')

    """SECTION 3: Text to List"""
    AtomList=[];
    AtomList = tb.listmaker_ez(inputfilename)

    """SECTION 4: Removes lines starting with text"""
    Output=[]; Ordered=[];
    for k in range (atoms):
        target = start + k
        temp=[float(x) for x in AtomList[target]]
        if tb.is_number(temp[0]): #if first character of temp a number added to List
            Output.append(temp)
    MasterList=[];
    for x in range(xlim):
        for y in range(ylim):
            for z in range(zlim):
                List=[x,y,z,0,0,0]
                MasterList.append(List)

    """SECTION 5: Atom -> Box"""
    counter=0
    for line in Output:
        #Scale atom coordinates to match Voxel coordinates
        AtomX,AtomY,AtomZ,Type=[int(math.floor(tb.wrap2(line[3], lx))*mult),int(math.floor(tb.wrap2(line[4], ly))*mult),int(math.floor(tb.wrap2(line[5], lz))*mult),line[2]]
        counter+=1
        for box in range(len(MasterList)): #Runs through list of Voxel,supplying coords
            CurrentVoxel=MasterList[box]
            CurrentX=CurrentVoxel[0]; CurrentY=CurrentVoxel[1]; CurrentZ=CurrentVoxel[2]
            ACount=CurrentVoxel[3]; BCount=CurrentVoxel[4]; CCount=CurrentVoxel[5]
            if CurrentX==AtomX and CurrentY==AtomY and CurrentZ==AtomZ: #If atom in Current Voxel
                if counter % 1000 ==0: #Prints atom counter for progress monitoring
                    print(counter)
                del(MasterList[box]) #Deletes current Voxel info
                if (Type==1 or Type==6 or Type==7) and AtomZ > (lz/2*mult):
                    ACount += 1
                elif (Type==1 or Type==6 or Type==7) and AtomZ < (lz/2*mult):
                    CCount += 1
                else:
                    BCount += 1
                List=[CurrentX,CurrentY,CurrentZ,ACount,BCount, CCount]
                MasterList.append(List) #Adds updated Voxel info


    """SECTION 6: Volume Fractions"""
    VolList = []; MasterVolList = [];
    VolA = 0; VolB = 0; VolC = 0; VolE = 0; V = Vox ** 3
    for entry in MasterList:
        #Info from current Voxel
        CurrentX = entry[0]; CurrentY = entry[1]; CurrentZ = entry[2]
        ACount = entry[3]; BCount = entry[4]; CCount = entry[5]
        fA = ACount; fB = BCount; fC = CCount
        if fA >= fB and fA > fC and fA != 0:
            List=[CurrentX, CurrentY, CurrentZ, fA, 0, 0]
            VolA+=1
        elif fC >= fB and fC > fA and fC != 0:
            List=[CurrentX, CurrentY, CurrentZ, 0, 0, fC]
            VolC+=1
        elif fB > fA:
            List=[CurrentX, CurrentY, CurrentZ, 0, fB, 0]
            VolB+=1
        else:
            List=[CurrentX, CurrentY, CurrentZ, 0, 0, 0]
            VolE+=1
        MasterVolList.append(List)

    MasterList=sorted(MasterList, key=itemgetter(0))
    MasterVolList=sorted(MasterVolList, key=itemgetter(0))

    Vbox=len(MasterVolList)

    print("""VolA: {}; Frac={}
VolB: {}; Frac={}
VolC: {}; Frac={}
Empty Volume: {}; Frac={}""".format(VolA * V,VolA/Vbox,VolB * V,VolB/Vbox,VolC * V,VolC/Vbox,VolE * V,VolE/Vbox))
    return [VolA * V, VolC * V]
    """SECTION 7: Write Results to OutoutFile for MatLab Analysis"""
    for data in MasterList:
        outputfile.write(str(data)+'\n')
    for Voldata in MasterVolList:
        outputfile2.write(str(Voldata)+'\n')

    """SECTION 8: Close Files"""
    outputfile.close()
    outputfile2.close()
    inputfile.close()
atoms = 22499
len_mol=12
lx = 150
ly = 150
lz = 120


no_mol=int(atoms/len_mol)
bonds=int(no_mol*(len_mol-1))
name='6-24-30'
#VoxelStructurewithVolFrac(8,1,"Ree.txt")
