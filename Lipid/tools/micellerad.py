from toolbox import *
# RUN SECTION -----------------------------------------------------------------------------------
file_path = input("What is the filepath? -->")
start_line = input("Which line should I start at? -->")
end_line = input("Which line should i end at? -->")
data = listprune(listmaker(int(start_line), str(file_path), int(end_line)), "2", 1) # gets a list of all my headgroup atoms from the data file
dists = []
for i in range(len(data)):
    dists.append(radiusfind(data, i)) # make a list of the max distance between atoms
print(dists)
avgrad = avg(dists) / (2 * 10)
print(avgrad)
