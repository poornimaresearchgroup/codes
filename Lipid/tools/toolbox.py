import math
import random
def stdev(data):
    """ takes a list of numbers, returns the standard deviation for that list"""
    av = avg(data)
    c = 0
    for number in data:
        c += (float(number)-av) ** 2
    return (c / (len(data) - 1)) ** .5
def is_number(s):
    """If input can be made into a float, returns true. Else returns false"""
    try:
        float(s)
        return True
    except ValueError:
        return False

def unitvector(magnitude, x, y, z):
    """generates a vector of magnitude = bondlength in a random direction from a point
     returns the coordinates of the new point
     useful for generating xyz files for a molecule so that you can use packmol"""
    thet = random.random() * 2 * math.pi
    phi = random.random() * 2 * math.pi
    x += magnitude * math.sin(thet) * math.cos(phi)
    y += magnitude * math.sin(thet) * math.sin(phi)
    z += magnitude * math.cos(thet)
    return[x,y,z]
def distcheck(master, coords, tolerance):
    """ checks the distance between two points
    returns True if the distance is smaller than your tolerance"""
    [x1, y1, z1] = master
    [x, y, z] = coords
    return (((x - x1) ** 2) + ((y - y1) ** 2) + ((z - z1) ** 2)) ** .5 < tolerance
def avg(data):
    """
    Takes a list of data
    note that this can handle None type.
    Returns the average value of that data
    """
    t = 0
    count = 0
    for number in data:
        if number:
            t += float(number)
            count += 1
    if count != 0:
        return t / count

def getcoords(atom, master, indices, box):
    """
    Takes two lists, which can be of any form so long as they are at least length 3 and are of the same form
    indices must be of the form [index of x coordinate, index of y coordinate, index of z coordinate]
    box is of the form [x dimension, y dimension, z dimension]

    Returns the coordinates of the first atom, wrapped to be consistent with the second atom in the form [x, y, z]
    """
    [lx, ly, lz] = box
    [x,y,z] = indices
    hx = float(master[x])
    hy = float(master[y])
    hz = float(master[z])
    x1 = wrap(hx, float(atom[x]), lx)
    y1 = wrap(hy, float(atom[y]), ly)
    z1 = wrap(hz, float(atom[z]), lz)
    return [x1,y1,z1]

def listmaker(target, file, end):
    """
    takes a file, a start line, and and end line,
    returns all data between the start and end as a list of lists,
    note: this splits by whitespace
    """
    data = open(str(file)).readlines()
    ans = []
    for a in range(len(data)):
        if a >= target-1 and a < end: # checks that the line is in the target area
            line = data[a]
            list = line.split()
            ans.append(list) #builds a list of all the lines in the target area
    return ans
def listmaker_ez(file):
    """
    takes a filename
    returns all data between the start and end as a list of lists,
    note: this splits by whitespace
    """
    data = open(str(file)).readlines()
    ans = []
    for a in range(len(data)):
        line = data[a]
        list = line.strip()
        list = list.split()
        ans.append(list) #builds a list of all the lines in the target area
    return ans
def listcut(data, index):
    """
    takes a list of lists, a condition, and an index.
    returns a list of only the lists that have the specified value at the specified index
    """
    ans = []
    for element in data:
        ans.append(element[index])
    return ans
def listprune(data, type, index):
    """
    takes a list of lists, a condition, and an index.
    returns a list of only the lists that have the specified value at the specified index
    """
    ans = []
    for element in data:
        if element[index] == type:
            ans.append(element)
    return ans

def radiusfind(data, target, lbox = 80, x = 2, y = 3, z = 4):
    """
    Takes a list of atoms with optional xyz indices, optional box length and a target atom
    note: this assumes a cubic box
    returns the greatest distance found between the target atom and any other atom
    """
    master = data[target]
    xt = float(master[x])
    yt = float(master[y])
    zt = float(master[z])#grab your main atom coordinates
    dist = 0
    for el in data:
        xb = wrap(xt, float(el[x]), lbox) # grab the coordinates for each atom to compare it to
        yb = wrap(yt, float(el[y]), lbox)
        zb = wrap(zt, float(el[z]), lbox)
        temp = (((xt - xb)**2)+((yt - yb)**2)+((zt - zb)**2)) ** (.5)
        if temp > dist:
            dist = temp # keeps track of the largest distance found
    return dist # works because the longest distance within a sphere is the diameter

def s2(vector, mastervector):
    """
    Takes a vector and a direction to compare it to.
    Returns the S2 value for that vector pair.
    """
    return (3 * (vector_cos(vector, mastervector) ** 2) - 1) / 2
def bilayer(list1, list2, lx, ly, lz, x = 2, y = 3, z = 4):
    """ This takes 2 lists of headgroups on opposite sides of your lipid bilayer
    needs box dimensions so that coordinates are wrapped properly
    x y and z are the inices that data is located
    returns the average distance between nearest-neighbor headgroups on opposite sides of the bilayer
    note: assumes units in are angstroms and desired units are nm"""
    ans = []
    for element in list1:
        temp = 500
        xt = float(element[x])
        yt = float(element[y])
        zt = float(element[z])
        for atom in list2:
            xb = wrap(xt, float(atom[x]), lx)
            yb = wrap(yt, float(atom[y]), ly)
            zb = wrap(zt, float(atom[z]), lz)
            if (((xt - xb)**2)+((yt - yb)**2)+((zt - zb)**2)) ** (.5) < temp:
                temp = (((xt - xb)**2)+((yt - yb)**2)+((zt - zb)**2)) ** (.5)
        ans.append(temp / 10)
    return ans
def unit_vector(point1, point2):
    """
    Takes two points. as lists of the form [x,y,z]
    returns the unit vector representing the direction from point1 to point 2
    """
    [x1,y1,z1] = point1
    [x2,y2,z2] = point2
    dir = [float(x1) - float(x2), float(y1) - float(y2), float(z1) - float(z2)]
    length = (((float(x1) - float(x2)) ** 2) + ((float(y1) - float(y2)) ** 2) + ((float(z1) - float(z2)) ** 2)) ** 0.5
    vector = [x / length for x in dir]
    return vector
def vectorize(data, head, end1, end2, num, length, x = 2, y = 3, z = 4, lx = 100, ly = 100, lz = 200):
    """
    Takes an ordered list of atoms,
    the location of the first headgroup and the fist two tail ends,
    the number of identical lipids,
    and the length of each of those lipids (total atoms)
    optionally you can specify:
    the indices for your xyz coordinates in your input list,
    x y z box dimensions (used for wrapping)

    Returns a list of unit vectors between a headgroup and each of its tail ends,
    the list of vectors will be exactly 2* the number of lipids.
    """

    vectorlist = []
    for a in range(num):
        h_group = data[head + (a * length)]
        tail_1 = data[end1 + (a * length)]
        tail_2 = data[end2 + (a * length)]
        [t_1x, t_1y, t_1z] = getcoords(tail_1, h_group, [x,y,z], [lx,ly,lz])
        [t_2x, t_2y, t_2z] = getcoords(tail_2, h_group, [x,y,z], [lx,ly,lz])
        vectorlist.append(unit_vector([h_group[x],h_group[y],h_group[z]], [t_1x,t_1y,t_1z]))
        vectorlist.append(unit_vector([h_group[x],h_group[y],h_group[z]], [t_2x,t_2y,t_2z]))
    return vectorlist

def vector_cos(v1, v2, m1 = 1, m2 = 1):
    """
    Takes a vector pair and their magnitudes.
    Returns the cosine of the angle between those vectors.
    """
    [x1, y1, z1] = v1
    [x2, y2, z2] = v2
    return ((x1*x2)+(y1*y2)+(z1*z2)) / (m1 * m2)
def wrap2(v, Lbox):
    """this will wrap things well, but note that this will break if your box doesn't start at 0,0,0"""
    return v-Lbox*math.floor(v/Lbox)
def wrap(c1, c2, lbox):
    """
    Takes a 1D coordinate pair, and periodic box length in that dimension.
    returns the second coordinate corrected for periodic boundary conditions
    """
    if c1 - c2 > lbox / 2: # correct the coordinates if there are boundary effects
        return c2 + lbox
    elif c1 - c2 < -1 * lbox / 2: # correct the coordinates if there are boundary effects
        return c2 - lbox
    else:
        return c2
def xymake(list, xi, yi):
    """makes a list of x and a list of y
    used for plotting"""
    x = []
    y = []
    for element in list:
        x.append(element[xi])
        y.append(element[yi])
    return [x,y]
