import toolbox as tool
file_path = "chain.xyz"
file = open(file_path, "w+")
x = 0
y = 0
z = 0
PEG = 6
PMMA = 12
chainLength = 1 + PEG + PMMA
bondlength = 3
molList = []
for block in range(3):
    if block == 0:
        file.write("{}\n \nN    {}  {}  {}\n".format(chainLength, x, y, z))
        molList.append([x,y,z])
    elif block == 1:
        for j in range(PEG):
            while True:
                flag = True
                [x,y,z] = tool.unitvector(bondlength, x, y, z)
                counter = 0
                for element in molList:
                    if tool.distcheck(element, [x,y,z], 2.0):
                        flag = False
                if flag:
                    print("breaking")
                    break
            file.write("P     {}   {} {}\n".format(x, y, z))
            molList.append([x,y,z])
    else:
        for k in range(PMMA):
            if k% 2 == 0:
                while True:
                    flag = True
                    [x,y,z] = tool.unitvector(bondlength, x, y, z)
                    counter = 0
                    for element in molList:
                        if tool.distcheck(element, [x,y,z], 2.0):
                            flag = False
                    if flag:
                        break
                file.write("1   {}  {}  {}\n".format(x, y, z))
                molList.append([x,y,z])
            else:
                while True:
                    flag = True
                    [xtemp,ytemp,ztemp] = tool.unitvector(bondlength, x, y, z)
                    counter = 0
                    for element in molList:
                        if tool.distcheck(element, [xtemp,ytemp,ztemp], 2.0):
                            flag = False
                    if flag:
                        break
                file.write("2   {}  {}  {}\n".format(xtemp, ytemp, ztemp))
                molList.append([x,y,z])
file.close()
