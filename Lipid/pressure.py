from Voxels import VoxelStructurewithVolFrac
from toolbox import *
import math
# RUN SECTION ------------------------------------------------------
file_path = "log.txt" #whatever your lammps log file is
start_line = "1000"
end_line = "10000" #pick these to be somewhere in the middle of your data
insidepress = listcut(listmaker(int(start_line), str(file_path), int(end_line)), 7)
outsidepress = listcut(listmaker(int(start_line), str(file_path), int(end_line)), 8)
press = listcut(listmaker(int(start_line), str(file_path), int(end_line)), 5) #this just gets the pressures
[outside,inside] = VoxelStructurewithVolFrac(8,1,"Ree.txt") #use the voxel code to find the volume of the inside and the outside (so that we get pressure properly)
print(avg(insidepress)/inside)
print(avg(outsidepress)/outside) #print all of the average values could write these to a file instead
print(avg(press))
