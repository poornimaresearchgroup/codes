# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 14:35:57 2023

@author: nxb1090
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import ToolBox as tb

# Input FILEPATH and trj file to be examined

SIMULATION = "K2"
FILEPATH = "C:/Users/nxb1090/Desktop/GradResearch/Buchanan/Projects/" + \
           "HelixMelt/Data/LowerPressure/T120P210/1Try/" + SIMULATION

AProfile = pd.read_csv(FILEPATH + "/PostProcessing/chunkA.txt")
BProfile = pd.read_csv(FILEPATH + "/PostProcessing/chunkB.txt")
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "ReezVector.txt"

# %% Time and Box Info

ReeList = []

dir_list = os.listdir(FILEPATH + '/LAMMPS/')
i = 0
for root, dirs, files in os.walk(FILEPATH + '/LAMMPS/'):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not "Old" in d]
    i = i + 1
    for file in files:
        if file == "lam.lammpstrj":
            filepath = root + '/' + file
            ReeList.append(filepath)
for runcounter, FILENAME in enumerate(ReeList):
    print('Run Counter: ' + str(runcounter))

    # Number of atoms from first timestep of trj file
    headerinfo = pd.read_csv(FILENAME, nrows=7)
    atoms = int(headerinfo.iloc[2, 0])

    # TimeReeData stores mean-squared end-to-end distance for each time step
    TimeBoxData = pd.DataFrame(columns=["Time", "Lx", "Ly", "Lz"])
    TimeReeData = pd.DataFrame(columns=["Time", "Ree^2", "A_Ree", "B_Ree"])
    TIMECOUNTER = 0

    # Takes each time step of a chunksize
    with pd.read_csv(FILENAME, chunksize=(9), skiprows=(lambda x: x % (atoms + 9)
                                                                  >= 9)) as reader:
        for chunk in reader:
            time = int(chunk.iloc[0, 0])  # time associated with chunk
            # Get box dimensions at current timestep
            [xlo, xhi, ylo, yhi, zlo, zhi] = [float(chunk.iloc[4, 0].split()[0]),
                                              float(
                                                  chunk.iloc[4, 0].split()[1]),
                                              float(
                                                  chunk.iloc[5, 0].split()[0]),
                                              float(
                                                  chunk.iloc[5, 0].split()[1]),
                                              float(
                                                  chunk.iloc[6, 0].split()[0]),
                                              float(chunk.iloc[6, 0].split()[1])]
            TimeBoxData.loc[TIMECOUNTER] = [
                time, xhi - xlo, yhi - ylo, zhi - zlo]
            TIMECOUNTER += 1
    print("Done with Box Info")
    # %% Chunker
    TIMECOUNTER = 0
    with pd.read_csv(FILENAME, chunksize=(atoms), sep=" ",
                     skiprows=(lambda x: x % (atoms + 9) < 9),
                     skipinitialspace=(True),
                     names=["id", 'mol', 'type', 'x', 'y', 'z',
                            'ix', 'iy', 'iz', "test"]) as reader:
        for chunk in reader:
            if TIMECOUNTER % 5 == 0:
                print(TIMECOUNTER)
            chunk.drop("test", axis=1, inplace=True)
            time = TimeBoxData.loc[TIMECOUNTER, "Time"]

            if time != TimeBoxData.loc[TIMECOUNTER, "Time"]:
                raise ValueError("Times Don't Match \n" + str(time) + "    " +
                                 str(TimeBoxData.loc[TIMECOUNTER, "Time"]))

            Lx = TimeBoxData.loc[TIMECOUNTER, "Lx"]
            Ly = TimeBoxData.loc[TIMECOUNTER, "Ly"]
            Lz = TimeBoxData.loc[TIMECOUNTER, "Lz"]

            chunk["x"] = (chunk["x"] + chunk["ix"]) * Lx
            chunk["ix"] = 0
            chunk["y"] = (chunk["y"] + chunk["iy"]) * Ly
            chunk["iy"] = 0
            chunk["z"] = (chunk["z"] + chunk["iz"]) * Lz
            chunk["iz"] = 0

            group_object = chunk.groupby(["mol", "type"])
            # first bead of every block
            first = chunk.loc[group_object.id.idxmin()]
            first.reset_index(inplace=True, drop=True)
            # last bead of every block
            last = chunk.loc[group_object.id.idxmax()]
            last.reset_index(inplace=True, drop=True)

            tempData = pd.DataFrame(columns=["mol", "type", "z", "Lz"])
            tempData.mol = first.mol
            if not last.mol.equals(tempData.mol):
                raise ValueError("Molecule IDs don't match")
            tempData.type = first.type
            if not last.type.equals(tempData.type):
                raise ValueError("Type IDs don't match")
            tempData.Lz = Lz

            # Distances for each block

            for pair in range(len(first)):
                v1 = first.z.iloc[pair]
                v2 = last.z.iloc[pair]
                v2 = tb.adjust(v1, v2, Lz)
                tempData.at[pair, "z"] = v2 - v1
            tempData["time"] = time

            try:
                histdata
            except NameError:
                histdata = tempData.copy()

            histdata = pd.concat([histdata, tempData])
            TIMECOUNTER += 1
# %% Figures

binlist = list(range(-15, 16, 1))
plt.figure()
n, bins, patch = plt.hist(
    tempData[tempData["type"] == 1].z, bins=binlist, color="r", alpha=0.3)
m, bins, patch = plt.hist(
    tempData[tempData["type"] == 2].z, bins=binlist, color="b", alpha=0.3)
plt.xlabel("End-to-End Z Distance")
plt.ylabel("Frequency")
plt.title(SIMULATION)

span = histdata[histdata.time > max(histdata["time"]) * 0.9]
plt.figure()
n, bins, patch = plt.hist(
    span[span["type"] == 1].z, bins=binlist, color="r", alpha=0.3, density=True)
m, bins, patch = plt.hist(
    span[span["type"] == 2].z, bins=binlist, color="b", alpha=0.3, density=True)
plt.xlabel("End-to-End Z Distance")
plt.ylabel("Frequency")
plt.title(SIMULATION)

binlist = np.linspace(-1, 1, 10)
test = span.copy()
test["z"] = test.z.astype("float64")
test["Domain"] = test.Lz / 4
test["Domain"] = test.Domain.div(2)
test["Norm"] = test.z.div(test.Domain)
plt.figure()
n, bins, patch = plt.hist(
    test[test["type"] == 1].Norm, color="r", alpha=0.3, density=True)
m, bins, patch = plt.hist(
    test[test["type"] == 2].Norm, color="b", alpha=0.3, density=True)
plt.xlabel("End-to-End Z Distance")
plt.ylabel("Frequency")
plt.title(SIMULATION)

test.to_csv(OUTPUTFILE)
