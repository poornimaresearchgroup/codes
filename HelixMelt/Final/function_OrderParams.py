# -*- coding: utf-8 -*-
"""
Created on Fri Jan  5 15:16:42 2024

@author: nxb1090
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.signal as sc

import ToolBox as tb

# Input FILEPATH and trj file to be examined

SIMULATION = "K1"
FILEPATH = f"C:/Users/nxb1090/Desktop/GradResearch/Buchanan/Projects/HelixMelt/Data/LowerPressure/T80P240/{SIMULATION}"

AProfile = pd.read_csv(f"{FILEPATH}/PostProcessing/chunkA.txt")
BProfile = pd.read_csv(f"{FILEPATH}/PostProcessing/chunkB.txt")

OUTPUTFILE_FULL = f"{FILEPATH}/PostProcessing/fullOrderData.txt"
OUTPUTFILE_SPAN = f"{FILEPATH}/PostProcessing/spanOrderData.txt"

# %% Bin List
APeak = sc.argrelextrema(AProfile.vol_frac.values, comparator=np.greater,
                         mode='wrap')[0]
BPeak = sc.argrelextrema(BProfile.vol_frac.values, comparator=np.greater,
                         mode='wrap')[0]
binStep = AProfile.Coord.values[0]
binList = AProfile.Coord.values
binList.sort()

# %% Time and Box Info

ReeList = []
TimeList = []
dir_list = os.listdir(FILEPATH + '/LAMMPS/')

i = 0
for root, dirs, files in os.walk(FILEPATH + '/LAMMPS/'):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not "Old" in d]
    i = i + 1
    for file in files:
        if file == "lam.lammpstrj":
            filepath = root + '/' + file
            ReeList.append(filepath)
for runcounter, FILENAME in enumerate(ReeList):
    print('Run Counter: ' + str(runcounter))

    # Number of atoms from first timestep of trj file
    headerinfo = pd.read_csv(FILENAME, nrows=7)
    atoms = int(headerinfo.iloc[2, 0])

    # TimeReeData stores mean-squared end-to-end distance for each time step
    TimeBoxData = pd.DataFrame(columns=["Time", "Lx", "Ly", "Lz"])
    TIMECOUNTER = 0

    # Takes each time step of a chunksize
    with pd.read_csv(FILENAME, chunksize=(9), skiprows=(lambda x: x % (atoms + 9)
                                                                  >= 9)) as reader:
        for chunk in reader:
            time = int(chunk.iloc[0, 0])  # time associated with chunk
            # Get box dimensions at current timestep
            [xlo, xhi, ylo, yhi, zlo, zhi] = [float(chunk.iloc[4, 0].split()[0]),
                                              float(
                                                  chunk.iloc[4, 0].split()[1]),
                                              float(
                                                  chunk.iloc[5, 0].split()[0]),
                                              float(
                                                  chunk.iloc[5, 0].split()[1]),
                                              float(
                                                  chunk.iloc[6, 0].split()[0]),
                                              float(chunk.iloc[6, 0].split()[1])]
            TimeBoxData.loc[TIMECOUNTER] = [
                time, xhi - xlo, yhi - ylo, zhi - zlo]
            TIMECOUNTER += 1

    print("Done with Box Info")
    TimeList.extend(TimeBoxData.Time)
    try:
        FullBoxData
        FullBoxData = pd.concat([FullBoxData, TimeBoxData])
    except NameError:
        FullBoxData = TimeBoxData.copy()

    # %% Chunker
    TIMECOUNTER = 0

    with pd.read_csv(FILENAME, chunksize=(atoms), sep=" ",
                     skiprows=(lambda x: x % (atoms + 9) < 9),
                     skipinitialspace=(True),
                     names=["id", 'mol', 'type', 'x', 'y', 'z',
                            'ix', 'iy', 'iz']) as reader:
        # For each timestep
        for chunk in reader:
            if TIMECOUNTER % 5 == 0:
                print(TIMECOUNTER)
            time = TimeBoxData.loc[TIMECOUNTER, "Time"]

            if time != TimeBoxData.loc[TIMECOUNTER, "Time"]:
                raise ValueError("Times Don't Match \n" + str(time) + "    " +
                                 str(TimeBoxData.loc[TIMECOUNTER, "Time"]))

            # Get box dimensions
            Lx = TimeBoxData.loc[TIMECOUNTER, "Lx"]
            Ly = TimeBoxData.loc[TIMECOUNTER, "Ly"]
            Lz = TimeBoxData.loc[TIMECOUNTER, "Lz"]

            binData = pd.DataFrame(columns=["time", "Coord",
                                            "Total", "A", "B",
                                            "phiA", "phiB",
                                            "polarA", "polarB",
                                            "polarAperp", "polarBperp",
                                            "QperpA", "QperpB",
                                            "QparA", "QparB"], dtype='float64')
            binData.Coord = AProfile.Coord
            binData.time = time

            # unwrap atom coordinates and set wrap flag to 0
            chunk["x"] = (chunk["x"] + chunk["ix"]) * Lx
            chunk["ix"] = 0
            chunk["y"] = (chunk["y"] + chunk["iy"]) * Ly
            chunk["iy"] = 0
            chunk["z"] = (chunk["z"] + chunk["iz"]) * Lz
            chunk["iz"] = 0

            chunk.insert(9, 'midpoint', 0)
            chunk.insert(9, 'binIndex', 0)
            chunk.insert(9, 'Length', 0)
            chunk.insert(9, 'DX', 0)
            chunk.insert(9, 'DY', 0)
            chunk.insert(9, 'DZ', 0)
            chunk.insert(9, 'Qx', 0)
            chunk.insert(9, 'Qy', 0)
            chunk.insert(9, 'Qz', 0)
            chunk = chunk.sort_values("id")
            chunk.index = chunk.id
            # group by block -> molecule ID then block type
            group_object = chunk.groupby(["mol", "type"])

            # For each atom - 1
            for row in group_object:
                curr_atom = row[1]
                curr_atom.index = curr_atom["id"]
                m, n = curr_atom.shape
                for k in range(0, m - 1):
                    v1 = tb.wrap(curr_atom.z.iloc[k], Lz)
                    v2 = tb.wrap(curr_atom.z.iloc[k + 1], Lz)
                    v2 = tb.adjust(v1, v2, Lz)  # wrap z coordinates to find diff
                    DZ = v2 - v1
                    midpoint = v1 + (DZ / 2)
                    chunk.at[(curr_atom.index[k]), 'midpoint'] = midpoint
                    # assigns bin based on which bin centerpoint is closest to the z midpoint of the bond
                    binID = min(AProfile.Coord, key=lambda x: abs(x - midpoint))
                    binID = AProfile.Chunk[AProfile.Coord == binID].iloc[0]
                    if binID == 0:
                        raise ValueError("BinID found to be 0")
                    chunk.at[(curr_atom.index[k]), 'binIndex'] = binID
                    chunk.at[(curr_atom.index[k]), 'DZ'] = DZ  # z vector length

                    v1 = tb.wrap(curr_atom.x.iloc[k], Lx)
                    v2 = tb.wrap(curr_atom.x.iloc[k + 1], Lx)
                    v2 = tb.adjust(v1, v2, Lx)
                    DX = v2 - v1
                    chunk.at[(curr_atom.index[k]), 'DX'] = DX  # x vector length

                    v1 = tb.wrap(curr_atom.y.iloc[k], Ly)
                    v2 = tb.wrap(curr_atom.y.iloc[k + 1], Ly)
                    v2 = tb.adjust(v1, v2, Ly)
                    DY = v2 - v1
                    chunk.at[(curr_atom.index[k]), 'DY'] = DY  # y vector length

                    chunk.at[(curr_atom.index[k]), 'Length'] = \
                        np.sqrt((DX ** 2) + (DY ** 2) + (DZ ** 2))  # bond length

                    chunk.at[(curr_atom.index[k]), 'Qx'] = \
                        DX * DX - 1 / 3
                    chunk.at[(curr_atom.index[k]), 'Qy'] = \
                        DY * DY - 1 / 3
                    chunk.at[(curr_atom.index[k]), 'Qz'] = \
                        DZ * DZ - 1 / 3
            # drop the last atom in every block since it does not start a bond
            chunk = chunk.loc[chunk.midpoint != 0]
            group_bin = chunk.groupby(["binIndex", "type"])
            binData.A = 0
            binData.B = 0

            for name, group in group_bin:
                binIndex = name[0]
                molType = name[1]

                try:
                    total = group_bin.get_group((binIndex, 1)).count()[0]
                except KeyError:
                    total = 0

                try:
                    total = total + \
                            group_bin.get_group((binIndex, 2)).count()[0]
                except KeyError:
                    total = total

                if group['id'].count() > 0:
                    if molType == 1:
                        binData.loc[binIndex - 1, "A"] = group['id'].count()
                        binData.loc[binIndex - 1, "polarA"] = group['Length'].sum()
                        binData.loc[binIndex - 1, "polarAperp"] = group['DZ'].sum()
                        QX = group['Qx'].sum() / (Lx * Ly * binStep * total)
                        QY = group['Qy'].sum() / (Lx * Ly * binStep * total)
                        QZ = group['Qz'].sum() / (Lx * Ly * binStep * total)
                        binData.loc[binIndex - 1, "QperpA"] = QZ
                        binData.loc[binIndex - 1, "QparA"] = (QX + QY) / 2
                    elif molType == 2:
                        binData.loc[binIndex - 1, "B"] = group['id'].count()
                        binData.loc[binIndex - 1, "polarB"] = group['Length'].sum()
                        binData.loc[binIndex - 1, "polarBperp"] = group['DZ'].sum()
                        QX = group['Qx'].sum() / (Lx * Ly * binStep * total)
                        QY = group['Qy'].sum() / (Lx * Ly * binStep * total)
                        QZ = group['Qz'].sum() / (Lx * Ly * binStep * total)
                        binData.loc[binIndex - 1, "QperpB"] = QZ
                        binData.loc[binIndex - 1, "QparB"] = (QX + QY) / 2
                    else:
                        raise ValueError("Unexpected Atom Type")
            binData.Total = binData.A + binData.B
            binData = binData[binData.Total != 0]
            binData.phiA = binData.A / binData.Total
            binData.phiB = binData.B / binData.Total

            binData = binData.assign(polarA=lambda x:
            x.polarA / (Lx * Ly * binStep * x.Total))
            binData = binData.assign(polarB=lambda x:
            x.polarB / (Lx * Ly * binStep * x.Total))
            binData = binData.assign(polarAperp=lambda x:
            x.polarAperp / (Lx * Ly * binStep * x.Total))
            binData = binData.assign(polarBperp=lambda x:
            x.polarBperp / (Lx * Ly * binStep * x.Total))

            binData = binData.infer_objects()

            try:
                histdata
                histdata = pd.concat([histdata, binData])
            except NameError:
                histdata = binData.copy()

            TIMECOUNTER += 1


# %%

def adjustCoord(row, peaks, Lz):
    binID = min(peaks, key=lambda x: abs(tb.adjust(row, x, Lz) - row))
    row = round(tb.adjust(row, binID, Lz) - row, 2)
    return row


def MasterAdjust(row, boxdata, peaks):
    time = row.time
    boxLength = boxdata.at[time, 'Lz']
    peaks = peaks[peaks.time == time]
    wrap = adjustCoord(row.Coord, peaks.Coord.values, boxLength)
    row.wrap = wrap
    return wrap


TimeBoxData.index = TimeBoxData.Time
FullBoxData.index = FullBoxData.Time
FullBoxData.drop_duplicates("Time", inplace=True)
span = histdata[histdata.time >= max(histdata['time']) * 0.90]
# span.insert(len(span.columns), 'wrapA',0)
# span.insert(len(span.columns), 'wrapB',0)
plotdata = pd.DataFrame(columns=span.columns)
PeakDataA = pd.DataFrame(columns=span.columns, dtype='float64')
PeakDataB = pd.DataFrame(columns=span.columns, dtype='float64')
timeGroup = span.groupby('time')

for name, group in timeGroup:
    binArray = group.phiA.to_numpy()
    binArray = np.concatenate((binArray, group.phiA.to_numpy()))
    APeaks = sc.find_peaks(binArray, prominence=0.5)
    APeaks = APeaks[0][0:4]
    wrapped = (APeaks > np.max(group.index.values)).astype(int)
    APeaks = APeaks - (wrapped * (np.max(group.index.values) + 1))
    APeaks = group.iloc[APeaks]
    PeakDataA = pd.concat([PeakDataA, APeaks])

    binArray = group.phiB.to_numpy()
    binArray = np.concatenate((binArray, group.phiB.to_numpy()))
    BPeaks = sc.find_peaks(binArray, prominence=0.5)
    BPeaks = BPeaks[0][0:4]
    wrapped = (BPeaks > np.max(group.index.values)).astype(int)
    BPeaks = BPeaks - (wrapped * (np.max(group.index.values) + 1))
    BPeaks = group.iloc[BPeaks]
    PeakDataB = pd.concat([PeakDataB, BPeaks])

# span.loc[:,'wrapA'] = 0
# span['wrapB'] = 0

test = span.apply(lambda x: [MasterAdjust(x, FullBoxData, PeakDataA)], axis=1, result_type='expand')
span.insert(len(span.columns), 'wrapA', test)
test = span.apply(lambda x: [MasterAdjust(x, FullBoxData, PeakDataB)], axis=1, result_type='expand')
span.insert(len(span.columns), 'wrapB', test)
DistanceGroupA = span.groupby('wrapA').mean()
DistanceGroupB = span.groupby('wrapB').mean()

# %% Plots
plt.figure()
plt.plot(DistanceGroupA.index.values, DistanceGroupA.phiA, 'r', label=r'$\phi_A$')
plt.plot(DistanceGroupB.index.values, DistanceGroupB.phiB, 'b', label=r'$\phi_B$')
plt.title(r'$\phi$')
plt.legend()

fig, ax1 = plt.subplots()
ax1.plot(DistanceGroupA.index.values, DistanceGroupA.polarAperp * np.sqrt(atoms), 'r',
         label=r'$pA\perp$')
ax1.set_xlabel('z')
ax1.set_ylabel(r'$N^{1/2}p^{A}_{\perp}$', color='r')
ax1.tick_params(axis='y', labelcolor='r')
plt.ylim([-0.5, 0.5])
ax2 = ax1.twinx()
ax2.set_ylabel(r'$\phi$', color='k')
ax2.plot(DistanceGroupA.index.values, DistanceGroupA.phiA, color="k",
         label=r'$\phi_A$')
ax2.tick_params(axis='y', labelcolor='k')
plt.ylim([0, 1])
fig.tight_layout()
plt.title(SIMULATION + ' A')
fig.legend(bbox_to_anchor=(1.1, 0.5))

fig, ax1 = plt.subplots()
ax1.plot(DistanceGroupB.index.values, DistanceGroupB.polarBperp * np.sqrt(atoms), 'b',
         label=r'$pB\perp$')
ax1.set_xlabel('z')
ax1.set_ylabel(r'$N^{1/2}p^{B}_{\perp}$', color='b')
ax1.tick_params(axis='y', labelcolor='b')
plt.ylim([-0.5, 0.5])
ax2 = ax1.twinx()
ax2.set_ylabel(r'$\phi$', color='k')
ax2.plot(DistanceGroupB.index.values, DistanceGroupB.phiB, color="k",
         label=r'$\phi_B$')
ax2.tick_params(axis='y', labelcolor='k')
plt.ylim([0, 1])
fig.tight_layout()
plt.title(SIMULATION + ' B')
fig.legend(bbox_to_anchor=(1.1, 0.5))

fig, ax1 = plt.subplots()
ax1.plot(DistanceGroupA.index.values, DistanceGroupA.QperpA * atoms, 'r',
         label=r'$Q^{A}_{\perp}$')
ax1.plot(DistanceGroupA.index.values, DistanceGroupA.QparA * atoms, 'r:',
         label=r'$Q^{A}_{||}$')
ax1.set_xlabel('z')
ax1.set_ylabel(r'$NQ_{A}$', color='r')
ax1.tick_params(axis='y', labelcolor='r')
# plt.ylim([-0.5, 0.5])
ax2 = ax1.twinx()
ax2.set_ylabel(r'$\phi$', color='k')
ax2.plot(DistanceGroupA.index.values, DistanceGroupA.phiA, color="k",
         label=r'$\phi_A$')
ax2.tick_params(axis='y', labelcolor='k')
plt.ylim([0, 1])
fig.tight_layout()
plt.title(SIMULATION + ' A')
fig.legend(bbox_to_anchor=(1.1, 0.5))

fig, ax1 = plt.subplots()
ax1.plot(DistanceGroupB.index.values, DistanceGroupB.QperpB * atoms, 'b',
         label=r'$Q^{B}_{\perp}$')
ax1.plot(DistanceGroupB.index.values, DistanceGroupB.QparB * atoms, 'b:',
         label=r'$Q^{B}_{||}$')
ax1.set_xlabel('z')
ax1.set_ylabel(r'$NQ_{B}$', color='b')
ax1.tick_params(axis='y', labelcolor='b')
# plt.ylim([-0.5, 0.5])
ax2 = ax1.twinx()
ax2.set_ylabel(r'$\phi$', color='k')
ax2.plot(DistanceGroupB.index.values, DistanceGroupB.phiB, color="k",
         label=r'$\phi_B$')
ax2.tick_params(axis='y', labelcolor='k')
plt.ylim([0, 1])
fig.tight_layout()
plt.title(SIMULATION + ' B')
fig.legend(bbox_to_anchor=(1.1, 0.5))

# %%
plt.figure()
plt.plot(span.Coord, span.QperpA)

histdata.to_csv(OUTPUTFILE_FULL)
span.to_csv(OUTPUTFILE_SPAN)
