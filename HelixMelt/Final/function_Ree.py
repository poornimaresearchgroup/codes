# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 08:53:23 2022

@author: Acer
"""

import math
import os

import matplotlib.pyplot as plt
import pandas as pd

# Input FILEPATH and trj file to be examined, span for Ree
SIMULATION = "K5"
FILEPATH = "C:/Users/nxb1090/Desktop/GradResearch/Buchanan/Projects/" + \
           "HelixMelt/Data/LowerPressure/T95P215/" + SIMULATION
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "Ree.txt"
DELTA_T = 0.0005
print(FILEPATH + "\n")

# %% Time and Box Info

ReeList = []
dir_list = os.listdir(FILEPATH + '/LAMMPS/')
i = 0
for root, dirs, files in os.walk(FILEPATH + '/LAMMPS/'):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not "Old" in d]
    i = i + 1
    for file in files:
        if file == "lam.lammpstrj":
            filepath = root + '/' + file
            ReeList.append(filepath)
for runcounter in range(0, len(ReeList)):
    print('Run Counter: ' + str(runcounter))
    FILENAME = ReeList[runcounter]

    # Number of atoms from first timestep of trj file
    headerinfo = pd.read_csv(FILENAME, nrows=7)
    atoms = int(headerinfo.iloc[2, 0])

    # TimeReeData stores mean-squared end-to-end distance for each time step
    TimeReeData = pd.DataFrame(columns=["Time", "Ree^2", "A_Ree", "B_Ree"])
    TimeBoxData = pd.DataFrame(columns=["Time", "Lx", "Ly", "Lz"])
    timecounter = 0

    # Takes each time step of a chunksize
    with pd.read_csv(FILENAME, chunksize=(9), skiprows=(lambda x: x % (atoms + 9)
                                                                  >= 9)) as reader:
        for chunk in reader:
            time = int(chunk.iloc[0, 0])  # time associated with chunk
            # Get box dimensions at current timestep
            [xlo, xhi, ylo, yhi, zlo, zhi] = [float(chunk.iloc[4, 0].split()[0]),
                                              float(chunk.iloc[4, 0].split()[1]),
                                              float(chunk.iloc[5, 0].split()[0]),
                                              float(chunk.iloc[5, 0].split()[1]),
                                              float(chunk.iloc[6, 0].split()[0]),
                                              float(chunk.iloc[6, 0].split()[1])]
            TimeBoxData.loc[timecounter] = [time, xhi - xlo, yhi - ylo, zhi - zlo]
            timecounter += 1
    print("Done with Box Info")
    # %% Chunker
    timecounter = 0
    with pd.read_csv(FILENAME, chunksize=(atoms), sep=" ",
                     skiprows=(lambda x: x % (atoms + 9) < 9),
                     skipinitialspace=(True),
                     names=["id", 'mol', 'type', 'x', 'y', 'z',
                            'ix', 'iy', 'iz', "test"]) as reader:
        for chunk in reader:
            if timecounter % 5 == 0: print(timecounter)
            chunk.drop("test", axis=1, inplace=True)
            time = TimeBoxData.loc[timecounter, "Time"]

            if time != TimeBoxData.loc[timecounter, "Time"]:
                raise ValueError("Times Don't Match \n" + str(time) + "    " +
                                 str(TimeBoxData.loc[timecounter, "Time"]))

            Lx = TimeBoxData.loc[timecounter, "Lx"]
            Ly = TimeBoxData.loc[timecounter, "Ly"]
            Lz = TimeBoxData.loc[timecounter, "Lz"]

            chunk["x"] = (chunk["x"] + chunk["ix"]) * Lx
            chunk["ix"] = 0
            chunk["y"] = (chunk["y"] + chunk["iy"]) * Ly
            chunk["iy"] = 0
            chunk["z"] = (chunk["z"] + chunk["iz"]) * Lz
            chunk["iz"] = 0

            group_object = chunk.groupby(["mol", "type"])
            first = chunk.loc[group_object.id.idxmin()]  # first bead of every block
            first.reset_index(inplace=True, drop=True)
            last = chunk.loc[group_object.id.idxmax()]  # last bead of every block
            last.reset_index(inplace=True, drop=True)

            tempData = pd.DataFrame(columns=
                                    ["mol", "type", "DN", "DX", "DY", "DZ",
                                     "Distance"])
            tempData.mol = first.mol
            if not last.mol.equals(tempData.mol):
                raise ValueError("Molecule IDs don't match")
            tempData.type = first.type
            if not last.type.equals(tempData.type):
                raise ValueError("Type IDs don't match")

            # Distances for each block
            tempData.DN = abs(first.id.sub(last.id)) + 1
            tempData.DX = first.x.sub(last.x)
            tempData.DY = first.y.sub(last.y)
            tempData.DZ = first.z.sub(last.z)

            tempData.Distance = (tempData.DX ** 2) + (tempData.DY ** 2) + \
                                tempData.DZ ** 2
            tempData.Distance = tempData.Distance.apply(math.sqrt)
            tempData.Distance = tempData.Distance.div(tempData.DN)

            histData = tempData
            TimeReeData.loc[timecounter] = [time, float("NaN"),
                                            tempData.groupby("type").Distance.mean()[1],  # Ablock distance
                                            tempData.groupby("type").Distance.mean()[2]]  # Block distance

            group_object = chunk.groupby(["mol"])
            first = chunk.loc[group_object.id.idxmin()]
            first.reset_index(inplace=True, drop=True)
            last = chunk.loc[group_object.id.idxmax()]
            last.reset_index(inplace=True, drop=True)

            tempData = pd.DataFrame(columns=
                                    ["mol", "DN", "DX", "DY", "DZ", "Distance"])
            tempData.mol = first.mol
            if not last.mol.equals(tempData.mol):
                raise ValueError("Molecule IDs don't match")

            tempData.DN = abs(first.id.sub(last.id)) + 1
            tempData.DX = first.x.sub(last.x)
            tempData.DY = first.y.sub(last.y)
            tempData.DZ = first.z.sub(last.z)

            tempData.Distance = (tempData.DX ** 2) + (tempData.DY ** 2) + \
                                tempData.DZ ** 2
            tempData.Distance = tempData.Distance.div(tempData.DN)

            TimeReeData.loc[timecounter, "Ree^2"] = tempData.Distance.mean()
            timecounter += 1

            if "Run1" in FILENAME:
                TimeReeDataFull = TimeReeData

            else:
                TimeReeDataFull = pd.concat([TimeReeDataFull, TimeReeData],
                                            ignore_index=True, axis=0)
print("Complete")

# %% Adjust Time
TimeReeDataFull.Time = TimeReeDataFull.Time * DELTA_T
TimeReeDataFull.sort_values(by=['Time'], inplace=True)
# %% Figures
plt.figure()
plt.plot(TimeReeDataFull.Time, TimeReeDataFull.A_Ree, "r", label="A")
plt.plot(TimeReeDataFull.Time, TimeReeDataFull.B_Ree, "b", label="B")
# plt.plot(TimeReeDataFull.Time, TimeReeDataFull["Ree^2"], "k", label="Total")
plt.legend()
plt.xlabel("Time");
plt.ylabel(r'$R_{ee}^2/N$')
ax = plt.gca()
ax.ticklabel_format(axis='x', style='scientific', scilimits=(0, 0))
# plt.xlim([0, 2E4])

plt.figure()
plt.hist(tempData.Distance)
plt.title(SIMULATION);
plt.xlabel(r'$R_{ee}^2$');
plt.ylabel("Frequency")

testA, testB = histData.groupby("type")
plt.figure()
plt.hist(testA[1].Distance,
         bins=[0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5],
         color='r')
plt.hist(testB[1].Distance,
         bins=[0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5],
         color='b', alpha=0.5)
plt.legend(["A", "B"])
plt.title(SIMULATION)
plt.xlabel(r'$R_{ee}^2$');
plt.ylabel("Frequency")

height = TimeBoxData.shape[0]
TimeReeDataSpan = TimeReeData[height - 11:]
print(TimeReeData.describe())
# print(TimeReeDataSpan.describe())

TimeReeData.to_csv(OUTPUTFILE)
