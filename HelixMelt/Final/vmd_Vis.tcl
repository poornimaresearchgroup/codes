mol representation VDW 0.3 12
pbc wrap -all
pbc box

puts "Enter ResID"
gets stdin target

proc reset_viz {molid} {
  # operate only on existing molecules
  if {[lsearch [molinfo list] $molid] >= 0} {
    # delete all representations
    set numrep [molinfo $molid get numreps]
    for {set i 0} {$i < $numrep} {incr i} {
      mol delrep $i $molid
    }
    # add new representations, based on Graphical Representations screen
  } 

}

set string1 "resid"
append string1 " " $target
set sel [atomselect top $string1]
mol selection [$sel text]
mol representation VDW 0.3 1000
mol addrep top

set string2 "resid"
append string2 " 1 to " [expr $target - 1]
set sel [atomselect top $string2]
mol selection [$sel text]
mol representation Points 1
mol addrep top

set string2 "resid"
append string2 " " [expr $target + 1] " to 600"
set sel [atomselect top $string2]
mol selection [$sel text]
mol representation Points 1
mol addrep top

set molid [molinfo list]
mol delrep 0 $molid

# Create a TCL dictionary for models
array set models {
    model1_r 0.8
    model1_g 0
    model1_b 0.8

    model2_r 0
    model2_g 0
    model2_b 1

    model3_r 0
    model3_g 0.5
    model3_b 1
	
    model4_r 0
    model4_g 0.8
    model4_b 0.8

    model5_r 0
    model5_g 0.8
    model5_b 0.5
}

# Prompt user to choose a model
puts "Enter the model number (1 - 5) to display its data:"
set userInput [gets stdin]
set modelNumber [string trim $userInput]

set string_r "_r"
set string_g "_g"
set string_b "_b"

puts "Model $modelNumber - Red: $models(model$modelNumber$string_r)"
puts "Model $modelNumber - Green: $models(model$modelNumber$string_g)"
puts "Model $modelNumber - Blue: $models(model$modelNumber$string_b)"

set r_value $models(model$modelNumber$string_r)
set g_value $models(model$modelNumber$string_g)
set b_value $models(model$modelNumber$string_b)


color change rgb cyan $r_value $g_value $b_value
color Name 2 cyan

mol modmaterial 1 $molid "AOChalky"
display projection orthographic
display resetview
mol list
animate goto end
rotate y by angle 90 
scale by 2
display ambientocclusion on

