# -*- coding: utf-8 -*-
"""
Updated on Fri Jan 20 11:54:54 2023

@author: nxb1090
@version: 3.9.15

"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.signal as sc

import ToolBox as tb

## NOTE: Need to change time adjustments from file to file

# Input FILEPATH and trj file to be examined
SIMULATION = "K5"
FILEPATH = "C:/Users/nxb1090/Desktop/GradResearch/Buchanan/Projects/" + \
           "HelixMelt/Data/LowerPressure/T95P215/" + SIMULATION
# LOGFILE = FILEPATH + "/LAMMPS/Run4/" + "log.lammps"
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "ZProfile.txt"

A_files = []
B_files = []
logDataFull = pd.DataFrame(columns=["Time", "Lx", "Ly", "Ly"])
# logDataFull.set_index("Time", inplace=True)

dir_list = os.listdir(FILEPATH + '/LAMMPS/')
i = 0
print("START")
for root, dirs, files in os.walk(FILEPATH + '/LAMMPS/'):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not "Old" in d]
    for file in files:
        if file == "zprofile-A.txt":
            filepath = root + '/' + file
            A_files.append(filepath)
        if file == "zprofile-B.txt":
            filepath = root + '/' + file
            B_files.append(filepath)
        if file == "log.lammps":
            i = i + 1
            LOGFILE = root + '/' + file
            logFrame = tb.listmaker(open(LOGFILE, "r"))
            logFrame = pd.DataFrame(logFrame)
            logFrame.drop(logFrame.columns[logFrame.columns > 13].values,
                          inplace=True, axis=1)
            # logFrame.drop([14,15,16,17], inplace=True, axis = 1) #Cuts extra columns that arise due to text delimination
            logFrame = logFrame.convert_dtypes(infer_objects=True, convert_floating=True, convert_string=True)
            # logFrame.columns = ["Time", "Temp", "Press", "K*", "Lx", "Ly", "Lz"]
            logFrame.columns = ["Time", "K*", "Temp", "Press", "Pxx", "Pyy", "Pzz", "PE",
                                "LJPE", "Vol", "Dens", "Lx", "Ly", "Lz"]

            # logData is only the data rows, where every value is a number. No text
            logData = pd.DataFrame(
                columns=["Time", "Lx", "Ly", "Lz"])  # columns = ["Time", "K*","Temp","Press","Pxx","Pyy",
            #                                  "Pzz", "PE","LJPE", "Vol","Dens","Lx","Ly",
            #                                 "Lz"])
            for n, row in logFrame.iterrows():
                row = row.astype(float, errors="ignore")
                if row.dtype == float:  # if all values are numbers, append to logData
                    temp = pd.DataFrame({"Time": row.Time, "Lx": row.Lx, "Ly": row.Ly,
                                         "Lz": row.Lz}, index=[0])
                    logData = pd.concat([logData, temp])
            logData.Time.astype(int)

            # Timestep Adjustment
            if i == 1:
                logDataFull = logData
                # logData.Time = logData.Time/2
                # run1time = logDataFull.Time.iloc[-1] - 500000
            else:
                # if i == 2:
                # logData.Time = logData.Time + 10000000
                #     run2time = logData.Time.iloc[0]
                #     logData.Time = logData.Time - run2time + run1time
                # if i == 3:
                #     logData.Time = logData.Time - run2time + run1time
                logData.set_index("Time", inplace=True)
                # logData.drop_duplicates(inplace = True)
                logData = logData[~logData.index.duplicated(keep='first')]
                logData = logData.reset_index()
                print(logData.shape)
                logDataFull = pd.concat([logDataFull, logData], axis=0, join='outer', ignore_index=True)
logDataFull.set_index("Time", inplace=True)
logDataFull = logDataFull[~logDataFull.index.duplicated(keep='first')]

print(SIMULATION)
# %% Import profiles as panda array
TimeBoxData = pd.DataFrame(columns=["Time", "DomainSize", "AWidth", "BWidth",
                                    "AHeight", "BHeight"])

headerinfo = pd.read_csv(A_files[0], nrows=10, comment="#", sep=" ")
NUMCHUNKS = int(headerinfo.columns[1])

stepdata = pd.DataFrame(columns=["Timestep", "Number-of-Chunks", "Total-count"])
# Get header info for each time step
j = 0
for filename in A_files:
    # Timestep Adjustment
    j = j + 1
    num_lines = sum(1 for line in open(filename))
    steplist = [*range(3, num_lines, NUMCHUNKS + 1)]
    to_exclude = [i for i in range(num_lines) if i not in steplist]
    temp = pd.read_csv(filename, skiprows=to_exclude,
                       names=["Timestep", "Number-of-Chunks", "Total-count"],
                       sep=" ", comment="#")
    # if j>5:
    #     temp.Timestep = temp.Timestep + run1time[0]
    stepdata = pd.concat([stepdata, temp])

# %%
timecounter = 0;
filecounter = 0
# Takes each time step for A Density Profile
for filename in A_files:
    filecounter = filecounter + 1
    with pd.read_csv(filename, chunksize=NUMCHUNKS + 1, header=3, sep=" ",
                     skipinitialspace=True,
                     names=["Chunk", "Coord", "Ncount", "mass_dens",
                            "num_dens"]) as reader:
        for chunk in reader:
            # Drops header info
            chunk.replace("", float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            # Raises error if first and last chunks in range are not the IDs expected
            if chunk.Chunk.iloc[0] != 1 or chunk.Chunk.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" +
                                 str(chunk.Chunk.iloc[0]) + "    " +
                                 str(chunk.Chunk.iloc[-1]))
            # Finds data from header at same time
            time = stepdata.iloc[timecounter]["Timestep"]
            # Time Adjustments
            # if "Run1" in filename:
            #     time = time/2
            # if "Run2" in filename:
            #     time = time + 10000000
            # elif "Run3" in filename:
            #     time = time - run2time + run1time
            totalACount = stepdata.iloc[timecounter]["Total-count"]
            averageN = chunk.Ncount.mean()
            chunk["vol_frac"] = chunk.Ncount.div(2 * averageN)

            # Converts coord from scaled to absolute
            try:
                Lz = logDataFull.loc[float(time)]["Lz"]
                chunkA = chunk
                chunkA.Coord = chunkA.Coord.mul(Lz)
            except:
                print('BREAK:' + str(time))
                break

            peaks = sc.find_peaks(chunkA.vol_frac, prominence=0.5)
            widths = sc.peak_widths(chunkA.vol_frac, peaks[0])
            heightsA = chunkA.iloc[peaks[0]]["vol_frac"]
            # if len(widths[0]) > 3:
            # plt.figure()
            # plt.plot(chunkA.Coord, chunkA.vol_frac, "r")
            # plt.title(time)
            widthsA = widths[0] * Lz / NUMCHUNKS
            TimeBoxData.loc[timecounter] = [time, Lz / 4, widthsA, [], [], []]
            TimeBoxData.iloc[timecounter]["AHeight"].extend(heightsA)
            timecounter += 1

print(timecounter)
# %% Takes each time step for B Density Profile
timecounter = 0
# print("B START")

for filename in B_files:
    with pd.read_csv(filename, chunksize=NUMCHUNKS + 1, header=3, sep=" ",
                     skipinitialspace=True,
                     names=["Chunk", "Coord", "Ncount", "mass_dens",
                            "num_dens"]) as reader:
        for chunk in reader:
            # Drops header info
            chunk.replace("", float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            # Raises error if first and last chunks in range are not the IDs expected
            if chunk.Chunk.iloc[0] != 1 or chunk.Chunk.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" +
                                 str(chunk.Chunk.iloc[0]) + "    " +
                                 str(chunk.Chunk.iloc[-1]))
            # Finds data from header at same time
            time = stepdata.iloc[timecounter]["Timestep"]

            # Time Adjustments
            # if "Run1" in filename:
            #     time = time/2
            # if "Run2" in filename:
            #     time = time + 10000000
            # elif "Run3" in filename:
            #     time = time - run2time + run1time

            totalBCount = stepdata.iloc[timecounter]["Total-count"]
            averageN = chunk.Ncount.mean()
            chunk["vol_frac"] = chunk.Ncount.div(2 * averageN)
            # Converts coord from scaled to absolute
            try:
                Lz = logDataFull.loc[float(time)]["Lz"]
                chunkB = chunk
                chunkB.Coord = chunkB.Coord.mul(Lz)
            except:
                print("B BREAK: " + str(time))
                break

            # Prominence ignores smalls jags
            peaks = sc.find_peaks(chunkB.vol_frac, prominence=0.5)
            widths = sc.peak_widths(chunkB.vol_frac, peaks[0])
            widthsB = widths[0] * Lz / NUMCHUNKS
            heightsB = chunkB.iloc[peaks[0]]["vol_frac"]

            # if len(widths[0]) > 3:
            # plt.figure()
            # plt.plot(chunkB.Coord, chunkB.vol_frac, "b")
            widthsB = widths[0] * Lz / NUMCHUNKS

            TimeBoxData.iloc[timecounter]['BWidth'].extend(widthsB)
            TimeBoxData.iloc[timecounter]["BHeight"].extend(heightsB)

            timecounter += 1
# %% Output
TimeBoxData.sort_values(by=['Time'], inplace=True)
plt.figure()
plt.plot(TimeBoxData.Time, TimeBoxData.DomainSize)
plt.xlabel("Time");
plt.ylabel("Domain Size ($\sigma$)")

plt.figure()
plt.plot(chunkA.Coord, chunkA.vol_frac, color=(1, 0.5, 0.5))
plt.plot(chunkB.Coord, chunkB.vol_frac, color=(0.5, 0, 1))
# plt.plot(chunkB.Coord, chunkA.vol_frac+ chunkB.vol_frac, "k")
plt.xlabel(r"$z/L_z$");
plt.ylabel("Vol frac")
# plt.legend(["A", "B"])

height = TimeBoxData.shape[0]
# TimeBoxSpan=TimeBoxData[int(height*0.9):]
TimeBoxSpan = TimeBoxData[height - 11:]

logHeight = logDataFull.shape[0];
print("Lx: " + str(logDataFull.Lx[int(logHeight * 0.95):].mean()))
print("Lx Std: " + str(logDataFull.Lx[int(logHeight * 0.95):].std()))

print("Lz: " + str(logDataFull.Lz[int(logHeight * 0.95):].mean()))
print("Lz Std: " + str(logDataFull.Lz[int(logHeight * 0.95):].std()))

SArho = logDataFull.Lx * logDataFull.Ly / (500 / 4);
print("SArho: " + str(SArho[int(logHeight * 0.95):].mean()))
print("SArho Std: " + str(SArho[int(logHeight * 0.95):].std()) + "\n")

print("Domain Size: " + str(TimeBoxSpan.DomainSize.mean()))
print("Domain Size Std: " + str(TimeBoxSpan.DomainSize.std()))

print("Avg A Width: " + str(np.mean(TimeBoxSpan.AWidth.explode())))
print("Std: " + str(np.std(TimeBoxSpan.AWidth.explode())))
print("Avg B Width: " + str(np.mean(TimeBoxSpan.BWidth.explode())))
print("Std: " + str(np.std(TimeBoxSpan.BWidth.explode())))

print("Avg A Height: " + str(np.mean(TimeBoxSpan.AHeight.explode())))
print("Std: " + str(np.std(TimeBoxSpan.AHeight.explode())))
print("Avg B Height: " + str(np.mean(TimeBoxSpan.BHeight.explode())))
print("Std: " + str(np.std(TimeBoxSpan.BHeight.explode())))

TimeBoxData.to_csv(OUTPUTFILE)
chunkA.to_csv(FILEPATH + "/PostProcessing/chunkA.txt")
chunkB.to_csv(FILEPATH + "/PostProcessing/chunkB.txt")
