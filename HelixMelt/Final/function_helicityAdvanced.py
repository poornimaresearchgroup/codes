# -*- coding: utf-8 -*-
"""
Finds helicity at every time step using inputed set points and tolerance,
following Boehm equation for helicity. Find average helicity of last 10% of
timesteps captured in angle/dihedral dump files

Inputs:
    SIMULATION = name of sim for name of folder and text inputs and outputs
    FILEPATH = directory where data is saved and outputs will be saved
    ANGLEFILE = name of angle dump file
    DIHEDRALFILE = name of dihedral dump file
    OUTPUTFILE = name of csv file containing average helicity at each
        timestep
    TOLERANCE = criteria for helicity. How far from the set points phi and
        theta can be and still counted as helical
    SET_THETA = set value of theta0. Some issues with wrapping.
    SET_PHI = set value of phi0. Some issues with wrapping. Not equal to
        LAMMPS setting. LAMMPS Phi = 200 -> SET_PHI = 20

Outputs:
    csv file containing helicity at each timestep
    Plot 1: Helicity at every timestep captured in the dump file
    Prints: Average helicity of the last 10% of timesteps in the dump files

Hard-Coded:
    *header length and position of timestep and box information
        (headerinfo, angleNum, dihedralNum, angle_time, dihedral_time, xlo..)
    *Currently for single chain. May need index adjustments if expanding to
        melts
    *Contents and angle and dihedral dumps (currentAngle, currentDihedral)
Error Messages:
    * "Time error": timesteps read from equivalent angle and dihedral chunks
        do not match. Check angleChink and dihedralChunk
    * "Helicity = 0. Check set points": Averag helicity found to be 0 which
        may indicate issues with set point wrapping

@author: Natalie Buchanan
@pytho: 3.9.15
"""

import math
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import ToolBox as tb

# Input FILEPATH and trj file to be examined
SIMULATION = "K1"
FILEPATH = "C:/Users/nxb1090/Desktop/GradResearch/Buchanan/Projects/" + \
           "HelixMelt/Data/LowerPressure/T80P240/" + SIMULATION
# ANGLEFILE = FILEPATH + "/LAMMPS/" + "angle.dump"
# DIHEDRALFILE = FILEPATH + "/0AMMPS/" + "dihedral.dump"
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "Helicity.txt"
OUTPUTFILEAVG = FILEPATH + "/PostProcessing/" + "HelicityAvg.txt"
OUTPUTFILEGRAPH = FILEPATH + "/PostProcessing/" + "HelicityByZ.txt"
TOLERANCE = 20
SET_THETA = 80
SET_PHI = 240 - 180
DELTA_T = 0.0005
print(SIMULATION + " Helicity" + "\n")

AFILES = []
DFILES = []
TRJFILES = []
TimeHelicityDataFull = pd.DataFrame(columns=["Time", "Helicity"])
BProfile = pd.read_csv(FILEPATH + "/PostProcessing/chunkB.txt")

dir_list = os.listdir(FILEPATH + '/LAMMPS/')
runCount = 0
for root, dirs, files in os.walk(FILEPATH + '/LAMMPS/'):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not "Old" in d]
    print(dirs)
    runCount = runCount + 1
    for file in files:
        if file == "angle.dump":
            filepath = root + '/' + file
            AFILES.append(filepath)
        if file == "dihedral.dump":
            filepath = root + '/' + file
            DFILES.append(filepath)
        if file == "lam.lammpstrj":
            filepath = root + '/' + file
            TRJFILES.append(filepath)
runCount = len(AFILES)
# %%
for folder in range(0, runCount):
    ANGLEFILE = AFILES[folder]
    DIHEDRALFILE = DFILES[folder]
    TRJFILE = TRJFILES[folder]

    # Number of angles from first timestep of angle file
    headerinfo = pd.read_csv(ANGLEFILE, nrows=9)
    angleNum = int(headerinfo.iloc[2, 0])

    # Number of dihedrals from first timestep of dihedral file
    headerinfo = pd.read_csv(DIHEDRALFILE, nrows=9)
    dihedralNum = int(headerinfo.iloc[2, 0])

    headerinfo = pd.read_csv(TRJFILE, nrows=7)
    atoms = int(headerinfo.iloc[2, 0])

    angle_chunker = pd.read_csv(ANGLEFILE, chunksize=angleNum + 9)
    dihedral_chunker = pd.read_csv(DIHEDRALFILE, chunksize=dihedralNum + 9)
    trj_chunker = pd.read_csv(TRJFILE, chunksize=(atoms), sep=" ",
                              skiprows=(lambda x: x % (atoms + 9) < 9),
                              skipinitialspace=(True),
                              names=["id", 'mol', 'type', 'x', 'y', 'z',
                                     'ix', 'iy', 'iz'])

    TimeHelicityData = pd.DataFrame(columns=["Time", "Helicity"])
    timecounter = 0

    # For single timeset of data in angle and dihedral files
    for angleChunk, dihedralChunk, trjChunk in zip(
            angle_chunker, dihedral_chunker, trj_chunker):
        angle_time = int(angleChunk.iloc[0, 0])
        dihedral_time = int(dihedralChunk.iloc[0, 0])
        trjChunk.set_index("id", inplace=True)

        print(angle_time)
        if angle_time != dihedral_time:  # checks that on same timestep
            raise ValueError("Times Don't Match \n" + str(angle_time) + "    "
                             + str(dihedral_time))
            # Get box dimensions at current timestep
        [xlo, xhi, ylo, yhi, zlo, zhi] = [float(angleChunk.iloc[4, 0].split()[0]),
                                          float(angleChunk.iloc[4, 0].split()[1]),
                                          float(angleChunk.iloc[5, 0].split()[0]),
                                          float(angleChunk.iloc[5, 0].split()[1]),
                                          float(angleChunk.iloc[6, 0].split()[0]),
                                          float(angleChunk.iloc[6, 0].split()[1])]

        Lx = xhi - xlo
        Ly = yhi - ylo
        Lz = zhi - zlo

        trjChunk["x"] = (trjChunk["x"] + trjChunk["ix"]) * Lx
        trjChunk["ix"] = 0
        trjChunk["y"] = (trjChunk["y"] + trjChunk["iy"]) * Ly
        trjChunk["iy"] = 0
        trjChunk["z"] = (trjChunk["z"] + trjChunk["iz"]) * Lz
        trjChunk["iz"] = 0

        # Removes header and creates data table sorted by atom ID for angle data
        currentAngle = [item[0].split() for item in
                        angleChunk.iloc[8:angleNum + 8].values.tolist()]
        currentAngle = pd.DataFrame(currentAngle,
                                    columns=['id', "type", 'atom1', 'atom2',
                                             'atom3', 'Theta', 'Eng'],
                                    dtype='float')
        # Sorts by atom id
        currentAngle["id"] = currentAngle["id"].astype("int64")
        currentAngle.set_index("atom1", inplace=True)
        currentAngle.index = currentAngle.index.astype("int64")
        currentAngle = currentAngle.sort_values("atom1")

        # Removes header and create data table sorted by atom ID for dihedral data
        currentDihedral = [item[0].split() for item in
                           dihedralChunk.iloc[8: dihedralNum + 8].values.tolist()]
        currentDihedral = pd.DataFrame(currentDihedral,
                                       columns=['id', "type", 'atom1', 'atom2',
                                                'atom3', 'atom4', 'Phi'],
                                       dtype='float')
        # Sorts by atom id
        currentDihedral["id"] = currentDihedral["id"].astype("int64")
        currentDihedral.set_index("atom1", inplace=True)
        currentDihedral = currentDihedral.sort_values("atom1")

        # Creates dataframe for current timestep
        currentHelicity = pd.DataFrame(index=list(currentDihedral.index.values),
                                       columns=["Theta", "Theta Diff",
                                                "Theta Spec", "Phi",
                                                "Phi Diff", "Phi Spec",
                                                "Residue", "Z"],
                                       dtype="float")
        currentHelicity.index = currentHelicity.index.astype("int64")
        currentHelicity.update(currentAngle)
        currentHelicity.update(currentDihedral)

        # Finds difference between setpoint and theta with wrapping
        currentHelicity["Theta Diff"] = currentHelicity["Theta"] - SET_THETA
        currentHelicity.loc[(currentHelicity["Theta Diff"]) >
                            180, "Theta Diff"] -= 360
        currentHelicity.loc[(currentHelicity["Theta Diff"]) < -180,
        "Theta Diff"] += 360

        # Theta Spec = 1 if theta is within spec
        currentHelicity["Theta Spec"] = np.where(abs(currentHelicity["Theta Diff"])
                                                 <= TOLERANCE, 1, 0)

        # Finds difference between setpoint and phi with wrapping
        currentHelicity["Phi Diff"] = currentHelicity["Phi"] - SET_PHI
        currentHelicity.loc[(currentHelicity["Phi Diff"]) >
                            180, "Phi Diff"] -= 360
        currentHelicity.loc[(currentHelicity["Phi Diff"]) <
                            -180, "Phi Diff"] += 360
        # Phi Spec = 1 if within Tolerance
        currentHelicity["Phi Spec"] = np.where(abs(currentHelicity["Phi Diff"])
                                               <= TOLERANCE, 1, 0)
        currentHelicity["Residue"] = np.where((currentHelicity["Theta Spec"] == 1)
                                              & (currentHelicity["Phi Spec"] == 1),
                                              1, 0)
        currentHelicity["Z"] = trjChunk.loc[currentHelicity.index, "z"]
        currentHelicity["Z"] = currentHelicity["Z"].apply(tb.wrap, Lbox=Lz)

        currentHelicity["binID"] = [BProfile.iloc[(BProfile['Coord'] - val)
        .abs().argsort()[0]]['Coord']
                                    for val in currentHelicity['Z']]

        helicity = currentHelicity["Residue"].sum() / dihedralNum
        currentHelicity["ResID"] = currentHelicity.index.values % 60

        resID = currentHelicity.groupby("ResID").mean()
        zID = currentHelicity.groupby("binID").mean()
        zCount = currentHelicity.groupby("binID").count()

        tempHelicityData = pd.DataFrame({"Time": [angle_time],
                                         "Helicity": [helicity]})
        TimeHelicityData = pd.concat([TimeHelicityData, tempHelicityData],
                                     ignore_index=True, axis=0)
        timecounter += 1

        tempRes = resID.Residue.transpose()
        tempRes.rename(str(angle_time), inplace=True)

        tempZ = zID.Residue.transpose()
        tempZ.rename(str(angle_time), inplace=True)

        tempCount = zCount["Residue"]
        tempCount.rename(str(angle_time), inplace=True)

        try:
            ResIDHelicityDataFull
            ResIDHelicityDataFull = pd.concat([ResIDHelicityDataFull, tempRes],
                                              names=(angle_time), axis=1)
            ZHelicityDataFull = pd.concat([ZHelicityDataFull, tempZ],
                                          names=(angle_time), axis=1)
            ZCountFull = pd.concat([ZCountFull, tempCount],
                                   names=(angle_time), axis=1)
        except NameError:
            ResIDHelicityDataFull = pd.DataFrame(index=tempRes.index)
            ResIDHelicityDataFull[(angle_time)] = tempRes
            ZHelicityDataFull = pd.DataFrame(index=tempZ.index)
            ZHelicityDataFull[(angle_time)] = tempZ
            ZCountFull = pd.DataFrame(index=tempCount.index)
            ZCountFull[(angle_time)] = tempCount

    if "Run1" in ANGLEFILE:
        TimeHelicityDataFull = TimeHelicityData
    else:
        TimeHelicityDataFull = pd.concat([TimeHelicityDataFull, TimeHelicityData],
                                         ignore_index=True, axis=0)

# %%
# Saves Helicity per timestep table to text file
TimeHelicityData.Time = TimeHelicityData.Time * DELTA_T
TimeHelicityData.sort_values(by=['Time'], inplace=True)
TimeHelicityDataFull.sort_values(by=['Time'], inplace=True)
TimeHelicityData.to_csv(OUTPUTFILE)

# Average Ree^2 from lat 10% of timesteps
SPAN = int(timecounter * 0.1)
print(SPAN)
print("Time: " + str(TimeHelicityData.Time.max() - TimeHelicityData.Time.min()))
print("Mean Helicity = " + str(TimeHelicityData["Helicity"].iloc
                               [-SPAN:].mean()))
print("Std Helicity = " + str(round(TimeHelicityData["Helicity"].iloc
                                    [-SPAN:].std(), 5)))
print("Mean Helicity = " + str(TimeHelicityDataFull["Helicity"].iloc
                               [timecounter - 11:].mean()))
print("Std Helicity = " + str(round(TimeHelicityDataFull["Helicity"].iloc
                                    [timecounter - 11:].std(), 5)))

if abs(TimeHelicityDataFull["Helicity"].iloc[-SPAN:].mean()) < 0.01:
    print("Helicity = 0. Check set points")
print("Average Phi = " + str(round(currentHelicity["Phi"].mean(), 3)) +
      "; Set Phi = " + str(SET_PHI))
print("Average Theta = " + str(round(currentHelicity["Theta"].mean(), 3)) +
      "; Set Theta = " + str(SET_THETA))

# Figure 1: Helicity over time
plt.figure()
plt.plot(TimeHelicityDataFull["Time"], TimeHelicityDataFull["Helicity"], lw=2)
plt.ylim([0, 1])
plt.xlabel("Time")
plt.ylabel("Helicity")
plt.annotate("Avg Hel = " +
             str(round(TimeHelicityData["Helicity"][-SPAN:].mean(), 3)),
             (TimeHelicityData["Time"].iloc[-1],
              TimeHelicityData["Helicity"].iloc[-1]),
             (.75, 0.9), "axes fraction")
plt.show()

ResIDSpan = ResIDHelicityDataFull.iloc[:,
            math.floor(len(ResIDHelicityDataFull.columns) * 0.9):len(ResIDHelicityDataFull.columns)]

plt.figure()
plt.plot(ResIDSpan.index.values, ResIDSpan.mean(axis=1), lw=2)
plt.ylim([0, 1])
plt.xlabel("ResID")
plt.ylabel("Helicity")
plt.annotate("Avg Hel = " +
             str(round(TimeHelicityData["Helicity"][-SPAN:].mean(), 3)),
             (TimeHelicityData["Time"].iloc[-1],
              TimeHelicityData["Helicity"].iloc[-1]),
             (.75, 0.9), "axes fraction")

# %% Weights
ZSpan = ZHelicityDataFull.iloc[:, math.floor(len(ZHelicityDataFull.columns) * 0.9):len(ZHelicityDataFull.columns)]
ZSpan.sort_index(inplace=True)
CountSpan = ZCountFull.iloc[:, math.floor(len(ZCountFull.columns) * 0.9):len(ZCountFull.columns)]
CountSpan.sort_index(inplace=True)
CountSpan.fillna(0, inplace=True)
test = ZSpan.mul(CountSpan)
test.fillna(0, inplace=True)
weight_average = test.sum(axis=1) / CountSpan.sum(axis=1)

# weight_std = np.sqrt(np.average((ZSpan - weight_average)**2, weights=CountSpan))
diff = ZSpan.sub(weight_average, axis='index')
diff = diff ** 2
weight_std = diff.sum(axis=1) / CountSpan.sum(axis=1)
weight_std = np.sqrt(weight_std)

fig, ax1 = plt.subplots()
ax1.scatter(weight_average.index, weight_average, 10, 'k', label='helicity')
ax1.set_xlabel('z')
ax1.set_ylabel('Helicity', color='k')
ax1.tick_params(axis='y', labelcolor='k')
plt.ylim([0, 1])
ax2 = ax1.twinx()
ax2.set_ylabel(r'$\phi$', color='b')
ax2.bar(BProfile.Coord, BProfile.vol_frac, color="b", label=r'$\phi_B$',
        align='center',
        width=1,
        alpha=0.1)
ax2.tick_params(axis='y', labelcolor='b')
plt.ylim([0, 1])
fig.tight_layout()
plt.title(SIMULATION + ' B')
fig.legend(bbox_to_anchor=(1.1, 0.5))

fig, ax1 = plt.subplots()
ax1.errorbar(weight_average.index, weight_average, yerr=weight_std,
             color="k", marker="o", linestyle="None", capsize=2,
             label='helicity')
ax1.set_xlabel('z')
ax1.set_ylabel('Helicity', color='k')
ax1.tick_params(axis='y', labelcolor='k')
plt.ylim([0, 1])
ax2 = ax1.twinx()
ax2.set_ylabel(r'$\phi$', color='b')
ax2.bar(BProfile.Coord, BProfile.vol_frac, color="b", label=r'$\phi_B$',
        align='center',
        width=1,
        alpha=0.1)
ax2.tick_params(axis='y', labelcolor='b')
plt.ylim([0, 1])
fig.tight_layout()
plt.title(SIMULATION + ' B')
fig.legend(bbox_to_anchor=(1.1, 0.5))

graph = pd.concat([weight_average, weight_std], axis=1)
graph.columns = ["average", "std"]
graph["Count"] = CountSpan.sum(axis=1)
TimeHelicityData.to_csv(OUTPUTFILEAVG)
graph.to_csv(OUTPUTFILEGRAPH)
