# Default Representation For Increased Speed
mol representation VDW 0.3 12

# User input directory path for where in.data and lammpstrj data is located, and where images will be placed
puts "Enter Directory Path"
gets stdin dirpath
puts [vmdinfo arch] 
set sel [topo readlammpsdata "${dirpath}/LAMMPS/in.data" molecular]
mol addfile "${dirpath}/LAMMPS/sim.lammpstrj" type lammpstrj waitfor all
file mkdir "${dirpath}/PostProcessing/Movie"

# Sets the view for the video. Note: The box remains static. No rotation seen in movie.
pbc wrap -all
pbc box 
display resetview
mol list
animate goto start

rotate y to 45
rotate x by 30 
scale to 0.05
display ambientocclusion on

# Removes any unneeded representations
proc reset_viz {molid} {
  # operate only on existing molecules
  if {[lsearch [molinfo list] $molid] >= 0} {
    # delete all representations
    set numrep [molinfo $molid get numreps]
    for {set i 0} {$i < $numrep} {incr i} {
      mol delrep $i $molid
    }
    # add new representations, based on Graphical Representations screen
	set numrep [molinfo $molid get numreps]
	for {set j 0} {$j < $numrep} {incr j} {
		# Should set bead properties that will be seen in the movie
		mol representation VDW 0.3 12
		mol modrep $j $molid
		mol modmaterial $j $molid AOChalky
	}
  } 
}

# Actually makes the movie
::MovieMaker::moviemaker

# This gives the directory that the movie will be placed in, set in line 10
set MovieMaker::workdir "${dirpath}/PostProcessing/Movie"
set MovieMaker::basename movie
set MovieMaker::movietype trajectory
set ::MovieMaker::renderer libtachyon 
set ::MovieMaker::cleanfiles 0
::MovieMaker::genframes_trajectory 