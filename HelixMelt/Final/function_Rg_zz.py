# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 09:32:01 2023

@author: nxb1090
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Input FILEPATH and trj file to be examined
SIMULATION = "K1"
FILEPATH = "C:/Users/nxb1090/Desktop/GradResearch/Buchanan/Projects/" + \
           "HelixMelt/Data/LowerPressure/T80P240/BackDown/" + SIMULATION
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "RogData.txt"
DELTA_T = 0.0005

RogList = []
ARogList = []
BRogList = []
TimeRogDataFull = pd.DataFrame(columns=["Time", "Rzz", "ARz", "BRz"])

dir_list = os.listdir(FILEPATH + '/LAMMPS/')
i = 0
for root, dirs, files in os.walk(FILEPATH + '/LAMMPS/'):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not "Old" in d]
    i = i + 1
    for file in files:
        if file == "rog.dump":
            filepath = root + '/' + file
            RogList.append(filepath)
        if file == "Arog.dump":
            filepath = root + '/' + file
            ARogList.append(filepath)
        if file == "Brog.dump":
            filepath = root + '/' + file
            BRogList.append(filepath)

for runcounter in range(0, len(RogList)):
    print('Run Counter: ' + str(runcounter))
    ROGFILE = RogList[runcounter]
    AROGFILE = ARogList[runcounter]
    BROGFILE = BRogList[runcounter]

    headerinfo = pd.read_csv(ROGFILE, nrows=0, comment="#", sep=" ")
    NUMCHUNKS = int(headerinfo.columns[1])

    # Get header info for each time step
    num_lines = sum(1 for line in open(ROGFILE, 'r', encoding='cp1252'))
    steplist = [*range(3, num_lines, NUMCHUNKS + 1)]
    to_exclude = [i for i in range(num_lines) if i not in steplist]
    stepdata = pd.read_csv(ROGFILE, skiprows=to_exclude,
                           names=["Timestep", "Number-of-Chunks"],
                           sep=" ")

    TimeRogData = pd.DataFrame(columns=["Time", "Rzz", "ARz", "BRz"])

    # %% Total Mol
    TIMECOUNTER = 0
    # Saves mean ROG of molecule for each timestep
    with pd.read_csv(ROGFILE, chunksize=NUMCHUNKS + 1, header=0, sep=" ",
                     comment="#", names=["Mol", "Rxx", "Ryy", "Rzz", "Rxy", "Rxz",
                                         "Ryz"]) as reader:
        for chunk in reader:
            chunk.where(chunk.Mol <= NUMCHUNKS, float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            chunk.set_index("Mol", drop=False, inplace=True)
            # Raises error if first and last chunks in range are not the IDs expected
            if chunk.Mol.iloc[0] != 1 or chunk.Mol.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" +
                                 str(chunk.Mol.iloc[0]) + "    " +
                                 str(chunk.Mol.iloc[-1]))
            chunk["Raz"] = np.nan
            for mol in range(NUMCHUNKS):
                Smatrix = np.array([[chunk.Rxx.iloc[mol], chunk.Rxy.iloc[mol],
                                     chunk.Rxz.iloc[mol]],
                                    [chunk.Rxy.iloc[mol], chunk.Ryy.iloc[mol],
                                     chunk.Ryz.iloc[mol]],
                                    [chunk.Rxz.iloc[mol], chunk.Ryz.iloc[mol],
                                     chunk.Rzz.iloc[mol]]])
                Raz = np.sqrt(chunk.Rzz.iloc[mol])
                chunk.loc[mol + 1, "Raz"] = Raz
            # Finds data from header at same time
            time = stepdata.iloc[TIMECOUNTER]["Timestep"]
            TimeRogData.loc[TIMECOUNTER] = [time, np.mean(chunk.Raz), np.nan, np.nan]

            TIMECOUNTER += 1
    # %% A Rog
    TIMECOUNTER = 0
    # Saves mean ROG of molecule for each timestep
    with pd.read_csv(AROGFILE, chunksize=NUMCHUNKS + 1, header=0, sep=" ",
                     comment="#", names=["Mol", "Rxx", "Ryy", "Rzz",
                                         "Rxy", "Rxz", "Ryz"]) as reader:
        for chunk in reader:
            chunk.where(chunk.Mol <= NUMCHUNKS, float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            chunk.set_index("Mol", drop=False, inplace=True)
            # Raises error if first and last chunks in range are not the IDs expected
            if chunk.Mol.iloc[0] != 1 or chunk.Mol.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" +
                                 str(chunk.Mol.iloc[0]) + "    " +
                                 str(chunk.Mol.iloc[-1]))
            chunk["Raz"] = np.nan
            for mol in range(NUMCHUNKS):
                Smatrix = np.array([[chunk.Rxx.iloc[mol], chunk.Rxy.iloc[mol],
                                     chunk.Rxz.iloc[mol]],
                                    [chunk.Rxy.iloc[mol], chunk.Ryy.iloc[mol],
                                     chunk.Ryz.iloc[mol]],
                                    [chunk.Rxz.iloc[mol], chunk.Ryz.iloc[mol],
                                     chunk.Rzz.iloc[mol]]])
                Raz = np.sqrt(chunk.Rzz.iloc[mol])
                chunk.loc[mol + 1, "Raz"] = Raz
            # Finds data from header at same time
            time = stepdata.iloc[TIMECOUNTER]["Timestep"]
            TimeRogData.iloc[TIMECOUNTER]['ARz'] = np.mean(chunk.Raz)
            TIMECOUNTER += 1
    # %% B Rog
    TIMECOUNTER = 0
    # Saves mean ROG of molecule for each timestep
    with pd.read_csv(BROGFILE, chunksize=NUMCHUNKS + 1, header=0, sep=" ",
                     comment="#", names=["Mol", "Rxx", "Ryy", "Rzz",
                                         "Rxy", "Rxz", "Ryz"]) as reader:
        for chunk in reader:
            chunk.where(chunk.Mol <= NUMCHUNKS, float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            chunk.set_index("Mol", drop=False, inplace=True)
            # Raises error if first and last chunks in range are not the IDs expected
            if chunk.Mol.iloc[0] != 1 or chunk.Mol.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" +
                                 str(chunk.Mol.iloc[0]) + "    " +
                                 str(chunk.Mol.iloc[-1]))
            chunk["Raz"] = np.nan
            for mol in range(NUMCHUNKS):
                Smatrix = np.array([[chunk.Rxx.iloc[mol], chunk.Rxy.iloc[mol],
                                     chunk.Rxz.iloc[mol]],
                                    [chunk.Rxy.iloc[mol], chunk.Ryy.iloc[mol],
                                     chunk.Ryz.iloc[mol]],
                                    [chunk.Rxz.iloc[mol], chunk.Ryz.iloc[mol],
                                     chunk.Rzz.iloc[mol]]])
                Raz = np.sqrt(chunk.Rzz.iloc[mol])
                chunk.loc[mol + 1, "Raz"] = Raz
            # Finds data from header at same time
            time = stepdata.iloc[TIMECOUNTER]["Timestep"]
            TimeRogData.iloc[TIMECOUNTER]['BRz'] = np.mean(chunk.Raz)
            TIMECOUNTER += 1
# Time Adjustments
if "Run1" in ROGFILE:
    # TimeRogData.Time = TimeRogData.Time/2
    # runtime1 = TimeRogData.Time.iloc[-1] - 500000
    TimeRogDataFull = TimeRogData
# elif "Run2" in ROGFILE:
#     TimeRogData.Time = TimeRogData.Time + 10000000
else:
    # if "Run2" in ROGFILE:
    #     start = TimeRogData.Time.iloc[0]
    #     TimeRogData.Time = TimeRogData.Time - start + runtime1
    # if "Run3" in ROGFILE:
    #     TimeRogData.Time = TimeRogData.Time - start + runtime1
    TimeRogDataFull = pd.concat([TimeRogDataFull, TimeRogData],
                                ignore_index=True, axis=0)
# %% Adjust Time
TimeRogDataFull.Time = TimeRogDataFull.Time * DELTA_T
TimeRogDataFull.sort_values(by=['Time'], inplace=True)
# %% Outputs
TimeRogDataFull.to_csv(OUTPUTFILE)
SPAN = int(TIMECOUNTER)
height = TimeRogDataFull.shape[0]
print(SPAN)
print("Time: " + str(TimeRogDataFull.Time.max() - TimeRogDataFull.Time.min()))

print("Mean Rzz = " + str(TimeRogDataFull["Rzz"].iloc
                          [height - 11:].mean()))
print("Std Rzz = " + str(TimeRogDataFull["Rzz"].iloc
                         [height - 11:].std()))
print("Mean A Rzz = " + str(TimeRogDataFull["ARz"].iloc
                            [height - 11:].mean()))
print("Std A Rzz = " + str(TimeRogDataFull["ARz"].iloc
                           [height - 11:].std()))
print("Mean B Rzz = " + str(TimeRogDataFull["BRz"].iloc
                            [height - 11:].mean()))
print("Std B Rzz = " + str(TimeRogDataFull["BRz"].iloc
                           [height - 11:].std()))
plt.figure()
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Rzz, "k", label="Molecule")
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.ARz, "r", label="A Block")
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.BRz, "b", label="B Block")
plt.legend()
plt.xlabel("Time")
plt.ylabel("Radius of Gyration")
ax = plt.gca()
ax.ticklabel_format(axis='x', style='scientific', scilimits=(0, 0))
# plt.xlim([0, 2E4]);
