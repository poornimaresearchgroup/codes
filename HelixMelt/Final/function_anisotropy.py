# -*- coding: utf-8 -*-
"""
Created on Thu Sep  1 09:51:08 2022

@author: Acer
"""

import math
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Input FILEPATH and trj file to be examined
SIMULATION = "K1"
FILEPATH = "C:/Users/nxb1090/Desktop/GradResearch/Buchanan/Projects/" + \
           "HelixMelt/Data/LowerPressure/T80P240//" + SIMULATION
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "RogData.txt"
DELTA_T = 0.0005
RogList = []
ARogList = []
BRogList = []
TimeRogDataFull = pd.DataFrame(columns=["Time", "Rog", "ARog", "BRog"])
print(SIMULATION + " start!")

dir_list = os.listdir(FILEPATH + '/LAMMPS/')
i = 0
for root, dirs, files in os.walk(FILEPATH + '/LAMMPS/'):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not "Old" in d]
    i = i + 1
    for file in files:
        if file == "rog.dump":
            filepath = root + '/' + file
            RogList.append(filepath)
        if file == "Arog.dump":
            filepath = root + '/' + file
            ARogList.append(filepath)
        if file == "Brog.dump":
            filepath = root + '/' + file
            BRogList.append(filepath)

for runcounter in range(0, len(RogList)):
    print('Run Counter: ' + str(runcounter))
    ROGFILE = RogList[runcounter]
    AROGFILE = ARogList[runcounter]
    BROGFILE = BRogList[runcounter]

    headerinfo = pd.read_csv(ROGFILE, nrows=0, comment="#", sep=" ")
    NUMCHUNKS = int(headerinfo.columns[1])

    # Get header info for each time step
    num_lines = sum(1 for line in open(ROGFILE))
    steplist = [*range(3, num_lines, NUMCHUNKS + 1)]
    to_exclude = [i for i in range(num_lines) if i not in steplist]
    stepdata = pd.read_csv(ROGFILE, skiprows=to_exclude,
                           names=["Timestep", "Number-of-Chunks"],
                           sep=" ")

    TimeRogData = pd.DataFrame(columns=["Time", "Molsphere", "Molcyl", "Moliso",
                                        "Asphere", "Acyl", "Aiso",
                                        "Bsphere", "Bcyl", "Biso"])

    # %% Total Mol

    timecounter = 0
    # Saves mean ROG of molecule for each timestep
    with pd.read_csv(ROGFILE, chunksize=NUMCHUNKS + 1, header=0, sep=" ",
                     comment="#", names=["Mol", "Rxx", "Ryy", "Rzz", "Rxy", "Rxz", "Ryz"]) as reader:
        for chunk in reader:
            chunk.where(chunk.Mol <= NUMCHUNKS, float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            chunk.set_index("Mol", drop=False, inplace=True)
            # Raises error if first and last chunks in range are not the IDs expected
            if chunk.Mol.iloc[0] != 1 or chunk.Mol.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" +
                                 str(chunk.Mol.iloc[0]) + "    " +
                                 str(chunk.Mol.iloc[-1]))
            chunk["b"] = np.nan
            chunk["c"] = np.nan
            chunk["aniso"] = np.nan
            for mol in range(NUMCHUNKS):
                Smatrix = np.array([[chunk.Rxx.iloc[mol], chunk.Rxy.iloc[mol],
                                     chunk.Rxz.iloc[mol]],
                                    [chunk.Rxy.iloc[mol], chunk.Ryy.iloc[mol],
                                     chunk.Ryz.iloc[mol]],
                                    [chunk.Rxz.iloc[mol], chunk.Ryz.iloc[mol],
                                     chunk.Rzz.iloc[mol]]])
                # [eigen,w] = np.linalg.eig(Smatrix)
                eigen = np.linalg.eigvals(Smatrix)
                eigen.sort()
                [L1, L2, L3] = eigen
                b = L3 - 0.5 * (L1 + L2)
                chunk.loc[mol + 1, "b"] = b
                c = L2 - L1
                chunk.loc[mol + 1, "c"] = c
                Rg = math.sqrt(L1 + L2 + L3)
                chunk.loc[mol + 1, "Rog"] = Rg
                chunk.loc[mol + 1, "aniso"] = ((b * b) + (0.75 * c * c)) / (Rg ** 4)

            # Finds data from header at same time
            time = stepdata.iloc[timecounter]["Timestep"]
            TimeRogData.loc[timecounter] = [time, np.mean(chunk.b), np.mean(chunk.c), np.mean(chunk.aniso), np.nan,
                                            np.nan, np.nan, np.nan, np.nan, np.nan]
            timecounter += 1
    print("Molecule Done")
    # %% A Rog            
    timecounter = 0
    # Saves mean ROG of molecule for each timestep
    with pd.read_csv(AROGFILE, chunksize=NUMCHUNKS + 1, header=0, sep=" ",
                     comment="#", names=["Mol", "Rxx", "Ryy", "Rzz", "Rxy", "Rxz", "Ryz"]) as reader:
        for chunk in reader:
            chunk.where(chunk.Mol <= NUMCHUNKS, float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            chunk.set_index("Mol", drop=False, inplace=True)
            # Raises error if first and last chunks in range are not the IDs expected
            if chunk.Mol.iloc[0] != 1 or chunk.Mol.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" +
                                 str(chunk.Mol.iloc[0]) + "    " +
                                 str(chunk.Mol.iloc[-1]))
            chunk["Rog"] = np.nan
            for mol in range(NUMCHUNKS):
                Smatrix = np.array([[chunk.Rxx.iloc[mol], chunk.Rxy.iloc[mol], chunk.Rxz.iloc[mol]],
                                    [chunk.Rxy.iloc[mol], chunk.Ryy.iloc[mol], chunk.Ryz.iloc[mol]]
                                       , [chunk.Rxz.iloc[mol], chunk.Ryz.iloc[mol], chunk.Rzz.iloc[mol]]])
                eigen = np.linalg.eigvals(Smatrix)
                eigen.sort()
                [L1, L2, L3] = eigen
                b = L3 - 0.5 * (L1 + L2)
                chunk.loc[mol + 1, "b"] = b
                c = L2 - L1
                chunk.loc[mol + 1, "c"] = c
                Rg = math.sqrt(L1 + L2 + L3)
                chunk.loc[mol + 1, "Rog"] = Rg
                chunk.loc[mol + 1, "aniso"] = ((b * b) + (0.75 * c * c)) / (Rg ** 4)
            # Finds data from header at same time
            time = stepdata.iloc[timecounter]["Timestep"]
            TimeRogData.iloc[timecounter]['Asphere'] = np.mean(chunk.b)
            TimeRogData.iloc[timecounter]['Acyl'] = np.mean(chunk.c)
            TimeRogData.iloc[timecounter]['Aiso'] = np.mean(chunk.aniso)
            timecounter += 1
    print("Type A Done")

    # %% B Rog            
    timecounter = 0
    # Saves mean ROG of molecule for each timestep
    with pd.read_csv(BROGFILE, chunksize=NUMCHUNKS + 1, header=0, sep=" ",
                     comment="#", names=["Mol", "Rxx", "Ryy", "Rzz", "Rxy", "Rxz", "Ryz"]) as reader:
        for chunk in reader:
            chunk.where(chunk.Mol <= NUMCHUNKS, float("NaN"), inplace=True)
            chunk.dropna(inplace=True)
            chunk.set_index("Mol", drop=False, inplace=True)
            # Raises error if first and last chunks in range are not the IDs expected
            if chunk.Mol.iloc[0] != 1 or chunk.Mol.iloc[-1] != NUMCHUNKS:
                raise ValueError("Chunk is Not Chunking Correct \n" +
                                 str(chunk.Mol.iloc[0]) + "    " +
                                 str(chunk.Mol.iloc[-1]))
            chunk["Rog"] = np.nan
            for mol in range(NUMCHUNKS):
                Smatrix = np.array([[chunk.Rxx.iloc[mol], chunk.Rxy.iloc[mol], chunk.Rxz.iloc[mol]],
                                    [chunk.Rxy.iloc[mol], chunk.Ryy.iloc[mol], chunk.Ryz.iloc[mol]]
                                       , [chunk.Rxz.iloc[mol], chunk.Ryz.iloc[mol], chunk.Rzz.iloc[mol]]])
                eigen = np.linalg.eigvals(Smatrix)
                eigen.sort()
                [L1, L2, L3] = eigen
                b = L3 - 0.5 * (L1 + L2)
                chunk.loc[mol + 1, "b"] = b
                c = L2 - L1
                chunk.loc[mol + 1, "c"] = c
                Rg = math.sqrt(L1 + L2 + L3)
                chunk.loc[mol + 1, "Rog"] = Rg
                chunk.loc[mol + 1, "aniso"] = ((b * b) + (0.75 * c * c)) / (Rg ** 4)
            # Finds data from header at same time
            time = stepdata.iloc[timecounter]["Timestep"]
            TimeRogData.iloc[timecounter]['Bsphere'] = np.mean(chunk.b)
            TimeRogData.iloc[timecounter]['Bcyl'] = np.mean(chunk.c)
            TimeRogData.iloc[timecounter]['Biso'] = np.mean(chunk.aniso)
            timecounter += 1
    print("Type B Done")
    if "Run1" in ROGFILE:
        TimeRogDataFull = TimeRogData
    else:
        TimeRogDataFull = pd.concat([TimeRogDataFull, TimeRogData],
                                    ignore_index=True, axis=0)
# %% Adjust Time
TimeRogDataFull.Time = TimeRogDataFull.Time * DELTA_T
TimeRogData.Time = TimeRogData.Time * DELTA_T
TimeRogDataFull.sort_values(by=['Time'], inplace=True)
TimeRogDataFull.sort_values(by=['Time'], inplace=True)
# %% Outputs
TimeRogDataFull.to_csv(OUTPUTFILE)
SPAN = int(timecounter)
height = TimeRogData.shape[0]
TimeRogDataSpan = TimeRogDataFull[height - 11:]
print(SPAN)
print("Time: " + str(TimeRogDataSpan.Time.max() - TimeRogDataSpan.Time.min()))
print("MEAN")
print(TimeRogDataSpan.mean())
print("STD")
print(TimeRogDataSpan.std())

plt.figure()
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Molsphere, "k", label="Molecule")
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Asphere, "r", label="A Block")
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Bsphere, "b", label="B Block")
plt.legend()
plt.xlabel("Time")
plt.ylabel("Asphere")

plt.figure()
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Molcyl, "k", label="Molecule")
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Acyl, "r", label="A Block")
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Bcyl, "b", label="B Block")
plt.legend()
plt.xlabel("Time")
plt.ylabel("Acyl")

plt.figure()
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Moliso, "k", label="Molecule")
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Aiso, "r", label="A Block")
plt.plot(TimeRogDataFull.Time, TimeRogDataFull.Biso, "b", label="B Block")
plt.legend()
plt.xlabel("Time")
plt.ylabel("Aniso")
ax = plt.gca()
ax.ticklabel_format(axis='x', style='scientific', scilimits=(0, 0))