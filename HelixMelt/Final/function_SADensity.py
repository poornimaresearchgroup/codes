# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 10:52:22 2023

@author: nxb1090
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.signal as sc

import ToolBox as tb

## NOTE: Need to change time adjustments from file to file

# Input FILEPATH and trj file to be examined
SIMULATION = "K5"
FILEPATH = "U:/GradResearch/HelixMelt/Data/T160P200/SmallBox/" + SIMULATION
TJFILENAME = FILEPATH + "/LAMMPS/Run2/" + SIMULATION + ".lammpstrj"

A_files = []
B_files = []
logDataFull = pd.DataFrame(columns=["Time", "Lx", "Ly", "Ly"])
# logDataFull.set_index("Time", inplace=True)

dir_list = os.listdir(FILEPATH + '/LAMMPS/')
i = 0
print("START")
for root, dirs, files in os.walk(FILEPATH + '/LAMMPS/'):
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not "Old" in d]
    for file in files:
        if file == "zprofile-A.txt":
            filepath = root + '/' + file
            A_files.append(filepath)
        if file == "zprofile-B.txt":
            filepath = root + '/' + file
            B_files.append(filepath)
        if file == "log.lammps":
            i = i + 1
            LOGFILE = root + '/' + file
            logFrame = tb.listmaker(open(LOGFILE, "r"))
            logFrame = pd.DataFrame(logFrame)
            logFrame.drop([14, 15, 16, 17], inplace=True,
                          axis=1)  # Cuts extra columns that arise due to text delimination
            logFrame = logFrame.convert_dtypes(infer_objects=True, convert_floating=True, convert_string=True)
            # logFrame.columns = ["Time", "Temp", "Press", "K*", "Lx", "Ly", "Lz"]
            logFrame.columns = ["Time", "K*", "Temp", "Press", "Pxx", "Pyy", "Pzz", "PE",
                                "LJPE", "Vol", "Dens", "Lx", "Ly", "Lz"]

            # logData is only the data rows, where every value is a number. No text
            logData = pd.DataFrame(
                columns=["Time", "Lx", "Ly", "Lz"])  # columns = ["Time", "K*","Temp","Press","Pxx","Pyy",
            #                                  "Pzz", "PE","LJPE", "Vol","Dens","Lx","Ly",
            #                                 "Lz"])
            for n, row in logFrame.iterrows():
                row = row.astype(float, errors="ignore")
                if row.dtype == float:  # if all values are numbers, append to logData
                    temp = pd.DataFrame({"Time": row.Time, "Lx": row.Lx, "Ly": row.Ly,
                                         "Lz": row.Lz}, index=[0])
                    logData = pd.concat([logData, temp])
            logData.Time.astype(int)

            # Timestep Adjustment
            if i == 1:
                logDataFull = logData
                # logData.Time = logData.Time/2
                # run1time = logDataFull.Time.iloc[-1] - 500000
            else:
                # if i == 2:
                #     run2time = logData.Time.iloc[0]
                #     logData.Time = logData.Time - run2time + run1time
                # if i == 3:
                #     logData.Time = logData.Time - run2time + run1time
                logData.set_index("Time", inplace=True)
                # logData.drop_duplicates(inplace = True)
                logData = logData[~logData.index.duplicated(keep='first')]
                logData = logData.reset_index()
                print(logData.shape)
                logDataFull = pd.concat([logDataFull, logData], axis=0, join='outer', ignore_index=True)
logDataFull.set_index("Time", inplace=True)
logDataFull = logDataFull[~logDataFull.index.duplicated(keep='first')]

print(SIMULATION)

# Number of atoms from first timestep of trj file
headerinfo = pd.read_csv(TJFILENAME, nrows=7)
atoms = int(headerinfo.iloc[2, 0])
mols = atoms / 60;
logDataFull["Interface"] = (logDataFull.Lx * logDataFull.Ly) / (mols / 4)

plt.figure()
plt.plot(logDataFull.index.values, logDataFull.Interface, "g", label="Interface")
plt.legend()
plt.xlabel("Time");
plt.ylabel(r'L_x * L_y/(n_{mol}/4')

plt.figure()
plt.plot(logDataFull.index.values, logDataFull.Lx * logDataFull.Ly * logDataFull.Lz, "g", label="Interface")
plt.legend()
plt.xlabel("Time");
plt.ylabel('Volume')
