# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 13:21:45 2024

@author: nxb1090
"""

import math
import re
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import ToolBox as tb


class HelicityCalculations:
    """Class to represent a simulation and process helicity"""

    def __init__(self, name, filepath, delta_t=0.0005, tolerance=20):
        """
        Initialize the HelicityCalculations object.

        Parameters
        ----------
        name : str
            Name of the simulation (folder within dataset)
        filepath : str
            Path to the directory containing simulation data.
        delta_t : float, optional
            Size of timestep in LAMMPS. The default is 0.0005.
        tolerance : int, optional
            Tolerance for helicity calculation in degrees. The default is 20.

        Returns
        -------
        None.

        """
        self.name = name
        self.filepath = os.path.normpath(os.path.join(filepath, name))
        self.delta_t = delta_t
        self.tolerance = tolerance

        # Output file paths
        self.output_dir = os.path.join(self.filepath, "PostProcessing")
        self.outputfile = os.path.join(self.output_dir, "Helicity.txt")
        self.outputfile_by_z = os.path.join(self.output_dir, "HelicityByZ.txt")
        self.BProfilePath = os.path.join(
            self.filepath, "PostProcessing", "chunkB.txt")
        self.BProfile = pd.read_csv(self.BProfilePath)

        # Extract theta and phi values from filepath
        self.theta, self.phi = self._extract_theta_and_phi(filepath)

        # Initialize dataframes
        self.angle_files, self.dihedral_files, self.trj_files = [], [], []
        self.TimeHelicityData = pd.DataFrame(columns=["Time", "Helicity"],
                                             dtype='float')
        self.ResIDHelicityData = pd.DataFrame()
        self.ZHelicityData = pd.DataFrame()
        self.ZCount = pd.DataFrame()

    def _extract_theta_and_phi(self, filepath):
        """
        Get values for theta and phi from file path

        Parameters
        ----------
        filepath : str
            Path to the directory containing simulation data.

        Returns
        -------
        theta : int
            Value of theta in degrees, follows 'T' in filepath
        phi : int
            Value of phi in degrees, follows 'T' in filepath

        """
        # Assumes filepath = C:/path/T##P##/simulation
        theta = int(re.findall(r'T(\d+)', filepath)[0])
        phi = int(re.findall(r'P(\d+)', filepath)[0])
        return theta, phi

    def find_files(self):
        """ Create list of angle, dihedral, and trj file paths."""
        print(f"Processing files for simulation: {self.name}")
        for root, dirs, files in os.walk(os.path.join(self.filepath,
                                                      'LAMMPS')):
            files = [f for f in files if not f[0] == '.']
            dirs[:] = [d for d in dirs if not "Old" in d]
            for file in files:
                filepath = os.path.join(root, file)
                if file == "angle.dump":
                    self.angle_files.append(filepath)
                elif file == "dihedral.dump":
                    self.dihedral_files.append(filepath)
                elif file == "lam.lammpstrj":
                    self.trj_files.append(filepath)

    def create_timestep(self, runcounter):
        """ Create TimeStep object. """
        return TimeStep(runcounter, self)

    def plot(self):
        """ Call different plot functions to create graphs."""
        span = int(self.TimeHelicityData.Time.max()) * 0.1
        self.plot_time_v_helicity(span)
        self.plot_resID_v_helicity(span)
        self.plot_z_v_helicity(span)

    def run(self):
        """
        Get helicity data for each time step. Saves time and helicity data
            to outputfile.

        Returns
        -------
        None.

        """
        self.find_files()
        for runcounter in range(len(self.trj_files)):
            print(f'Run Counter: {runcounter}')
            current_run = self.create_timestep(runcounter)
            current_run.run(self)
        self.output_summary()
        self.TimeHelicityData.Time = self.TimeHelicityData.Time * self.delta_t
        self.TimeHelicityData.sort_values(by=['Time'], inplace=True)
        self.TimeHelicityData.to_csv(self.outputfile)

    def output_summary(self):
        """ Print mean and standard deviation for helicity over time """
        # Find out how much time is the last 10% of data
        span = int(self.TimeHelicityData.Time.max()) * 0.1
        print(f' Span: {span}')
        print("Time: " + str(self.TimeHelicityData.Time.max() -
                             self.TimeHelicityData.Time.min()))
        print("Mean Helicity = " + str(self.TimeHelicityData["Helicity"].iloc
                                       [-span:].mean()))
        print("Std Helicity = " + str(round(self.TimeHelicityData["Helicity"].iloc
                                            [-span:].std(), 5)))
        print("Mean Helicity = " + str(self.TimeHelicityData["Helicity"].iloc
                                       [len(self.TimeHelicityData) - 11:].mean()))
        print("Std Helicity = " + str(round(self.TimeHelicityData["Helicity"].iloc
                                            [len(self.TimeHelicityData) - 11:].std(), 5)))

        if abs(self.TimeHelicityData["Helicity"].iloc[-span:].mean()) < 0.01:
            print("Helicity = 0. Check set points")

    def plot_time_v_helicity(self, span):
        """
        Plot helicity over time with average helicity annotated on graph.

        Parameters
        ----------
        span : float
            the number of timesteps to average.

        Returns
        -------
        None.

        """
        plt.figure()
        plt.plot(self.TimeHelicityData["Time"],
                 self.TimeHelicityData["Helicity"], lw=2)
        plt.ylim([0, 1])
        plt.xlabel("Time")
        plt.ylabel("Helicity")
        plt.annotate("Avg Hel = " +
                     str(round(
                         self.TimeHelicityData["Helicity"][-span:].mean(), 3)),
                     (self.TimeHelicityData["Time"].iloc[-1],
                      self.TimeHelicityData["Helicity"].iloc[-1]),
                     (.75, 0.9), "axes fraction")
        plt.show()

    def plot_resID_v_helicity(self, span):
        """
        Plot helicity by residue in the block.

        Parameters
        ----------
        span : float
            the number of timesteps to average.

        Returns
        -------
        None.

        """

        ResIDSpan = self.ResIDHelicityData.iloc[:,
                                                math.floor(len(self.ResIDHelicityData.columns) * 0.9):len(self.ResIDHelicityData.columns)]
        plt.figure()
        plt.plot(ResIDSpan.index.values, ResIDSpan.mean(axis=1), lw=2)
        plt.ylim([0, 1])
        plt.xlabel("ResID")
        plt.ylabel("Helicity")
        plt.annotate("Avg Hel = " +
                     str(round(
                         self.TimeHelicityData["Helicity"][-span:].mean(), 3)),
                     (self.TimeHelicityData["Time"].iloc[-1],
                      self.TimeHelicityData["Helicity"].iloc[-1]),
                     (.75, 0.9), "axes fraction")
        plt.show()

    def plot_z_v_helicity(self, span):
        """
        Plot helicity over z coordinate in the morphology.
        Print to outputfile_by_z.

        Parameters
        ----------
        span : float
            the number of timesteps to average.

        Returns
        -------
        None.

        """
        ZSpan = self.ZHelicityData.iloc[:, math.floor(
            len(self.ZHelicityData.columns) * 0.9):len(self.ZHelicityData.columns)]
        ZSpan.sort_index(inplace=True)
        CountSpan = self.ZCount.iloc[:, math.floor(
            len(self.ZCount.columns) * 0.9):len(self.ZCount.columns)]
        CountSpan.sort_index(inplace=True)
        CountSpan.fillna(0, inplace=True)
        counts = ZSpan.mul(CountSpan)
        counts.fillna(0, inplace=True)
        # weigh helicity value by number of residues in the bin
        weight_average = counts.sum(axis=1) / CountSpan.sum(axis=1)

        diff = ZSpan.sub(weight_average, axis='index')
        diff = diff ** 2
        weight_std = diff.sum(axis=1) / CountSpan.sum(axis=1)
        weight_std = np.sqrt(weight_std)

        fig, ax1 = plt.subplots()
        ax1.scatter(weight_average.index, weight_average,
                    10, 'k', label='helicity')
        ax1.set_xlabel('z')
        ax1.set_ylabel('Helicity', color='k')
        ax1.tick_params(axis='y', labelcolor='k')
        plt.ylim([0, 1])
        ax2 = ax1.twinx()
        ax2.set_ylabel(r'$\phi$', color='b')
        ax2.bar(self.BProfile.Coord, self.BProfile.vol_frac, color="b", label=r'$\phi_B$',
                align='center',
                width=1,
                alpha=0.1)
        ax2.tick_params(axis='y', labelcolor='b')
        plt.ylim([0, 1])
        fig.tight_layout()
        plt.title(self.name + ' B')
        fig.legend(bbox_to_anchor=(1.1, 0.5))

        fig, ax1 = plt.subplots()
        ax1.errorbar(weight_average.index, weight_average, yerr=weight_std,
                     color="k", marker="o", linestyle="None", capsize=2,
                     label='helicity')
        ax1.set_xlabel('z')
        ax1.set_ylabel('Helicity', color='k')
        ax1.tick_params(axis='y', labelcolor='k')
        plt.ylim([0, 1])
        ax2 = ax1.twinx()
        ax2.set_ylabel(r'$\phi$', color='b')
        ax2.bar(self.BProfile.Coord, self.BProfile.vol_frac, color="b", label=r'$\phi_B$',
                align='center',
                width=1,
                alpha=0.1)
        ax2.tick_params(axis='y', labelcolor='b')
        plt.ylim([0, 1])
        fig.tight_layout()
        plt.title(self.name + ' B')
        fig.legend(bbox_to_anchor=(1.1, 0.5))

        graph = pd.concat([weight_average, weight_std], axis=1)
        graph.columns = ["average", "std"]
        graph["Count"] = CountSpan.sum(axis=1)
        graph.to_csv(self.outputfile_by_z)


class TimeStep(HelicityCalculations):
    """Subclass to look at each time step."""

    def __init__(self, runcounter, super_self):
        """Initialize the TimeStep object """
        self.runcounter = runcounter
        self.angle_file = super_self.angle_files[runcounter]
        self.dihedral_file = super_self.dihedral_files[runcounter]
        self.trj_file = super_self.trj_files[runcounter]
        self.angleNum, self.dihedralNum, self.atoms = self._extract_header_info()
        
    def set_up_chunks(self):
        """ Create chunks containing one time step for angle, dihedrals, and trj."""
        angle_chunker = pd.read_csv(self.angle_file,
                                    chunksize=self.angleNum + 9)
        dihedral_chunker = pd.read_csv(self.dihedral_file,
                                       chunksize=self.dihedralNum + 9)
        trj_chunker = pd.read_csv(self.trj_file,
                                  chunksize=(self.atoms), sep=" ",
                                  skiprows=lambda x: x %
                                  (self.atoms + 9) < 9,
                                  skipinitialspace=True,
                                  names=["id", 'mol', 'type', 'x', 'y', 'z',
                                         'ix', 'iy', 'iz'])

        return angle_chunker, dihedral_chunker, trj_chunker

    def _extract_header_info(self):
        """Extract number of angle, dihedral, and atoms from trj file."""
        headerinfo_angle = pd.read_csv(self.angle_file, nrows=9)
        headerinfo_dihedral = pd.read_csv(self.dihedral_file, nrows=9)
        headerinfo_trj = pd.read_csv(self.trj_file, nrows=7)
        return int(headerinfo_angle.iloc[2, 0]), int(headerinfo_dihedral.iloc[2, 0]), int(headerinfo_trj.iloc[2, 0])


    def _process_chunk(self, angle_chunk, dihedral_chunk, trj_chunk, super_self):
        """
        Check chunks are at the same time step before running calculations.

        Parameters
        ----------
        angle_chunk : pd.DataFrame
            one timestep extracted from angle.dump file
        dihedral_chunk : pd.DataFrame
            One timestep extracted from dihedral.dump file
        trj_chunk : pd.DataFrame
            One timestep extracted from the trj file
        super_self : HelicityCalculations object
            Object containing simulation information

        Raises
        ------
        ValueError
            Timesteps of the chunks do not match.

        Returns
        -------
        None.

        """
        # Function to process each chunk
        angle_time, dihedral_time = int(
            angle_chunk.iloc[0, :]), int(dihedral_chunk.iloc[0, :])
        if angle_time != dihedral_time:
            raise ValueError(
                f"Times Don't Match \n{angle_time}    {dihedral_time}")

        self._process_time_step(angle_time, angle_chunk,
                                dihedral_chunk, trj_chunk, super_self)

    def _process_time_step(self, angle_time, angle_chunk, dihedral_chunk, trj_chunk, super_self):
        """
        Wrap atom coordinates before calculations.

        Parameters
        ----------
        angle_time : int
            Timestep being analyzed
        angle_chunk : pd.DataFrame
            one timestep extracted from angle.dump file
        dihedral_chunk : pd.DataFrame
            One timestep extracted from dihedral.dump file
        trj_chunk : pd.DataFrame
            One timestep extracted from the trj file
        super_self : HelicityCalculations object
            Object containing simulation information

        Returns
        -------
        None.

        """
        [xlo, xhi], [ylo, yhi], [zlo, zhi] = [map(float, angle_chunk.iloc[4, 0].split()),
                                              map(float,
                                                  angle_chunk.iloc[5, 0].split()),
                                              map(float, angle_chunk.iloc[6, 0].split())]
        Lx, Ly, Lz = xhi - xlo, yhi - ylo, zhi - zlo
        trj_chunk["x"] = (trj_chunk["x"] + trj_chunk["ix"]) * Lx
        trj_chunk["ix"] = 0
        trj_chunk["y"] = (trj_chunk["y"] + trj_chunk["iy"]) * Ly
        trj_chunk["iy"] = 0
        trj_chunk["z"] = (trj_chunk["z"] + trj_chunk["iz"]) * Lz
        trj_chunk["iz"] = 0
        box = [Lx, Ly, Lz]

        current_angle, current_dihedral = self._parse_angle_dihedral(
            angle_chunk, dihedral_chunk)
        current_helicity = self._calculate_helicity(
            current_angle, current_dihedral, super_self)
        self._update_results(current_helicity, box,
                             angle_time, trj_chunk, super_self)

    def _parse_angle_dihedral(self, angle_chunk, dihedral_chunk):
        """
        

        Parameters
        ----------
        angle_chunk : pd.DataFrame
            one timestep extracted from angle.dump file
        dihedral_chunk : pd.DataFrame
            One timestep extracted from dihedral.dump file

        Returns
        -------
        current_angle : pd.DataFrame
            One timestep with columns atom1, id, type,
            atom2, atom3, theta, and energy
        current_dihedral : pd.DataFrame
            One timestep with columns atom1, id, type,
            atom2, atom3, atom4, and phi
        """
    
        current_angle = pd.DataFrame([item[0].split() for item in
                                      angle_chunk.iloc[8:self.angleNum + 8].values.tolist()],
                                     columns=['id', "type", 'atom1', 'atom2',
                                              'atom3', 'Theta', 'Eng'],
                                     dtype='float')
        current_angle["id"] = current_angle["id"].astype("int64")
        current_angle.set_index("atom1", inplace=True)
        current_angle.index = current_angle.index.astype("int64")
        current_angle = current_angle.sort_values("atom1")

        current_dihedral = pd.DataFrame([item[0].split() for item in
                                         dihedral_chunk.iloc[8: self.dihedralNum + 8].values.tolist()],
                                        columns=['id', "type", 'atom1',
                                                 'atom2', 'atom3', 'atom4', 'Phi'],
                                        dtype='float')

        current_dihedral["id"] = current_dihedral["id"].astype("int64")
        current_dihedral.set_index("atom1", inplace=True)
        current_dihedral.index = current_dihedral.index.astype("int64")
        current_dihedral = current_dihedral.sort_values("atom1")
        return current_angle, current_dihedral

    def _calculate_helicity(self, current_angle, current_dihedral, super_self):
        """
        Find residues where theta and phi are both within tolerance.

        Parameters
        ----------
        current_angle : pd.DataFrame
            One timestep with columns atom1, id, type,
            atom2, atom3, theta, and energy
        current_dihedral : pd.DataFrame
            One timestep with columns atom1, id, type,
            atom2, atom3, atom4, and phi
        super_self : HelicityCalculations object
            Object containing simulation information

        Returns
        -------
        current_helicity : pd.DataFrane
            Contains info for each first atom from one timestep.
            Columns = atom1, id, type, atom2, atom3, Theta, Eng, ThetaDiff,
                atom4, Phi, Phi Diff, Theta Space, Phi Spec, Residue

        """
        # Find and wraps distance of measure from set point
        current_angle["Theta Diff"] = current_angle["Theta"] - super_self.theta
        current_angle.loc[(current_angle["Theta Diff"]) >
                          180, "Theta Diff"] -= 360
        current_angle.loc[(current_angle["Theta Diff"]) < -180,
                          "Theta Diff"] += 360

        current_dihedral["Phi Diff"] = current_dihedral["Phi"] - super_self.phi
        current_dihedral.loc[(current_dihedral["Phi Diff"]) >
                             180, "Phi Diff"] -= 360
        current_dihedral.loc[(current_dihedral["Phi Diff"]) < -180,
                             "Phi Diff"] += 360

        # Merge angle and dihedral information by matching first atom of residue
        current_helicity = pd.merge(current_angle,
                                    current_dihedral, "right",
                                    left_index=True, right_index=True,
                                    suffixes=[None, '_remove'])
        current_helicity.drop([i for i in current_helicity.columns if '_remove' in i],
                              axis=1, inplace=True)
        # Spec = 1 if within tolerance, 0 if outside
        current_helicity["Theta Spec"] = np.where(
            abs(current_helicity["Theta Diff"]) <= super_self.tolerance, 1, 0)
        current_helicity["Phi Spec"] = np.where(
            abs(current_helicity["Phi Diff"]) <= super_self.tolerance, 1, 0)
        # Residue = 1 if residue is helical, theta and phi both within tolerance
        current_helicity["Residue"] = np.where((current_helicity["Theta Spec"] == 1)
                                               & (current_helicity["Phi Spec"] == 1),
                                               1, 0)

        return current_helicity

    def _update_results(self, current_helicity, box, angle_time, trj_chunk, super_self):
        """
        Add results from current timestep to dataframes for simulation.

        Parameters
        ----------
        current_helicity : pd.DataFrane
            Contains info for each first atom from one timestep.
            Columns = atom1, id, type, atom2, atom3, Theta, Eng, ThetaDiff,
                atom4, Phi, Phi Diff, Theta Space, Phi Spec, Residue
        box : list
            length of simulation box sides. [Lx, Ly, Lz]
        angle_time : int
            Timestep being analyzed
        trj_chunk : pd.DataFrame
            One timestep extracted from the trj file
        super_self : HelicityCalculations object
            Object containing simulation information

        Returns
        -------
        None.

        """

        # Time_v_Helicity Data
        temp_helicity_data = pd.DataFrame({"Time": [angle_time],
                                           "Helicity":
                                               sum(current_helicity["Residue"])
                                               / len(current_helicity)})
        super_self.TimeHelicityData = pd.concat(
            [super_self.TimeHelicityData, temp_helicity_data], ignore_index=True)

        # ResID_v_Helicity Data
        current_helicity["ResID"] = current_helicity.index.values % 60
        if super_self.ResIDHelicityData.empty:
            super_self.ResIDHelicityData.index = pd.unique(
                current_helicity["ResID"])

        resID = current_helicity.groupby("ResID").mean()
        tempRes = resID.Residue.transpose()
        tempRes.rename(str(angle_time), inplace=True)
        super_self.ResIDHelicityData[(angle_time)] = tempRes

        # Z_v_Helicity Data
        [Lz, Ly, Lz] = box
        trj_chunk.set_index("id", inplace=True)
        current_helicity["Z"] = trj_chunk.loc[current_helicity.index, "z"]
        current_helicity["Z"] = current_helicity["Z"].apply(tb.wrap, Lbox=Lz)
        current_helicity["binID"] = [super_self.BProfile.iloc[(super_self.BProfile['Coord'] - val)
                                                              .abs().argsort()[0]]['Coord']
                                     for val in current_helicity['Z']]
        if super_self.ZHelicityData.empty:
            super_self.ZHelicityData.index = pd.unique(
                current_helicity["binID"])
        ZID = current_helicity.groupby("binID").mean()
        tempZ = ZID.Residue.transpose()
        tempZ.rename(str(angle_time), inplace=True)
        super_self.ZHelicityData[angle_time] = tempZ

        # Z Count Data
        zCount = current_helicity.groupby("binID").count()
        if super_self.ZCount.empty:
            super_self.ZCount.index = pd.unique(current_helicity["binID"])

        tempCount = zCount["Residue"]
        tempCount.rename(str(angle_time), inplace=True)
        super_self.ZCount[angle_time] = tempCount
        
    def run(self, super_self):
        """
        For each time step, run calculations.

        Parameters
        ----------
        super_self : HelicityCalculations object
            Object containing simulation information

        Returns
        -------
        None.

        """
        angle_chunker, dihedral_chunker, trj_chunker = self.set_up_chunks()
        for angleChunk, dihedralChunk, trjChunk in zip(angle_chunker,
                                                       dihedral_chunker,
                                                       trj_chunker):
            self._process_chunk(angleChunk, dihedralChunk,
                                trjChunk, super_self)

simulation = HelicityCalculations(
    "K1", "C:/Users/nxb1090/Desktop/GradResearch/Buchanan/Projects/" +
    "HelixMelt/Data/LowerPressure/T80P240/")
simulation.run()
simulation.plot()
