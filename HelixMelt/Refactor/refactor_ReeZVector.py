# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 13:25:30 2024

@author: nxb1090
"""

import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import ToolBox as tb


class ReeZVectors:
    """Class to find z component of end-to-end vectors."""

    def __init__(self, simulation_name, filepath):
        """
        Initialize ReeZVectors class.

        Parameters
        ----------
        simulation_name (str): Name of the simulation.
        filepath (str): Path to the simulation data folder.
        """
        self.simulation_name = simulation_name
        self.filepath = os.path.normpath(os.path.join(
            filepath, simulation_name))
        self.outputfile = os.path.normpath(os.path.join(
            filepath, simulation_name, "PostProcessing", "ReeZVector.txt"))
        self.trj_list = []
        self.BoxData = pd.DataFrame(columns=["Time"], dtype='float')
        self.VectorData = pd.DataFrame(columns=["Time"], dtype='float')

    def load_profiles(self):
        """
        Read density profiles from LAMMP files.

        Returns
        -------
        a_profile (pd.DataFrame): dataframe of chunkA.txt
        b_profile (pd.DataFrame): dataframe of chunkB.txt

        """
        a_profile = pd.read_csv(os.path.join(
            self.filepath, "PostProcessing/chunkA.txt"))
        b_profile = pd.read_csv(os.path.join(
            self.filepath, "PostProcessing/chunkB.txt"))
        return a_profile, b_profile

    def load_trj_files(self):
        """Make a list of filepaths for trj files within simulation folder."""
        print(f"Processing files for simulation: {self.simulation_name}")
        for root, dirs, files in os.walk(os.path.join(
                self.filepath, 'LAMMPS')):
            files = [f for f in files if not f[0] == '.']
            dirs[:] = [d for d in dirs if "Old" not in d]
            for file in files:
                if file == "lam.lammpstrj":
                    filepath = os.path.join(root, file)
                    self.trj_list.append(filepath)

    def get_box_dimensions(self, trj_file):
        """
        Make DataFrame with timestep and box lengths extracted from trj files.

        Parameters
        ----------
        trj_file (str): path of trj file

        Returns
        -------
        list
            time_box_data (pd.DataFrame): time and box data for current file
            atoms (int): number of atoms present. Assumed constant within a
                file

        """
        time_box_data = pd.DataFrame(columns=["Time", "Lx", "Ly", "Lz"])
        headerinfo = pd.read_csv(trj_file, nrows=7)
        atoms = int(headerinfo.iloc[2, 0])
        time_counter = 0
        with pd.read_csv(trj_file, chunksize=(9),
                         skiprows=(lambda x: x % (atoms + 9) >= 9)) as reader:
            for chunk in reader:
                time = int(chunk.iloc[0, 0])  # time associated with chunk
                # Get box dimensions at current timestep
                [xlo, xhi, ylo, yhi, zlo, zhi] = [float(chunk.iloc
                                                        [4, 0].split()[0]),
                                                  float(chunk.iloc[4, 0]
                                                        .split()[1]),
                                                  float(chunk.iloc[5, 0]
                                                        .split()[0]),
                                                  float(chunk.iloc[5, 0]
                                                        .split()[1]),
                                                  float(chunk.iloc[6, 0]
                                                        .split()[0]),
                                                  float(chunk.iloc[6, 0]
                                                        .split()[1])]
                time_box_data.loc[time_counter] = [
                    time, xhi - xlo, yhi - ylo, zhi - zlo]
                time_counter += 1
        return [time_box_data, atoms]

    def find_ree_z_vectors(self, trj_file, atoms, time_box_data):
        """
        Find the z-component of the end-to-end vector of the block.

        Parameters
        ----------
        trj_file (str): path of trj file
        atoms (int): number of atoms present. Assumed constant within a
            file
        time_box_data (pd.DataFrame): time and box data for current file

        Raises
        ------
        ValueError
            molecule ids or type of first and last beads do not match

        Returns
        -------
        histData (pd.DataFrame): "time","mol", "type", "z", "Lz" for each block

        """
        histData = pd.DataFrame(columns=["Time"], dtype='float')
        timecounter = 0
        # chunk is one timestep. Assumes header is 9 for each timestep
        with pd.read_csv(trj_file, chunksize=(atoms), sep=" ",
                         skiprows=(lambda x: x % (atoms + 9) < 9),
                         skipinitialspace=(True),
                         names=["id", 'mol', 'type', 'x', 'y', 'z',
                                'ix', 'iy', 'iz', "test"]) as reader:
            for chunk in reader:
                chunk.drop("test", axis=1, inplace=True)
                time = time_box_data.loc[timecounter, "Time"]

                Lx = time_box_data.loc[timecounter, "Lx"]
                Ly = time_box_data.loc[timecounter, "Ly"]
                Lz = time_box_data.loc[timecounter, "Lz"]

                chunk["x"] = (chunk["x"] + chunk["ix"]) * Lx
                chunk["ix"] = 0
                chunk["y"] = (chunk["y"] + chunk["iy"]) * Ly
                chunk["iy"] = 0
                chunk["z"] = (chunk["z"] + chunk["iz"]) * Lz
                chunk["iz"] = 0

                group_object = chunk.groupby(["mol", "type"])
                # first bead of every block
                first = chunk.loc[group_object.id.idxmin()]
                first.reset_index(inplace=True, drop=True)
                # last bead of every block
                last = chunk.loc[group_object.id.idxmax()]
                last.reset_index(inplace=True, drop=True)

                tempData = pd.DataFrame(columns=["time", "mol", "type",
                                                 "z", "Lz"], dtype='float')
                tempData.mol = first.mol
                tempData.time = time
                if not last.mol.equals(tempData.mol):
                    raise ValueError("Molecule IDs don't match")
                tempData.type = first.type
                if not last.type.equals(tempData.type):
                    raise ValueError("Type IDs don't match")
                tempData.Lz = Lz

                for pair in range(len(first)):
                    v1 = first.z.iloc[pair]
                    v2 = last.z.iloc[pair]
                    v2 = tb.adjust(v1, v2, Lz)
                    tempData.at[pair, "z"] = v2 - v1
                histData = pd.concat([histData, tempData])
                timecounter += 1
        return histData

    def plot(self):
        """Plot distribution of each type, normalized by region width."""
        binlist = list(range(-15, 16, 1))

        span = self.VectorData[self.VectorData.time >
                               max(self.VectorData["time"]) * 0.9].copy()

        binlist = np.linspace(-1, 1, 10)
        span["z"] = span.z.astype("float64")
        span["Domain"] = span.Lz / 4
        span["Domain"] = span.Domain.div(2)
        span["Norm"] = span.z.div(span.Domain)
        plt.figure()
        n, bins, patch = plt.hist(
            span[span["type"] == 1].Norm, bins=binlist, color="r", alpha=0.3,
            density=True)
        m, bins, patch = plt.hist(
            span[span["type"] == 2].Norm, bins=binlist, color="b", alpha=0.3,
            density=True)
        plt.xlabel(r'$R_{ee,z}/Region Width$')
        plt.ylabel("Frequency")
        plt.title(self.simulation_name)

    def run(self):
        """Execute calculation."""
        self.load_profiles()
        self.load_trj_files()
        print('Processing Run Counter: ')
        for runcounter in range(len(self.trj_list)):
            print(f' {runcounter}')
            # list of files
            trj_file = self.trj_list[runcounter]
            [time_box_data, atoms] = self.get_box_dimensions(trj_file)
            self.BoxData = pd.concat([self.BoxData,
                                      time_box_data],
                                     ignore_index=True, axis=0)
            # Create a dictionary of all time steps
            ree_data = self.find_ree_z_vectors(trj_file, atoms, time_box_data)
            self.VectorData = pd.concat([self.VectorData,
                                         ree_data],
                                        ignore_index=True, axis=0)

        self.VectorData.to_csv(self.outputfile)


test = ReeZVectors("K1", "C:/Users/nxb1090/Desktop/GradResearch/Buchanan/" +
                   "Projects/HelixMelt/Data/LowerPressure/T80P240/")
test.run()
test.plot()
