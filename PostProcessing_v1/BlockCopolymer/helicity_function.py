# -*- coding: utf-8 -*-
"""
Finds helicity at every time step using inputed set points and tolerance,
following Boehm equation for helicity. Find average helicity of last 10% of
timesteps captured in angle/dihedral dump files

Inputs:
    SIMULATION = name of sim for name of folder and text inputs and outputs
    FILEPATH = directory where data is saved and outputs will be saved
    ANGLEFILE = name of angle dump file
    DIHEDRALFILE = name of dihedral dump file
    OUTPUTFILE = name of csv file containing average helicity at each
        timestep
    TOLERANCE = criteria for helicity. How far from the set points phi and
        theta can be and still counted as helical
    SET_THETA = set value of theta0. Some issues with wrapping.
    SET_PHI = set value of phi0. Some issues with wrapping. Not equal to
        LAMMPS setting. LAMMPS Phi = 200 -> SET_PHI = 20

Outputs:
    csv file containing helicity at each timestep
    Plot 1: Helicity at every timestep captured in the dump file
    Prints: Average helicity of the last 10% of timesteps in the dump files

Hard-Coded:
    *header length and position of timestep and box information
        (headerinfo, angleNum, dihedralNum, angle_time, dihedral_time, xlo..)
    *Currently for single chain. May need index adjustments if expanding to
        melts
    *Contents and angle and dihedral dumps (currentAngle, currentDihedral)
Error Messages:
    * "Time error": timesteps read from equivalent angle and dihedral chunks
        do not match. Check angleChink and dihedralChunk
    * "Helicity = 0. Check set points": Averag helicity found to be 0 which
        may indicate issues with set point wrapping

@author: Natalie Buchanan
@python: 3.8.10
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


# Input FILEPATH and angle and dihedral file to be examined
SIMULATION = "SimulationName"
FILEPATH = "C:/FilePath/DataSet/" + SIMULATION
ANGLEFILE = FILEPATH + "/LAMMPS/" + SIMULATION + "_angle.dump"
DIHEDRALFILE = FILEPATH + "/LAMMPS/" + SIMULATION + "_dihedral.dump"
OUTPUTFILE = FILEPATH + "/PostProcessing/" + "helicity.txt"
TOLERANCE = 20
SET_THETA = 160
SET_PHI = 200 - 180
print(SIMULATION + " Helicity" + "\n")

# Number of angles from first timestep of angle file
headerinfo = pd.read_csv(ANGLEFILE, nrows=9)
angleNum = int(headerinfo.iloc[2, 0])

# Number of dihedrals from first timestep of dihedral file
headerinfo = pd.read_csv(DIHEDRALFILE, nrows=9)
dihedralNum = int(headerinfo.iloc[2, 0])

angle_chunker = pd.read_csv(ANGLEFILE, chunksize=angleNum + 9)
dihedral_chunker = pd.read_csv(DIHEDRALFILE, chunksize=dihedralNum + 9)

TimeHelicityData = pd.DataFrame(columns=["Time", "Helicity"])
TIMESTEPS = 0

# For single timeset of data in angle and dihedral files
for angleChunk, dihedralChunk in zip(angle_chunker, dihedral_chunker):
    angle_time = int(angleChunk.iloc[0, 0])
    dihedral_time = int(dihedralChunk.iloc[0, 0])
    if angle_time != dihedral_time:  # checks that on same timestep
        print("Time error")
        # Get box dimensions at current timestep
    [xlo, xhi, ylo, yhi, zlo, zhi] = [float(angleChunk.iloc[4, 0].split()[0]),
                                      float(angleChunk.iloc[4, 0].split()[1]),
                                      float(angleChunk.iloc[5, 0].split()[0]),
                                      float(angleChunk.iloc[5, 0].split()[1]),
                                      float(angleChunk.iloc[6, 0].split()[0]),
                                      float(angleChunk.iloc[6, 0].split()[1])]

    # Removes header and creates data table sorted by atom ID for angle data
    currentAngle = [item[0].split() for item in
                    angleChunk.iloc[8:angleNum+8].values.tolist()]
    currentAngle = pd.DataFrame(currentAngle,
                                columns=['id', "type", 'atom1', 'atom2',
                                         'atom3', 'Theta', 'Eng'],
                                dtype='float')
    # Sorts by atom id
    currentAngle["id"] = currentAngle["id"].astype("int64")
    currentAngle.set_index("atom1", inplace=True)
    currentAngle.index = currentAngle.index.astype("int64")
    currentAngle = currentAngle.sort_values("atom1")

    # Removes header and create data table sorted by atom ID for dihedral data
    currentDihedral = [item[0].split() for item in
                       dihedralChunk.iloc[8: dihedralNum + 8].values.tolist()]
    currentDihedral = pd.DataFrame(currentDihedral,
                                   columns=['id', "type", 'atom1', 'atom2',
                                            'atom3', 'atom4', 'Phi'],
                                   dtype='float')
    # Sorts by atom id
    currentDihedral["id"] = currentDihedral["id"].astype("int64")
    currentDihedral.set_index("atom1", inplace=True)
    currentDihedral = currentDihedral.sort_values("atom1")

    # Creates dataframe for current timestep
    currentHelicity = pd.DataFrame(index=list(currentDihedral.index.values),
                                   columns=["Theta", "Theta Diff",
                                            "Theta Spec", "Phi",
                                            "Phi Diff", "Phi Spec", "Residue"],
                                   dtype="float")
    currentHelicity.index = currentHelicity.index.astype("int64")
    currentHelicity.update(currentAngle)
    currentHelicity.update(currentDihedral)

    # Finds difference between setpoint and theta with wrapping
    currentHelicity["Theta Diff"] = currentHelicity["Theta"] - SET_THETA
    currentHelicity.loc[(currentHelicity["Theta Diff"]) >
                        180, "Theta Diff"] -= 360
    currentHelicity.loc[(currentHelicity["Theta Diff"]) < -180,
                        "Theta Diff"] += 360

    # Theta Spec = 1 if theta is within spec
    currentHelicity["Theta Spec"] = np.where(abs(currentHelicity["Theta Diff"])
                                             <= TOLERANCE, 1, 0)

    # Finds difference between setpoint and phi with wrapping
    currentHelicity["Phi Diff"] = currentHelicity["Phi"] - SET_PHI
    currentHelicity.loc[(currentHelicity["Phi Diff"]) >
                        180, "Phi Diff"] -= 360
    currentHelicity.loc[(currentHelicity["Phi Diff"]) <
                        -180, "Phi Diff"] += 360
    # Phi Spec = 1 if within Tolerance
    currentHelicity["Phi Spec"] = np.where(abs(currentHelicity["Phi Diff"])
                                           <= TOLERANCE, 1, 0)
    currentHelicity["Residue"] = np.where((currentHelicity["Theta Spec"] == 1)
                                          & (currentHelicity["Phi Spec"] == 1),
                                          1, 0)
    helicity = currentHelicity["Residue"].sum()/dihedralNum
    TimeHelicityData = TimeHelicityData.append({"Time": angle_time,
                                                "Helicity": helicity},
                                               ignore_index=True)
    TIMESTEPS += 1

# Saves Helicity per timestep table to text file
TimeHelicityData.to_csv(OUTPUTFILE)

# Average Ree^2 from lat 10% of timesteps
SPAN = int(TIMESTEPS * 0.1)
print("Mean Helicity = " + str(round(TimeHelicityData["Helicity"]
                                     [-SPAN:].mean(), 3)))

if abs(TimeHelicityData["Helicity"][-SPAN:].mean()) < 0.01:
    print("Helicity = 0. Check set points")
print("Average Phi = " + str(round(currentHelicity["Phi"].mean(), 3)) +
      "; Set Phi = " + str(SET_PHI))
print("Average Theta = " + str(round(currentHelicity["Theta"].mean(), 3)) +
      "; Set Theta = " + str(SET_THETA))

# Figure 1: Helicity over time
plt.figure()
plt.plot(TimeHelicityData["Time"], TimeHelicityData["Helicity"], lw=0.5)
plt.ylim([0, 1])
plt.xlabel("Time")
plt.ylabel("Helicity")
plt.annotate("Avg Hel = " +
             str(round(TimeHelicityData["Helicity"][-SPAN:].mean(), 3)),
             (TimeHelicityData["Time"].iloc[-1],
              TimeHelicityData["Helicity"].iloc[-1]),
             (.75, 0.9), "axes fraction")
plt.show()
