atoms = 120000
len_mol = 60

#Box Parameters
xlo = -25.0866 ; xhi = -xlo;
ylo = xlo; yhi = xhi;
zlo = -28.3313; zhi = -zlo;
Lbox=[xlo, xhi, ylo, yhi, zlo, zhi]
no_mol = atoms/len_mol
no_A = 30; no_B = 30; no_C = len_mol - no_A - no_B
bonds = 118000
name = 't0.1mil.lammpstrj'
filepath = './'

FramesToSkip = 0
atoms_A = no_mol*no_A

configInfo = [atoms, len_mol, Lbox, no_mol, no_A, no_B, no_C, bonds, name, filepath, FramesToSkip, atoms_A]

