# Works with python 3
import ToolBox as tb
from config import *
import math
from operator import itemgetter
import sys
import numpy as np
import time
#import matplotlib as mpl
#mpl.use('Agg')
#import matplotlib.pyplot as plt

""" SECTION 1: Settings"""
start = 1 # Line to start on
stepsize = 1 #stepsize for q coordinate generation
qcut = 2 # we can theoretically go up until 2*pi, but the signal might be weak
Timesteps = 1#number of timesteps from trj file to be averaged
Output = []
header =  ((atoms+9)*FramesToSkip) + 9#header length of file, if file includes more frames you want to skip, use this.

"""SECTION 2: File Paths"""
inputfilename = filepath + 'Doc.txt'
inputfile = open(inputfilename, 'r')
SqAfile = open(filepath + 'SqA.txt','w')
allqfile = open(filepath + 'FullQlist.txt','w')

"""SECTION 3: Text to List""" # We need to scale data or not, depending on whether xs/ys/zs text is present in the Doc.txt file. If any corrections for scaled positions are made, check structure factor calculation lines as well
AtomList = tb.listmaker2(header, inputfile) #Every entry becomes a float

"""SECTION 4: Determine number of Wave Vectors to be made"""
x_range=xhi - xlo; y_range=yhi-ylo; z_range=zhi-zlo
counter=0; 
qmin=(2 * math.pi) / max([x_range, y_range, z_range]); qmax = (2 * math.pi)
for nx in range(int(math.floor(x_range)/stepsize)): #Generates valid wave vectors
    qx= (tb.q(nx, x_range) * stepsize)
    for ny in  range(int((y_range)/stepsize)):
        qy = (tb.q(ny, y_range) * stepsize)
        for nz in range(int((z_range)/stepsize)):
            qz = (tb.q(nz, z_range) * stepsize)
            ql = ((qx ** 2) + (qy ** 2) + (qz ** 2)) ** 0.5
            if ql >= qmin and ql <= qcut: # PP has replaced qmax with qcut to speed up calculations. Can change back for more accuracy
                counter+=1
print(counter)
maxcounter = counter

"""Section 4.1: Create Wave Vectors"""             
## Make set of viable wave vectors ##
counter=0; QArray=np.zeros((maxcounter,8)) # NOTE: CAN WE DELETE THIS COMMENT? This must be manually inputted if the box changes. Set to a high number, add sys.exit() after print counter. The value of the counter goes in here
qmin=(2 * math.pi) / max([x_range, y_range, z_range]); qmax = (2 * math.pi)
for nx in range(int(math.floor(x_range)/stepsize)): #Generates valid wave vectors
    qx= (tb.q(nx, x_range) * stepsize)
    print('qx: ' + str(qx)) #prints for progress tracking
    for ny in  range(int((y_range)/stepsize)):
        qy = (tb.q(ny, y_range) * stepsize)
        for nz in range(int((z_range)/stepsize)):
            qz = (tb.q(nz, z_range) * stepsize)
            ql = ((qx ** 2) + (qy ** 2) + (qz ** 2)) ** 0.5
            if ql >= qmin and ql <= qcut: # PP has replaced qmax with qcut. Can change back for more accuracy
                counter+=1
                QArray[counter-1]=[counter, qx, qy, qz, ql, 0, 0, 0]

"""SECTION 5: For each wave, calculate structure for each atom"""
counter=0; startTime=time.time()
for wave in QArray:
        q_info = wave[1:5]
        CosSumA = 0; SinSumA = 0; current_A = 0; 
        #Progress report
        if counter % 100 == 0:
            print(str(counter)+'    '+str(time.time()-startTime))#prints for progress tracking 
            startTime=time.time()
        counter += 1; 
        
        for atom in AtomList: # NOTE: Should be atom[2], x be atom[3], y=atom[4],z=atom[5]
            Type = atom[2]
            Atom_info = [atom[3]*x_range - xlo, atom[4]*y_range-ylo, atom[5]*z_range-zlo] # we are subtracting box edge because q looks very weird otherwise. # we must unscale correctly
            if Type == 1.0: #Type A Atoms # PP comment: is there a way to put this in config.py? What if we have two types that we want to find the S(q) for?
                [CosSumA, SinSumA, current_A] = tb.structure_for_atom(q_info, Atom_info, [CosSumA, SinSumA], current_A)
        wave = tb.structure_values([CosSumA, SinSumA, current_A], atoms, wave) 
                
print("Section 5 done")
"""SECTION 6: Saving full qlist to file"""# For each wave deivide structure, beta, and theta by total number of atoms""" # is this double counting?
#for wave in QArray: # are we double-dividing???
#    wave[5]=wave[5]/atoms_A #Structure
#    wave[6]=wave[6]/atoms_A #Beta
#    wave[7]=wave[7]/atoms_A #Theta
# write out just so I know
for wave in QArray:
    allqfile.write(str(int(wave[0]))+' '+str(wave[1])+' '+str(wave[2])+' '+str(wave[3])+' '+str(wave[4])+' '+str(wave[5])+ '\n')
allqfile.close()
   
print("write done")

"""SECTION 7: Sort by ql value, then by qx"""
QArray = QArray[QArray[:,1].argsort()]
QArray = QArray[QArray[:,4].argsort(kind='mergesort')]

#MasterArray=np.empty([0,2])
qlList=[]; structurelist=[]
y_max =0;

uniqueReturn=np.unique(QArray[:,4], return_counts=True) # TO DO: round up wave vectors while generating QArray so that the unique() doesn't make errors
MasterArray=np.zeros((len(uniqueReturn[0]),3))
MasterArray[:,0]=uniqueReturn[0]
MasterArray[:,1]=uniqueReturn[1]

for x in range(len(MasterArray)):
    ql=MasterArray[x][0]
    for info in QArray:
        ai = info[4]
        ac = info[5]
        if ql == ai:
            MasterArray[x][2] +=ac
    MasterArray[x][2]/=MasterArray[x][1] # average
# y_max=max(MasterArray[:,2])

for i in range(len(MasterArray)): # do not normalize. Simply write to file
    #MasterArray[i][2]/=y_max
    qlList.append(MasterArray[i][0])
    structurelist.append(MasterArray[i][2])
    SqAfile.write(str(MasterArray[i][0]) + '    ' + str(MasterArray[i][2]) + '\n')

"""SECTION 7: Graphs"""
#plt.rcParams["font.family"] = "Times New Roman"
#plt.rcParams.update({'font.size': 16})   
#fig=plt.figure(1)
#plt.title('Structure Factor Type A')
#plt.xlabel('Wave  number (q)')
#plt.ylabel('Structure Factor (Scaled)')
#ax=fig.add_subplot(111)
#plt.scatter(qlList, structurelist, c='Red',alpha=.5)
#plt.yscale('log')
##plt.ylim(10E-6,1.5)
##plt.xlim(0,2*math.pi)

#plt.savefig(filepath+"AvgSqA.png")
##plt.show()
SqAfile.close()

