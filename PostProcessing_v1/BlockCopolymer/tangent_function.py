# -*- coding: utf-8 -*-
"""
Finds tangent function of single polymer chain. Finds average distance between
particles s apart. Finds s_lp, lp, pitch, and s_p and averages these values
over the last 10% of timesteps.

Inputs:
    SIMULATION = name of sim for name of folder and text inputs and outputs
    FILEPATH = directory where data is saved and outputs will be saved
    FILENAME = name of trajectory file
    OUTPUTFILE_TAN = name of csv file containing s, sum of tangents, number of
        data points, average tan function, and average distance for last
        time step
    OUTPUTFILE_PEAK = name of csv file containing s and the exponential of the
        tangent function for the locations identified as the peaks of the
        tangent function for the last time step
    OUTPUTFILE_CHARACTER = name of csv file containing step number, step time,
        s_lp, lp, pitch, and s_p"

Outputs:
    1. AvgTan Func - csv containing s, tangent function, exponential fit,
        average distance, and number of data points
    2. Avg Peak - csv containing peak information - s, ln(tan func)
    3. Chain Character - timested id, timestep,  time, s_lp, lp,
        pitch, s_p
    Prints: Differences in n and avg distance betfrom
        exponential fit of peaks, trendline of exponential fitween peaks,
        trendline for exponential fit of the peaks

Hard-Coded:
    *Data from last 10% of trj file (v_firstStep, v_lastStep)
    *Peak trendline is assumed to be linear (v_peak_fit)
    *Exponential fit taken to be linear (v_fitline)

@author: Natalie Buchanan
@python: 3.8.10
"""

import numpy as np
import scipy.signal as sc
from scipy.signal import argrelextrema
import pandas as pd
import ToolBox as tb

# Input FILEPATH and trj file to be examined
SIMULATION = "SimulationName"
FILEPATH = "C:/FilePath/DataSet/" + SIMULATION
FILENAME = FILEPATH + "/LAMMPS/" + SIMULATION + ".lammpstrj"
OUTPUTFILE_TAN = FILEPATH + "/PostProcessing/" + SIMULATION + "_AvgTanFunc.txt"
OUTPUTFILE_PEAK = FILEPATH + "/PostProcessing/" + \
    SIMULATION + "_AvgPeakData.txt"
OUTPUTFILE_CHARACTER = FILEPATH + "/PostProcessing/" + SIMULATION +\
    "_ChainCharacter.txt"
print(SIMULATION + "\n")

# %% Reads last timestep
# Reads dump file, selects the last 10 % timestep, and wraps so all atoms in
# molecule are in the same box
with open(FILENAME) as f:
    d = tb.listmaker(f)
atoms = int(d[3][0])
totalsteps = int(len(d)/(atoms+9))
firstStep = int(round(totalsteps*0.9, 0))
lastStep = int(round(totalsteps, 0))

# Avg Table collects data from tangent function for each time step of interest
AvgTable = np.zeros(len(range(firstStep, lastStep)), 6)
AvgTable = pd.DataFrame(AvgTable, columns=["step", "ts", "s_lp", "Lp",
                                           "pitch", "s_p"])
AvgTable["step"] = range(firstStep, lastStep)
AvgTable.set_index("step", inplace=True)

for ts in range(firstStep, lastStep):
    print(ts)
    currentstep = d[ts*(atoms+9):(ts+1)*(atoms+9)]
    time = int(currentstep[1][0])
    AvgTable.loc[int(ts)] = time
    [xlo, xhi, ylo, yhi, zlo, zhi] = [float(currentstep[5][0]),
                                      float(currentstep[5][1]),
                                      float(currentstep[6][0]),
                                      float(currentstep[6][1]),
                                      float(currentstep[7][0]),
                                      float(currentstep[7][1])]
    # currentstep hold atom info for the current step
    currentstep = currentstep[9:]
    currentstep = pd.DataFrame(currentstep, columns=[
                            'id', 'mol', 'type', 'x', 'y', 'z',
                            'ix', 'iy', 'iz'],
                            dtype='float')
    currentstep = currentstep.sort_values("id")

    # %% Unwrap Coordinates
    for i in range(atoms):
        atom0 = currentstep.iloc[i]
        atom0["x"] = tb.unwrap(atom0["x"], atom0["ix"], xhi-xlo)
        atom0["ix"] = 0
        atom0["y"] = tb.unwrap(atom0["y"], atom0["iy"], yhi-ylo)
        atom0["iy"] = 0
        atom0["z"] = tb.unwrap(atom0["z"], atom0["iz"], zhi-zlo)
        atom0["iz"] = 0

    # %% Set up currentTanTable
    # Creates array to hold current time steptangent function data
    # Columns: N (distance between tangents tested), Sum of tan dot products,
    # number of tangents with given N, Avg tan dot product
    currentTanTable = np.zeros((atoms-1, 4))
    currentTanTable = pd.DataFrame(currentTanTable,
                                   columns=["Sum of tan", "Number", "Avg Tan",
                                            "Avg Distance"])

    # OutputTanTable holds average tan function information for each timestep
    OutputTanTable = pd.DataFrame(columns=["s", "Tan Fun",
                                           "Exponential Fit", "Avg Distance"])

    # %% Tangent Function
    # %%% Information for first tangent t_0
    for i in range(atoms-2):
        atom0 = currentstep.iloc[i]
        atom1 = currentstep.iloc[i+1]

        # Unit vector t0
        vectordistance = tb.distance([atom0["x"], atom0["y"],
                                      atom0["z"]],
                                     [atom1["x"], atom1["y"], atom1["z"]])
        t0 = [(atom1["x"] - atom0["x"]) / vectordistance,
              (atom1["y"] - atom0["y"]) / vectordistance,
              (atom1["z"] - atom0["z"]) / vectordistance]

        # Information for second tangent t_N
        for j in range(i, atoms-1):
            atomN = currentstep.iloc[j]
            atomN1 = currentstep.iloc[j+1]

            vectordistance = tb.distance([atomN["x"], atomN["y"],
                                          atomN["z"]],
                                         [atomN1["x"], atomN1["y"],
                                          atomN1["z"]])
            # Unit Vector tn
            tn = [(atomN1["x"] - atomN["x"]) / vectordistance,
                  (atomN1["y"] - atomN["y"]) / vectordistance,
                  (atomN1["z"] - atomN["z"]) / vectordistance]

            # Distance between monomers
            distance = tb.distance([atom0["x"], atom0["y"], atom0["z"]],
                                   [atomN1["x"], atomN1["y"], atomN1["z"]])

            # Dot product, adds data to table
            dotproduct = np.dot(t0, tn)
            s = j - i
            currentTanTable["Sum of tan"][s] = \
                currentTanTable["Sum of tan"][s] + dotproduct
            currentTanTable["Number"][s] = currentTanTable["Number"][s]+1
            currentTanTable["Avg Distance"][s] = \
                currentTanTable["Avg Distance"][s] + distance

    # %%% Finds average tan product and distance. Stores in currenTanTable
    currentTanTable["Avg Tan"] = \
        currentTanTable["Sum of tan"] / currentTanTable["Number"]
    currentTanTable["Avg Distance"] = \
        currentTanTable["Avg Distance"] / currentTanTable["Number"]

    # %% Tangent Function over n
    # %%% Finds maxima of tangent function
    # Finds peaks on tangent function
    peaks, _ = sc.find_peaks(currentTanTable["Avg Tan"]
                             [:int(atoms/2)], height=0)
    fullpeaks, _ = sc.find_peaks(currentTanTable["Avg Tan"][:], height=0)

    # peakTable holds peak locations, distance, and tan value
    peakTable = {"Location": fullpeaks,
                 "Avg Distance": currentTanTable["Avg Distance"][fullpeaks],
                 "Avg Tan": currentTanTable["Avg Tan"][fullpeaks]}
    peakTable = pd.DataFrame(peakTable)

    peakTable.loc[len(peakTable.index)] = [0, 0, 1]   # Adds 0,1 point as max
    peakTable = peakTable.sort_values("Location")
    peakTable.reset_index(drop=True, inplace=True)  # Index now by peak

    # %%% ln(tangent) over n for all peaks where n <= 50
    CUTOFF = 50   # picked due to sufficient statistics as a starting point

    # expoFrame holds peak and ln(tan) info for current time step
    expoFrame = pd.DataFrame(peakTable["Location"].loc[peakTable["Location"]
                                                       <= CUTOFF])
    expoFrame["Expo Fit"] = peakTable["Avg Tan"].loc[peakTable["Location"]
                                                     <= CUTOFF]
    expoFrame["Expo Fit"] = np.log(expoFrame["Expo Fit"])

    pitch = peakTable["Avg Distance"].diff(1)[1]
    s_pitch = peakTable["Location"].diff(1)[1]

    # Find the first minimum peak which will be used for fit
    expoMin = expoFrame.iloc[argrelextrema(expoFrame["Expo Fit"].values,
                                           np.less_equal, order=2)[0]]
    expoFrame = expoFrame[0:expoMin.index[0] + 1]

    # Linear fit of ln(tan) of peak points
    peak_fit = np.polyfit(expoFrame["Location"], expoFrame["Expo Fit"], 1)
    x_peak_fit = range(0, expoMin["Location"].iloc[0], 1)
    y_peak_fit = []
    for i in range(len(x_peak_fit)):
        y_peak_fit.append(peak_fit[0]*x_peak_fit[i] + peak_fit[1])

    # %%% Find l_p, and s_lp
    s_lp = -1/peak_fit[0]

    # Extrapolates/interpolates average distance for s_lp found
    if s_lp > 0:
        currentTanTable.index = currentTanTable.index.map(float)
        # Extrapolates value for s_lp if value found higher than cutoff
        if s_lp > currentTanTable.index[-1]:
            slope = ((currentTanTable["Avg Distance"].iloc[-2] -
                     currentTanTable["Avg Distance"].iloc[-1]) /
                     (currentTanTable.index[-2]-currentTanTable.index[-1]))
            b = currentTanTable["Avg Distance"].iloc[-1] \
                - (slope * currentTanTable.index[-1])
            currentTanTable.loc[s_lp] = [np.nan, np.nan, np.nan, np.nan]
            currentTanTable["Avg Distance"][s_lp] = (slope*s_lp) + b
        else:  # Interpolates if within cutoff
            currentTanTable.loc[s_lp] = [np.nan, np.nan, np.nan, np.nan]
            currentTanTable.sort_index(inplace=True)
            currentTanTable.interpolate(method="linear", inplace=True)
        lp = currentTanTable["Avg Distance"][s_lp]
    else:
        # Print error message stating the s_lp does not exist
        print("\n" + "s_lp = " + str(round(s_lp, 3)) + ". Cannot find lp.")

    # %%% Output files for current time step
    OutputTanTable["s"] = currentTanTable.index
    OutputTanTable["Tan Fun"] = currentTanTable["Avg Tan"]
    OutputTanTable["Exponential Fit"] = OutputTanTable.index*peak_fit[0]\
        + peak_fit[1]
    OutputTanTable["Avg Distance"] = currentTanTable["Avg Distance"]
    OutputTanTable.to_csv(OUTPUTFILE_TAN)
    expoFrame.to_csv(OUTPUTFILE_PEAK)

    AvgTable = pd.DataFrame(AvgTable, columns=["step", "ts", "s_lp", "Lp",
                                               "Pitch", "s_pitch"])
    AvgTable["s_lp"].loc[ts] = s_lp
    AvgTable["Lp"].loc[ts] = lp
    AvgTable["Pitch"].loc[ts] = pitch
    AvgTable["s_pitch"].loc[ts] = s_pitch

print(SIMULATION)
print(AvgTable.describe())
AvgTable.to_csv(OUTPUTFILE_CHARACTER)
